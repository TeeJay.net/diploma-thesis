<p>
	Dobrý den,<br />
	Vaše objednávka byla potvrzena. V příloze zasíláme smlouvu, kterou je nutné podepsat, a fakturu.<br />
	Hlavní detaily vaší objednávky:
</p>
<table>
	<tbody>
	<tr>
		<td>Jméno</td>
		<td>{{ $order->first_name }} {{ $order->last_name }}</td>
	</tr>
	<tr>
		<td>Email</td>
		<td>{{ $order->email }}</td>
	</tr>
	<tr>
		<td>Telefon</td>
		<td>{{ $order->phone }}</td>
	</tr>
	<tr>
		<td>Místo focení</td>
		<td>{{ $order->street }} {{ $order->house_number }}, {{ $order->postcode }} {{ $order->city }}</td>
	</tr>
	<tr>
		<td>Datum a čas focení</td>
		<td>{{ $order->rental_from->format('d.m.Y H:i') }} - {{ $order->rental_to->format('d.m.Y H:i') }}</td>
	</tr>
	<tr>
		<td>Vybrané pozadí</td>
		<td>{{ $order->background->name . ' ' . strip_tags($order->background->description) }}</td>
	</tr>
	<tr>
		<td>Tisk</td>
		<td>{{ \App\Order::getPrintOptions()[$order->print] }}</td>
	</tr>

	@if ($order->note)
	<tr>
		<td>Poznámka</td>
		<td>{{ $order->note }}</td>
	</tr>
	@endif

	</tbody>
</table>

<p>
	Tým <a href="http://cvakmat.cz">Cvakmat</a><br />
	Email: <a href="mailto:hello@cvakmat.cz">hello@cvakmat.cz</a><br />
	Telefon: +420 604 780 622
</p>