@extends('BE.layouts.login', [
	'title' => trans('auth.singular'),
])

@section('content')
	<form class="m-t" role="form" method="POST" action="{{ url('/admin/login') }}">
		{!! csrf_field() !!}

		<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
			<label class="control-label hidden">{{ trans('auth.email') }}</label>
			<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ trans('auth.email') }}" autofocus>

			@if ($errors->has('email'))
				<span class="help-block">
				<strong>{{ $errors->first('email') }}</strong>
			</span>
			@endif
		</div>

		<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
			<label class="control-label hidden">{{ trans('auth.password') }}</label>
			<input type="password" class="form-control" name="password" placeholder="{{ trans('auth.password') }}">

			@if ($errors->has('password'))
				<span class="help-block">
				<strong>{{ $errors->first('password') }}</strong>
			</span>
			@endif
		</div>

		<div class="form-group">
			<button type="submit" class="btn btn-primary block full-width m-b">
				<i class="fa fa-btn fa-sign-in"></i> {{ trans('auth.login') }}
			</button>
		</div>

		<div class="form-group">
			<div class="checkbox">
				<label>
					<input type="checkbox" name="remember"> {{ trans('auth.remember') }}
				</label>
			</div>
		</div>

		<p class="m-t">
			<small>
				<a href="https://tomasjanecek.cz">Tomáš Janeček</a>, {{ date('Y') }}
			</small>
		</p>
	</form>
@endsection
