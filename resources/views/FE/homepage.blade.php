@extends('FE.layouts.app', [
	'title' => $texts['home_about_us']->name,
])

@section('content')
<div id="cvakmat-carousel" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#cvakmat-carousel" data-slide-to="0" class="active"></li>
		<li data-target="#cvakmat-carousel" data-slide-to="1"></li>
		<li data-target="#cvakmat-carousel" data-slide-to="2"></li>
		<li data-target="#cvakmat-carousel" data-slide-to="3"></li>
		<li data-target="#cvakmat-carousel" data-slide-to="4"></li>
	</ol>

	<div class="carousel-inner" role="listbox">
		<div class="item active">
			<img src="assets/fe/img/carousel/01.jpg" alt="Cvakmat">
			<div class="carousel-caption">
				{!! $texts['home_slider']->text !!}
			</div>
		</div>
		<div class="item">
			<img src="assets/fe/img/carousel/02.jpg" alt="Cvakmat">
			<div class="carousel-caption">
				{!! $texts['home_slider']->text !!}
			</div>
		</div>
		<div class="item">
			<img src="assets/fe/img/carousel/03.jpg" alt="Cvakmat">
			<div class="carousel-caption">
				{!! $texts['home_slider']->text !!}
			</div>
		</div>
		<div class="item">
			<img src="assets/fe/img/carousel/04.jpg" alt="Cvakmat">
			<div class="carousel-caption">
				{!! $texts['home_slider']->text !!}
			</div>
		</div>
		<div class="item">
			<img src="assets/fe/img/carousel/05.jpg" alt="Cvakmat">
			<div class="carousel-caption">
				{!! $texts['home_slider']->text !!}
			</div>
		</div>
	</div>

	<a class="left carousel-control" href="#cvakmat-carousel" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Předchozí</span>
	</a>
	<a class="right carousel-control" href="#cvakmat-carousel" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Další</span>
	</a>
</div>

<div class="container content-sm about-us">
	{!! $texts['home_about_us']->text !!}
</div>

<div class="bg-color-light why">
	<div class="container content-sm">
		{!! $texts['home_why']->text !!}
	</div>
</div>

<div class="call-action-v1 cta">
	<div class="container">
		<div class="call-action-v1-box">
			<div class="call-action-v1-in">
				{!! $texts['home_cta']->text !!}
			</div>
			<div class="call-action-v1-in inner-btn page-scroll"><a class="btn-u btn-u-lg btn-brd btn-brd-width-2 btn-brd-hover btn-u-dark btn-u-block" href="{{ url('objednat-cvakmat') }}">Chci Cvakmat!</a></div>
		</div>
	</div>
</div>

<div class="bg-color-light opinions">
	<div class="container content-sm">
		{!! $texts['home_opinions']->text !!}
	</div>
</div>

<div class="container content-sm blog">

	{!! $texts['home_articles']->text !!}

	<div class="row">
	@foreach($articles as $article)
		@php
		$articleLink = url('/blog/' . $article->alias);
		@endphp
		<div class="col-sm-4">
			<div class="thumbnails-v1">
				<div class="thumbnail-img">
					<a href="{{ $articleLink }}">
					@if ($article->image)
						<img class="img-responsive" src="{{ asset($article->image . '?w=737&h=466&fit=crop&version=' . $article->updated_at->format('dmyHis')) }}" alt="{{ $article->heading }}">
					@else
						<img class="img-responsive hidden-xs" src="{{ asset('images/other/transparent.png?w=737&h=466&fit=crop') }}" alt="{{ $article->heading }}">
					@endif
					</a>
				</div>
				<div class="caption">
					<h3><a href="{{ $articleLink }}">{{ $article->heading }}</a></h3>
					<p>{!! str_limit($article->intro, 200) !!}</p>
					<p><a class="read-more" href="{{ $articleLink }}">Číst dál...</a></p>
				</div>
			</div>
		</div>
	@endforeach
	</div>
</div>

<div class="bg-color-light clients">
	<div class="container content-sm">
		{!! $texts['home_clients']->text !!}
		<div class="row">
			@foreach($clients as $client)
			<div class="col-xs-6 col-sm-2">
				<img class="img-responsive" src="{{ asset($client->image . '?w=737&h=737&fit=crop&version=' . $client->updated_at->format('dmyHis')) }}" alt="{{ $client->name }}">
			</div>
			@endforeach
		</div>
	</div>
</div>

<div class="container content-sm social">
	<div class="col-sm-6">
		<div class="text-right">
			<a href="https://www.facebook.com/misik.photography">
				<i class="fa fa-facebook"></i> FACEBOOK
			</a>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="text-left">
			<a href="https://www.instagram.com/cvakmaster/">
				<i class="fa fa-instagram"></i> INSTAGRAM
			</a>
		</div>
	</div>
</div>

<div class="bg-color-light galleries">
	<div class="container content-sm">
		{!! $texts['home_galleries']->text !!}
		<div class="row margin-top-20">
			@foreach ($galleries as $gallery)
				@php
					$galleryLink = url('/galerie/' . $gallery->alias);
				@endphp
				<div class="col-md-4 grid-image">
					<a href="{{ $galleryLink }}" title="{{ $gallery->name }}">
						@if (!$gallery->password && $gallery->photos)
							<img class="img-responsive" src="{{ asset($gallery->photos[0] . '?w=735&h=464&fit=crop&version=' . $gallery->updated_at->format('dmyHis')) }}" alt="{{ $gallery->name }}">
						@else
							<img class="img-responsive" src="{{ asset('assets/img/gallery-placeholder.png') }}" alt="{{ $gallery->heading }}">
						@endif
						<div class="row">
							<div class="col-sm-6">
								<h2 class="heading-standard text-left">{{ $gallery->name }}</h2>
							</div>
							<div class="col-sm-6">
								<h3 class="heading-standard text-right">{{ $gallery->created_at->format('d. m. Y') }}</h3>
							</div>
						</div>
					</a>
				</div>
			@endforeach
		</div>
	</div>
</div>
@endsection
