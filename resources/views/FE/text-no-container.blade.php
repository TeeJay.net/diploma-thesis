@extends('FE.layouts.app', [
	'title' => $title,
])

@section('content')
	<div class="breadcrumbs">
		<div class="container">
			<h1 class="pull-left">{{ $title }}</h1>
			<ul class="pull-right breadcrumb">
				<li><a href="{{ url('/') }}">{{ menu(1, $menu) }}</a></li>
				<li class="active">{{ $title }}</li>
			</ul>
		</div>
	</div>

	{!! $content !!}
@endsection
