@extends('FE.layouts.app', [
	'title' => menu(6, $menu),
])

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<h1 class="pull-left">{{ menu(6, $menu) }}</h1>
		<ul class="pull-right breadcrumb">
			<li><a href="{{ url('/') }}">{{ menu(1, $menu) }}</a></li>
			<li class="active">{{ menu(6, $menu) }}</li>
		</ul>
	</div>
</div>

<div class="container content">
	@foreach ($articles as $article)
		@php
			$articleLink = url('/blog/' . $article->alias);
		@endphp
		<div class="row margin-bottom-20">
			<div class="col-sm-5 sm-margin-bottom-20">
				<a href="{{ $articleLink }}">
				@if ($article->image)
					<img class="img-responsive" src="{{ asset($article->image . '?w=456&h=289&fit=crop&version=' . $article->updated_at->format('dmyHis')) }}" alt="{{ $article->heading }}">
				@else
					<img class="img-responsive" src="{{ asset('images/other/transparent.png?w=456&h=1&fit=crop') }}" alt="{{ $article->heading }}">
				@endif
				</a>
			</div>
			<div class="col-sm-7">
				<div class="news-v3">
					<ul class="list-inline posted-info">
						<li>{{ $article->publishedByUser->public_name }}</li>
						<li><a href="{{ url('/blog/kategorie/' . $article->category->alias) }}">{{ $article->category->name }}</a></li>
						<li>{{ $article->created_at->format('d. m. Y') }}</li>
						@if (!$article->tags->isEmpty())
						<li>Tagy:
							@foreach ($article->tags as $tag)
								<a href="{{ url('/blog/tag/' . $tag->alias) }}">{{ $tag->name }}</a>{{ !$loop->last ? ',' : '' }}
							@endforeach
						</li>
						@endif
					</ul>
					<h2><a href="{{ $articleLink }}">{{ $article->heading }}</a></h2>
					{!! $article->intro !!}
				</div>
			</div>
		</div>
		<div class="clearfix margin-bottom-20"><hr></div>
	@endforeach

	<div class="clearfix"></div>
	<div class="pagination-wrapper">
		{!! $articles->appends(request()->all())->render() !!}
	</div>
</div>
@endsection
