@extends('FE.layouts.app', [
	'title' => $article->heading,
	'breadcrumbs'	=> [
	],
	'og' => [
		'image' => !$article->fb_image && !$article->image
			? null
			: ($article->fb_image
				? asset($article->fb_image . '?w=1200&version=' . $article->updated_at->format('dmyHis'))
				: asset($article->image . '?w=1200&version=' . $article->updated_at->format('dmyHis'))
			),
		'title' => $article->heading,
		'description' => $article->intro
			? str_limit(strip_tags($article->intro), 220)
			: str_limit(strip_tags($article->text), 220),
	],
])

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<h1 class="pull-left">{{ $article->heading }}</h1>
		<ul class="pull-right breadcrumb">
			<li><a href="{{ url('/') }}">{{ menu(1, $menu) }}</a></li>
			<li><a href="{{ url('/blog') }}">{{ menu(6, $menu) }}</a></li>
			<li class="active">{{ $article->heading }}</li>
		</ul>
	</div>
</div>

<div class="container content">
	<div class="row">
		<div class="col-md-9">
			<div class="news-v3 bg-color-white margin-bottom-30">
				@if ($article->image)
					<img class="img-responsive full-width" src="{{ asset($article->image . '?w=848&version=' . $article->updated_at->format('dmyHis')) }}" alt="{{ $article->heading }}">
				@else
					<img class="img-responsive full-width" src="{{ asset('images/other/transparent.png?w=848&h=1&fit=crop') }}" alt="{{ $article->heading }}">
				@endif
				<div class="news-v3-in">
					<ul class="list-inline posted-info">
						<li>Autor: {{ $article->publishedByUser->public_name }}</li>
						<li><a href="{{ url('/blog/kategorie/' . $article->category->alias) }}">{{ $article->category->name }}</a></li>
						<li>{{ $article->created_at->format('d. m. Y') }}</li>
						@if (!$article->tags->isEmpty())
							<li>Tagy:
								@foreach ($article->tags as $tag)
									<a href="{{ url('/blog/tag/' . $tag->alias) }}">{{ $tag->name }}</a>{{ !$loop->last ? ',' : '' }}
								@endforeach
							</li>
						@endif
					</ul>
					<h2>{{ $article->heading }}</h2>
					{!! $article->intro !!}
					{!! $article->text !!}
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="headline-v2"><h2>Nejnovější články</h2></div>
			<ul class="list-unstyled blog-latest-posts margin-bottom-50">
				@foreach($latestArticles as $latestArticle)
				<li>
					<h3><a href="{{ url('/blog/' . $latestArticle->alias) }}">{{ $latestArticle->heading }}</a></h3>
					@if ($latestArticle->image)
						<a href="{{ url('/blog/' . $latestArticle->alias) }}">
							<img class="img-responsive" src="{{ asset($latestArticle->image . '?w=737&h=466&fit=crop&version=' . $latestArticle->updated_at->format('dmyHis')) }}" alt="{{ $article->heading }}">
						</a>
					@endif
					<small>{{ $latestArticle->created_at->format('d. m. Y') }} / <a href="{{ url('/blog/kategorie/' . $latestArticle->category->alias) }}">{{ $latestArticle->category->name }}</a></small>
					<p>{{ str_limit(strip_tags($latestArticle->intro), 100) }}</p>
				</li>
				@endforeach
			</ul>

			<div class="headline-v2"><h2>Tagy</h2></div>
			<ul class="list-inline tags-v2 margin-bottom-50">
				@foreach ($tags as $tag)
					<li><a href="{{ url('/blog/tag/' . $tag->alias) }}">{{ $tag->name }}</a></li>
				@endforeach
			</ul>
		</div>
	</div>
</div>
@endsection
