@extends('FE.layouts.app', [
	'title' => menu(4, $menu),
])

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<h1 class="pull-left">{{ menu(4, $menu) }}</h1>
		<ul class="pull-right breadcrumb">
			<li><a href="{{ url('/') }}">{{ menu(1, $menu) }}</a></li>
			<li class="active">{{ menu(4, $menu) }}</li>
		</ul>
	</div>
</div>

<div class="container content">
	<div class="margin-bottom-40">
		{!! $content !!}
	</div>

	<div class="row">
		@foreach ($backgrounds as $background)
			<div class="col-sm-2 col-md-4 grid-image text-center">
				<a href="{{ asset($background->image . '?w=1920&&version=' . $background->updated_at->format('dmyHis')) }}" class="fancybox" data-rel="backgrounds" title="{{ $background->name }}">
					<img class="img-responsive" src="{{ asset($background->image . '?w=735&h=464&fit=crop&version=' . $background->updated_at->format('dmyHis')) }}" alt="{{ $background->name }}">
					<h2 class="heading">{{ $background->name }}</h2>
				</a>
				{!! $background->description !!}
			</div>
			@if (($loop->index + 1)  % 3 === 0)
				<div class="clearfix visible-md visible-lg"></div>
			@endif
			@if (($loop->index + 1) % 2 === 0)
				<div class="clearfix visible-sm"></div>
			@endif
		@endforeach
	</div>
</div>
@endsection
