<!DOCTYPE html>
<!--[if IE 8]> <html lang="cs" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="cs" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="cs"> <!--<![endif]-->
<head>
	<title>{{ $title }} | {{ config('app.name') }}</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	@if (isset($og))
	<meta property="fb:admins" content="jamesjohnjimbo">
	<meta property="og:locale" content="cs_CZ">
	<meta property="og:site_name" content="{{ config('app.name') }}">
		@if (isset($og['image']) && $og['image'])
		<meta property="og:image" content="{{ $og['image'] }}">
		@endif
	<meta property="og:title" content="{{ $og['title'] }}">
	<meta property="og:description" content="{{ $og['description'] }}">
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ request()->url() }}">
	@endif

	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

	<link rel="stylesheet" href="{{ asset('assets/fe/plugins/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/fe/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/fe/css/headers/header-v6.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/fe/css/footers/footer-v6.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/fe/plugins/line-icons/line-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/fe/plugins/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/fe/css/pages/page_pricing.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/fe/plugins/sky-forms-pro/skyforms/css/sky-forms.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/fe/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css') }}">
	<!--[if lt IE 9]><link rel="stylesheet" href="{{ asset('assets/fe/plugins/sky-forms-pro/skyforms/css/sky-forms-ie8.css') }}"><![endif]-->
	<link rel="stylesheet" href="{{ asset('assets/fe/plugins/fancybox/source/jquery.fancybox.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/vendor/jquery.datetimepicker-2.5.4.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/fe/css/custom.css') }}">
</head>

<body class="header-fixed">
<div class="wrapper">
	<div class="header-v6 header-classic-dark header-sticky">
		<div class="navbar mega-menu" role="navigation">
			<div class="container">
				<div class="menu-container">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<div class="navbar-brand">
						<a href="{{ url('/') }}">
							<img class="default-logo" src="{{ asset('assets/fe/img/logo.png') }}" alt="Logo">
							<img class="shrink-logo" src="{{ asset('assets/fe/img/logo-mini.png') }}" alt="Logo">
						</a>
					</div>
				</div>

				<div class="collapse navbar-collapse navbar-responsive-collapse">
					<div class="menu-container">
						<ul class="nav navbar-nav">
							@php
								$url = request()->segment(1);
							@endphp

							<li @if(!$url)class="active"@endif>
								<a href="{{ url('/') }}">{{ menu(1, $menu) }}</a>
							</li>
							<li @if($url === 'co-to-je-cvakmat')class="active"@endif>
								<a href="{{ url('/co-to-je-cvakmat') }}">{{ menu(8, $menu) }}</a>
							</li>
							<li @if($url === 'cenik')class="active"@endif>
								<a href="{{ url('/cenik') }}">{{ menu(2, $menu) }}</a>
							</li>
							<li @if($url === 'objednat-cvakmat')class="active"@endif>
								<a href="{{ url('/objednat-cvakmat') }}">{{ menu(3, $menu) }}</a>
							</li>
							<li @if($url === 'pozadi')class="active"@endif>
								<a href="{{ url('/pozadi') }}">{{ menu(4, $menu) }}</a>
							</li>
							<li @if($url === 'galerie')class="active"@endif>
								<a href="{{ url('/galerie') }}">{{ menu(5, $menu) }}</a>
							</li>
							<li>
								<a href="https://cvakbus.cz/" target="_blank">{{ menu(9, $menu) }}</a>
							</li>
							<li @if($url === 'blog')class="active"@endif>
								<a href="{{ url('/blog') }}">{{ menu(6, $menu) }}</a>
							</li>
							<li @if($url === 'kontakt')class="active"@endif>
								<a href="{{ url('/kontakt') }}">{{ menu(7, $menu) }}</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	@yield('content')

	<div class="footer-v6">
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<ul class="footer-socials list-inline">
							<li>
								<a href="https://www.facebook.com/CVAKMAT/" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top" title="Facebook" data-original-title="Facebook">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<li>
								<a href="{{ url('assets/fe/files/obchodni-podminky-cvakmat.pdf') }}" target="_blank">Obchodní podmínky</a>
							</li>
							<li>
								<a href="{{ url('assets/fe/files/podminky-ochrany-osobnich-udaju-gdpr-cvakmat.pdf') }}" target="_blank">Ochrana osobních údajů</a>
							</li>
						</ul>
					</div>
					<div class="col-md-3">
						<div class="text-right">
							{{ date('Y') }} © <a href="https://tomasjanecek.cz">Tomáš Janeček</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="{{ asset('assets/fe/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/fe/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/fe/plugins/back-to-top.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/fe/plugins/fancybox/source/jquery.fancybox.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vendor/jquery.datetimepicker.full-2.5.4.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/fe/js/custom.js') }}"></script>

<!--[if lt IE 9]>
<script src="{{ asset('assets/fe/plugins/respond.js') }}"></script>
<script src="{{ asset('assets/fe/plugins/html5shiv.js') }}"></script>
<script src="{{ asset('assets/fe/plugins/placeholder-IE-fixes.js') }}"></script>
<![endif]-->

</body>
</html>
