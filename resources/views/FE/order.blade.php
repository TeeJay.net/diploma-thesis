@extends('FE.layouts.app', [
	'title' => menu(3, $menu),
])

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<h1 class="pull-left">{{ menu(3, $menu) }}</h1>
		<ul class="pull-right breadcrumb">
			<li><a href="{{ url('/') }}">{{ menu(1, $menu) }}</a></li>
			<li class="active">{{ menu(3, $menu) }}</li>
		</ul>
	</div>
</div>

<div class="container content">

	@if (Session::has('message'))
		<div class="bg-success">
			<div class="panel panel-success">
				<div class="panel-heading form-success">{{ Session::get('message') }}</div>
			</div>
		</div>
	@endif
@php
if ($errors->any()) {
	/**
	* @var \Illuminate\Support\ViewErrorBag $errors
	*/
	$messages = $errors->getMessages();
}
@endphp
	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	@if (strip_tags($content))
		<div class="margin-bottom-40">
			{!! $content !!}
		</div>
	@endif

	<form method="post" class="sky-form">
		{!! csrf_field() !!}
		<header>Mám zájem</header>

		<fieldset>
			<h3>Kontaktní údaje</h3>
			<div class="row">
				<section class="col col-6">
					<label class="input {{ isset($messages['first_name']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-user"></i>
						<input type="text" name="first_name" placeholder="Jméno *" value="{{ old('first_name') }}">
						<div class="note note-error">
							{{ isset($messages['first_name']) ? $messages['first_name'][0] : '' }}
						</div>
					</label>
				</section>
				<section class="col col-6">
					<label class="input {{ isset($messages['last_name']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-user"></i>
						<input type="text" name="last_name" placeholder="Příjmení *" value="{{ old('last_name') }}">
						<div class="note note-error">
							{{ isset($messages['last_name']) ? $messages['last_name'][0] : '' }}
						</div>
					</label>
				</section>
			</div>
			<div class="row">
				<section class="col col-6">
					<label class="input {{ isset($messages['street']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-building"></i>
						<input type="text" name="street" placeholder="Ulice *" value="{{ old('street') }}">
						<div class="note note-error">
							{{ isset($messages['street']) ? $messages['street'][0] : '' }}
						</div>
					</label>
				</section>
				<section class="col col-6">
					<label class="input {{ isset($messages['house_number']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-building"></i>
						<input type="text" name="house_number" placeholder="Číslo popisné *" value="{{ old('house_number') }}">
						<div class="note note-error">
							{{ isset($messages['house_number']) ? $messages['house_number'][0] : '' }}
						</div>
					</label>
				</section>
				<section class="col col-6">
					<label class="input {{ isset($messages['city']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-university"></i>
						<input type="text" name="city" placeholder="Město *" value="{{ old('city') }}">
						<div class="note note-error">
							{{ isset($messages['city']) ? $messages['city'][0] : '' }}
						</div>
					</label>
				</section>
				<section class="col col-6">
					<label class="input {{ isset($messages['postcode']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-envelope"></i>
						<input type="text" name="postcode" placeholder="PSČ *" value="{{ old('postcode') }}">
						<div class="note note-error">
							{{ isset($messages['postcode']) ? $messages['postcode'][0] : '' }}
						</div>
					</label>
				</section>
			</div>
			<div class="row">
				<section class="col col-6">
					<label class="input {{ isset($messages['email']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-at"></i>
						<input type="text" name="email" placeholder="Email *" value="{{ old('email') }}">
						<div class="note note-error">
							{{ isset($messages['email']) ? $messages['email'][0] : '' }}
						</div>
					</label>
				</section>
				<section class="col col-6">
					<label class="input {{ isset($messages['phone']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-phone"></i>
						<input type="text" name="phone" placeholder="Telefon *" value="{{ old('phone') }}">
						<div class="note note-error">
							{{ isset($messages['phone']) ? $messages['phone'][0] : '' }}
						</div>
					</label>
				</section>
			</div>
		</fieldset>
		<fieldset>
			<h3>Údaje o akci</h3>

			<div class="row">
				<section class="col col-4">
					<label class="input {{ isset($messages['invoice_event']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-calendar"></i>
						<input type="text" name="invoice_event" placeholder="Název akce *" value="{{ old('invoice_event') }}">
						<div class="note note-error">
							{{ isset($messages['invoice_event']) ? $messages['invoice_event'][0] : '' }}
						</div>
					</label>
				</section>
				<section class="col col-4">
					<label class="select {{ isset($messages['background_id']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-calendar"></i>
						<select name="background_id">
							<option value="">Zvolte si pozadí *</option>
							@foreach(\App\Background::all() as $background)
								<option value="{{ $background->id }}" {{ old('background_id') == $background->id ? 'selected' : '' }}>{{ $background->name . ' ' . strip_tags($background->description) }}</option>
							@endforeach
						</select>
						<div class="note">
							<a href="{{ url('/pozadi') }}" target="_blank">Fotografie pozadí zde</a>
						</div>
						<div class="note note-error">
							{{ isset($messages['background_id']) ? $messages['background_id'][0] : '' }}
						</div>
					</label>
				</section>
				<section class="col col-4">
					<label class="select {{ isset($messages['print']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-calendar"></i>
						<select name="print">
							@foreach(['' => 'Zvolte si možnost tisku *'] + \App\Order::getPrintOptions() as $key => $value)
								<option value="{{ $key }}" {{ old('print') == $key ? 'selected' : '' }}>{{ $value }}</option>
							@endforeach
						</select>
						<div class="note note-error">
							{{ isset($messages['print']) ? $messages['print'][0] : '' }}
						</div>
					</label>
				</section>
			</div>
			<div class="row">
				<section class="col col-6">
					<label class="input {{ isset($messages['rental_from']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-calendar"></i>
						<input type="text" class="datetimepicker" name="rental_from" placeholder="Pronájem od *" value="{{ old('rental_from') }}">
						<div class="note note-error">
							{{ isset($messages['rental_from']) ? $messages['rental_from'][0] : '' }}
						</div>
					</label>
				</section>
				<section class="col col-6">
					<label class="input {{ isset($messages['rental_to']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-calendar"></i>
						<input type="text" class="datetimepicker" name="rental_to" placeholder="Pronájem do *" value="{{ old('rental_to') }}">
						<div class="note note-error">
							{{ isset($messages['rental_to']) ? $messages['rental_to'][0] : '' }}
						</div>
					</label>
				</section>
			</div>
		</fieldset>

		<fieldset>
			<label class="checkbox">
				<input type="checkbox" name="is_company" id="is_company" {{ old('is_company') ? 'checked' : '' }}><i></i>Firma
			</label>
			<div class="row" id="company-data" style="{{ old('is_company') ? '' : 'display:none' }}">
				<section class="col col-4">
					<label class="input {{ isset($messages['company']) ? 'state-error' : '' }}">
						<i class="icon-append fa fa-briefcase"></i>
						<input type="text" name="company" placeholder="Název firmy *" value="{{ old('company') }}">
						<div class="note note-error">
							{{ isset($messages['company']) ? $messages['company'][0] : '' }}
						</div>
					</label>
				</section>
				<section class="col col-4">
					<label class="input {{ isset($messages['ic']) ? 'state-error' : '' }}">
						<input type="text" name="ic" placeholder="IČO *" value="{{ old('ic') }}">
						<div class="note note-error">
							{{ isset($messages['ic']) ? $messages['ic'][0] : '' }}
						</div>
					</label>
				</section>
				<section class="col col-4">
					<label class="input {{ isset($messages['dic']) ? 'state-error' : '' }}">
						<input type="text" name="dic" placeholder="DIČ" value="{{ old('dic') }}">
						<div class="note note-error">
							{{ isset($messages['dic']) ? $messages['dic'][0] : '' }}
						</div>
					</label>
				</section>
			</div>
		</fieldset>

		<fieldset>
			<label class="checkbox">
				<input type="checkbox" name="invoice_data_are_same" id="invoice_data_are_same" {{ old('invoice_data_are_same') ? 'checked' : '' }}><i></i>Fakturační údaje jsou stejné
			</label>
			<div id="invoice_data" style="{{ old('invoice_data_are_same') ? 'display:none' : '' }}">
				<h3>Fakturační údaje</h3>
				<div class="row">
					<section class="col col-6">
						<label class="input {{ isset($messages['invoice_first_name']) ? 'state-error' : '' }}">
							<i class="icon-append fa fa-user"></i>
							<input type="text" name="invoice_first_name" placeholder="Jméno *" value="{{ old('invoice_first_name') }}">
							<div class="note note-error">
								{{ isset($messages['invoice_first_name']) ? $messages['invoice_first_name'][0] : '' }}
							</div>
						</label>
					</section>
					<section class="col col-6">
						<label class="input {{ isset($messages['invoice_last_name']) ? 'state-error' : '' }}">
							<i class="icon-append fa fa-user"></i>
							<input type="text" name="invoice_last_name" placeholder="Příjmení *" value="{{ old('invoice_last_name') }}">
							<div class="note note-error">
								{{ isset($messages['invoice_last_name']) ? $messages['invoice_last_name'][0] : '' }}
							</div>
						</label>
					</section>
				</div>
				<div class="row">
					<section class="col col-6">
						<label class="input {{ isset($messages['invoice_street']) ? 'state-error' : '' }}">
							<i class="icon-append fa fa-building"></i>
							<input type="text" name="invoice_street" placeholder="Ulice *" value="{{ old('invoice_street') }}">
							<div class="note note-error">
								{{ isset($messages['invoice_street']) ? $messages['invoice_street'][0] : '' }}
							</div>
						</label>
					</section>
					<section class="col col-6">
						<label class="input {{ isset($messages['invoice_house_number']) ? 'state-error' : '' }}">
							<i class="icon-append fa fa-building"></i>
							<input type="text" name="invoice_house_number" placeholder="Číslo popisné *" value="{{ old('invoice_house_number') }}">
							<div class="note note-error">
								{{ isset($messages['invoice_house_number']) ? $messages['invoice_house_number'][0] : '' }}
							</div>
						</label>
					</section>
					<section class="col col-6">
						<label class="input {{ isset($messages['invoice_city']) ? 'state-error' : '' }}">
							<i class="icon-append fa fa-university"></i>
							<input type="text" name="invoice_city" placeholder="Město *" value="{{ old('invoice_city') }}">
							<div class="note note-error">
								{{ isset($messages['invoice_city']) ? $messages['invoice_city'][0] : '' }}
							</div>
						</label>
					</section>
					<section class="col col-6">
						<label class="input {{ isset($messages['invoice_postcode']) ? 'state-error' : '' }}">
							<i class="icon-append fa fa-envelope"></i>
							<input type="text" name="invoice_postcode" placeholder="PSČ *" value="{{ old('invoice_postcode') }}">
							<div class="note note-error">
								{{ isset($messages['invoice_postcode']) ? $messages['invoice_postcode'][0] : '' }}
							</div>
						</label>
					</section>
				</div>
				<label class="checkbox">
					<input type="checkbox" name="is_company_invoice_data" id="is_company_invoice_data" {{ !old('invoice_data_are_same') && old('is_company_invoice_data') ? 'checked' : '' }}><i></i>Firma
				</label>
				<div class="row" id="invoice-company-data" style="{{ !old('invoice_data_are_same') && old('is_company_invoice_data') ? '' : 'display:none' }}">
					<section class="col col-4">
						<label class="input {{ isset($messages['invoice_company']) ? 'state-error' : '' }}">
							<i class="icon-append fa fa-briefcase"></i>
							<input type="text" name="invoice_company" placeholder="Název firmy *" value="{{ old('invoice_company') }}">
							<div class="note note-error">
								{{ isset($messages['invoice_company']) ? $messages['invoice_company'][0] : '' }}
							</div>
						</label>
					</section>
					<section class="col col-4">
						<label class="input {{ isset($messages['invoice_ic']) ? 'state-error' : '' }}">
							<input type="text" name="invoice_ic" placeholder="IČ *" value="{{ old('invoice_ic') }}">
							<div class="note note-error">
								{{ isset($messages['invoice_ic']) ? $messages['invoice_ic'][0] : '' }}
							</div>
						</label>
					</section>
					<section class="col col-4">
						<label class="input {{ isset($messages['invoice_dic']) ? 'state-error' : '' }}">
							<input type="text" name="invoice_dic" placeholder="DIČ" value="{{ old('invoice_dic') }}">
							<div class="note note-error">
								{{ isset($messages['invoice_dic']) ? $messages['invoice_dic'][0] : '' }}
							</div>
						</label>
					</section>
				</div>
			</div>
		</fieldset>

		<fieldset>
			<section>
				<label class="textarea">
					<i class="icon-append fa fa-comment"></i>
					<textarea rows="4" name="note" placeholder="Poznámka" value="{{ old('note') }}"></textarea>
				</label>
			</section>
		</fieldset>
		<footer class="text-right">
			<button type="submit" class="btn-u">Odeslat objednávku</button>
			<div class="progress"></div>
		</footer>
	</form>
</div>
@endsection
