@extends('FE.layouts.app', [
	'title' => $gallery->name,
])

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<h1 class="pull-left">{{ $gallery->name }}</h1>
		<ul class="pull-right breadcrumb">
			<li><a href="{{ url('/') }}">{{ menu(1, $menu) }}</a></li>
			<li><a href="{{ url('/galerie') }}">{{ menu(5, $menu) }}</a></li>
			<li class="active">{{ $gallery->name }}</li>
		</ul>
	</div>
</div>

<div class="container content">
	@if (strip_tags($content))
		<div class="margin-bottom-40">
			{!! $content !!}
		</div>
	@endif

	<div class="row">
		<form action="{{ url('galerie/' . $gallery->id . '/login') }}" class="sky-form" method="POST">
			{!! csrf_field() !!}
			<header>Vstup do galerie</header>
			<fieldset>
				<section>
					<label class="input {{ Session::has('message') ? 'has-error' : '' }}">
						<i class="icon-append fa fa-lock"></i>
						<input type="password" name="password" placeholder="Heslo" id="password">
							@if (Session::has('message'))
								<span class="help-block">
									<strong>{!! Session::get('message') !!}</strong>
								</span>
							@endif
					</label>
				</section>
			</fieldset>
			<footer>
				<button type="submit" class="btn-u">Vstoupit</button>
			</footer>
		</form>
	</div>
</div>
@endsection
