@extends('FE.layouts.app', [
	'title' => menu(5, $menu),
])

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<h1 class="pull-left">{{ menu(5, $menu) }}</h1>
		<ul class="pull-right breadcrumb">
			<li><a href="{{ url('/') }}">{{ menu(1, $menu) }}</a></li>
			<li class="active">{{ menu(5, $menu) }}</li>
		</ul>
	</div>
</div>

<div class="container content">
	@foreach ($galleries as $gallery)
		@php
			$galleryLink = url('/galerie/' . $gallery->alias);
		@endphp

		<div class="col-sm-2 col-md-4 grid-image text-center">
			<a href="{{ $galleryLink }}" title="{{ $gallery->name }}">
				@if (!$gallery->password && $gallery->photos)
					<img class="img-responsive" src="{{ asset($gallery->photos[0] . '?w=735&h=464&fit=crop&version=' . $gallery->updated_at->format('dmyHis')) }}" alt="{{ $gallery->name }}">
				@else
					<img class="img-responsive" src="{{ asset('assets/img/gallery-placeholder.png') }}" alt="{{ $gallery->heading }}">
				@endif
				<h2 class="heading">{{ $gallery->name }}</h2>
				<h3 class="heading">{{ $gallery->created_at->format('d. m. Y') }}</h3>
			</a>
			{!! $gallery->description !!}
		</div>
		@if (($loop->index + 1)  % 3 === 0)
			<div class="clearfix visible-md visible-lg"></div>
		@endif
		@if (($loop->index + 1) % 2 === 0)
			<div class="clearfix visible-sm"></div>
		@endif
	@endforeach

	<div class="clearfix"></div>
	<div class="pagination-wrapper">
		{!! $galleries->appends(request()->all())->render() !!}
	</div>
</div>

@endsection
