@extends('FE.layouts.app', [
	'title' => $gallery->name,
])

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<h1 class="pull-left">{{ $gallery->name }}</h1>
		<ul class="pull-right breadcrumb">
			<li><a href="{{ url('/') }}">{{ menu(1, $menu) }}</a></li>
			<li><a href="{{ url('/galerie') }}">{{ menu(5, $menu) }}</a></li>
			<li class="active">{{ $gallery->name }}</li>
		</ul>
	</div>
</div>

<div class="container content">
	@if (strip_tags($gallery->description))
		<div class="margin-bottom-40">
			{!! $gallery->description !!}
		</div>
	@endif

	<div class="row gallery-row">
		@foreach($gallery->photos as $photo)
			<div class="col-sm-2 col-md-4 gallery-photo">
				<a href="{{ asset($photo . '?w=1920&&version=' . $gallery->updated_at->format('dmyHis')) }}" class="fancybox" data-rel="gallery">
					<img class="img-responsive" src="{{ asset($photo . '?w=735&h=464&fit=crop&version=' . $gallery->updated_at->format('dmyHis')) }}" alt="{{ $gallery->name }}">
				</a>
			</div>
			@if (($loop->index + 1)  % 3 === 0)
				<div class="clearfix visible-md visible-lg"></div>
			@endif
			@if (($loop->index + 1) % 2 === 0)
				<div class="clearfix visible-sm"></div>
			@endif
		@endforeach
	</div>
</div>
@endsection
