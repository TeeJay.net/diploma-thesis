@php
	echo BootForm::open([
		'update' => 'BE\OrdersController@update' . ($fieldName == 'invoice_html' ? 'InvoicePdf' : 'ContractPdf'),
		'method' => 'PUT'
	]);
@endphp

	<div class="col-xs-12">
		<div class="form-group">
			<label for="invoice_html">{{ trans('models.orders.fields.' . $fieldName) }}</label>
			<textarea class="form-control" id="{{ $fieldName }}" name="{{ $fieldName }}" cols="50" rows="50">
				{{ $order->$fieldName }}
			</textarea>
		</div>

		<div class="form-group">
			<input class="btn btn-primary" type="submit" value="{{ trans('generic.actions.save') }}">
		</div>
	</div>
@php
	echo BootForm::close();
@endphp