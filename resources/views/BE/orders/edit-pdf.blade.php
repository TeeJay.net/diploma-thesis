@extends('BE.layouts.app', [
	'title'			=> trans('models.orders.actions.edit_' . $itemName . '_pdf'),
	'breadcrumbs'	=> [
		'/admin/orders'							=> trans('models.orders.plural'),
		'/admin/orders/' . $order->id . '/edit'	=> $order->name,
	],
])

@section('actions')
	<a href="{{ url('admin/' . modelUrl($order) . '/' . $itemName . '-pdf') }}" class="btn btn-primary" target="_blank">
		<i class="fa fa-file-pdf-o"></i> {{ trans('models.orders.actions.download_pdf') }}
	</a>
	<a href="{{ url('admin/' . modelUrl($order) . '/refresh-' . $itemName . '-pdf') }}" class="btn btn-danger">
		<i class="fa fa-file-pdf-o"></i> {{ trans('models.orders.actions.refresh_' . $itemName . '_pdf') }}
	</a>
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content clearfix">

					@include('BE.components.errors')
					@include('BE.orders.pdf-form', [
						'fieldName' => $itemName == 'invoice' ? 'invoice_html' : 'contract_html',
					])

				</div>
			</div>
		</div>
	</div>
@endsection
