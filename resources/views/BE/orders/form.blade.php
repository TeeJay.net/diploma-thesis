@php

echo BootForm::open([
	'model'		=> $order,
	'store'		=> 'BE\OrdersController@store',
	'update'	=> 'BE\OrdersController@update',
]);
echo BootForm::text('company', trans('models.orders.fields.company'), isset($order) ? $order->company : null);
echo BootForm::text('first_name', trans('models.orders.fields.first_name'), isset($order) ? $order->first_name : null, [
	'class' => 'required',
]);
echo BootForm::text('last_name', trans('models.orders.fields.last_name'), isset($order) ? $order->last_name : null, [
	'class' => 'required',
]);
echo BootForm::text('street', trans('models.orders.fields.street'), isset($order) ? $order->street : null, [
	'class' => 'required',
]);
echo BootForm::text('house_number', trans('models.orders.fields.house_number'), isset($order) ? $order->house_number : null, [
	'class' => 'required',
]);
echo BootForm::text('city', trans('models.orders.fields.city'), isset($order) ? $order->city : null, [
	'class' => 'required',
]);
echo BootForm::text('postcode', trans('models.orders.fields.postcode'), isset($order) ? $order->postcode : null, [
	'class' => 'required',
]);
echo BootForm::email('email', trans('models.orders.fields.email'), isset($order) ? $order->email : null, [
	'class' => 'required',
]);
echo BootForm::text('phone', trans('models.orders.fields.phone'), isset($order) ? $order->phone : null, [
	'class' => 'required',
]);
echo BootForm::text('rental_from', trans('models.orders.fields.rental_from'), isset($order) ? $order->rental_from->format('d.m.Y H:i') : null, [
	'class' => 'datetimepicker required',
]);
echo BootForm::text('rental_to', trans('models.orders.fields.rental_to'), isset($order) ? $order->rental_to->format('d.m.Y H:i') : null, [
	'class' => 'datetimepicker required',
]);
echo BootForm::select('background_id', trans('models.orders.fields.background'), ['' => ''] + \App\Background::all()->pluck('name', 'id')->all(), isset($order) ? $order->background_id : null, [
	'class' => 'chosen-select required',
	'data-placeholder' => trans('models.orders.actions.choose_background'),
]);
echo BootForm::select('print', trans('models.orders.fields.rental_type'), ['' => ''] + \App\Order::getPrintOptions(), isset($order) ? $order->rental_type : null, [
	'class' => 'chosen-select',
	'data-placeholder' => trans('models.orders.actions.choose_print'),
]);
echo BootForm::textarea('note', trans('models.orders.fields.note'), isset($order) ? $order->note : null, [
	'rows' => 3,
]);
echo BootForm::text('distance', trans('models.orders.fields.distance'), isset($order) ? $order->distance : null);
echo BootForm::text('delivery_price', trans('models.orders.fields.delivery_price'), isset($order) ? $order->delivery_price : null);
echo BootForm::text('final_price', trans('models.orders.fields.final_price'), isset($order) ? $order->final_price : null, [
	'class' => 'required',
]);
echo BootForm::text('final_price_words', trans('models.orders.fields.final_price_words'), isset($order) ? $order->final_price_words : null);

@endphp
<br>
<hr>
<br>
@php

	echo BootForm::text('invoice_event', trans('models.orders.fields.invoice_event'), isset($order) ? $order->invoice_event : null);
	echo BootForm::text('invoice_number', trans('models.orders.fields.invoice_number'), isset($order) ? $order->invoice_number : null);
	echo BootForm::text('invoice_vs', trans('models.orders.fields.invoice_vs'), isset($order) ? $order->invoice_vs : null);
	echo BootForm::text('invoice_date_created', trans('models.orders.fields.invoice_date_created'), isset($order->invoice_date_created) ? $order->invoice_date_created->format('d.m.Y') : null, [
		'class' => 'datepicker',
	]);
	echo BootForm::text('invoice_date_due', trans('models.orders.fields.invoice_date_due'), isset($order->invoice_date_due) ? $order->invoice_date_due->format('d.m.Y') : null, [
		'class' => 'datepicker',
	]);
	echo BootForm::text('invoice_first_name', trans('models.orders.fields.invoice_first_name'), isset($order) ? $order->invoice_first_name : null);
	echo BootForm::text('invoice_last_name', trans('models.orders.fields.invoice_last_name'), isset($order) ? $order->invoice_last_name : null);
	echo BootForm::text('invoice_street', trans('models.orders.fields.invoice_street'), isset($order) ? $order->invoice_street : null);
	echo BootForm::text('invoice_house_number', trans('models.orders.fields.invoice_house_number'), isset($order) ? $order->invoice_house_number : null);
	echo BootForm::text('invoice_city', trans('models.orders.fields.invoice_city'), isset($order) ? $order->invoice_city : null);
	echo BootForm::text('invoice_postcode', trans('models.orders.fields.invoice_postcode'), isset($order) ? $order->invoice_postcode : null);
	echo BootForm::text('invoice_ic', trans('models.orders.fields.invoice_ic'), isset($order) ? $order->invoice_ic : null);
	echo BootForm::text('invoice_dic', trans('models.orders.fields.invoice_dic'), isset($order) ? $order->invoice_dic : null);
@endphp

<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		@include('BE.components.checkbox', [
			'name' => 'confirmed',
			'label' => trans('models.orders.fields.confirmed'),
			'checked' => isset($order) ? $order->confirmed : null,
		])
	</div>
</div>

@php
echo BootForm::submit(trans('generic.actions.save'));
echo BootForm::close();
@endphp