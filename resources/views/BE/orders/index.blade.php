@extends('BE.layouts.app', [
	'title'			=> trans('models.orders.plural'),
	'breadcrumbs'	=> [
	],
])

@section('actions')
	<a href="{{ url('admin/orders/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> {{ trans('models.orders.actions.create') }}</a>
@endsection

@section('content')

	@include('BE.components.calendar.month', ['calendar' => $calendar])

	{!! $dataTable->render() !!}

@endsection
