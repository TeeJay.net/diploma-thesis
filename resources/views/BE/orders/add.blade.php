@extends('BE.layouts.app', [
	'title'			=> trans('models.orders.actions.create'),
	'breadcrumbs'	=> [
		'/admin/orders'	=> trans('models.orders.plural'),
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.orders.form')

				</div>
			</div>
		</div>
	</div>
@endsection
