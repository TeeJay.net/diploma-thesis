<style>
	body {
		font-size: 13px;
		line-height: 2.0;
	}

	.big {
		font-size: 20px;
	}

	h2 {
		text-align: center;
	}

	table {
		width: 100%;
	}
	.w25
	{
		width: 25% ;
	}
	.w50
	{
		width: 50% ;
	}

</style>


<h1><u>SMLOUVA O NÁJMU MOVITÉ VĚCI</u></h1>
<p>
	CVAKMAT, s.r.o.<br>
	IČ: 04260996<br>
	se sídlem Zeyerova 693/4, Pražské Předměstí, 500 02 Hradec Králové<br>
	BÚ: 2200846763/2010<br>
	Zastoupená jednatelem Jakubem Misíkem<br>
	na straně jedné (dále jen jako „pronajímatel“)<br>
</p>
<p>
	a
</p>
<p>
	[firma_nebo_jmeno_odberatele], [ulice_a_cp], [mesto_a_psc]<br>
	IČ: [ico]<br>
	Kterou zastupuje [jmeno_odberatele]<br>
	na straně druhé (dále jen jako „nájemce“)<br>
</p>

<h2>I.</h2>
<p>Pronajímatel prohlašuje, že je výlučným vlastníkem předmětu nájmu, tedy CVAKMATu a všech jeho součástí a
	příslušenství, všech dodaných rekvizit, plátna, a zavazuje se přenechat tyto nezuživatelné movité věci k dočasnému
	užívání nájemci, a nájemce se zavazuje zaplatit za to pronajímateli níže sjednané nájemné.
	Pronajímatel zajistí tiskárnu, kterou mohou návštěvníci po dobu pronájmu využívat k tisku svých fotografií
	ve formátu 10x15 cm, tisk fotografií je neomezený a pro všechny návštěvníky akce je zdarma.
	Všechny fotografie v digitální podobě budou poskytnuty nájemci nejdéle pět dní po připsání nájemného na účet pronajímatele.</p>
<h2>II.</h2>
<p>Pronajímatel přenechává předmět nájmu na dobu určitou od [pronajem_od_datum_cas] [pronajem_do_datum_cas].
	Po dohodě s obsluhou na místě lze prodloužit dobu pronájmu – cena za každou započatou hodinu pronájmu činí 2000 Kč
	(slovy dva tisíce korun českých). Částka za prodloužení pronájmu bude fakturována spolu s nájemným a případným
	cestovným.</p>
<h2>III.</h2>
<p>Nájemné bylo mezi smluvními stranami sjednáno ve výši [finalni_cena] (slovy [finalni_cena_slovy] korun
	českých). Nájemce je povinen zaplatit nájemné na bankovní účet pronajímatele č.ú. 2200846763/2010 nejdéle 5
	pracovních dnů po události.
	V rámci smluvního nájemného je zahrnuto dopravné do 15 km od adresy Přemyslova 382, 500 08 Hradec Králové. Každý
	další kilometr do a následně z místa pronájmu je počítán pevnou cenou 6 Kč/km. Případná cena za dopravu bude
	součástí vystavené faktury.
	Smluvní strany dohodly se tak, že dnem úhrady nájemného rozumí se den, kdy budou připsány finanční prostředky na
	bankovním účtu pronajímatele.</p>
<h2>IV.</h2>
a) Povinnosti pronajímatele<br>
<br>
Nájemní smlouva pronajímatele zavazuje:<br>
- přenechat předmět nájmu nájemci tak, aby ho mohl užívat k ujednanému účelu<br>
- zajistit zaškolenou obsluhu, která předmět dopraví, postaví a bude se o něj starat<br>
- přivézt pozadí č. [cislo_pozadi]<br>
<br>
b) Povinnosti nájemce:<br>
- vyhradit pro CVAKMAT zastřešený interiérový prostor 3x3x2,5 metru s přívodem elektrického proudu<br>
- připravit na místě stůl na rekvizity<br>
- vyhradit volné nezpoplatněné parkovací místo co nejblíže vchodu<br>
- užívat věc jako řádný hospodář k účelu sjednanému, případně obvyklému, a zaplatit nájemné dle této smlouvy sjednané článkem III
<br>
- oznámit pronajímateli, že věc má vadu, kterou je povinen odstranit pronajímatel, a to ihned poté, kdy ji zjistí nebo kdy při pečlivém užívání věci zjistit mohl
<br>
- ukáže-li se během nájmu potřeba provést nezbytnou opravu věci, kterou nelze odložit na dobu po skončení nájmu, musí ji nájemce strpět, i když mu provedení opravy způsobí obtíže nebo omezí užívání věci
</p>
<h2>V.</h2>
<p>Umožní-li nájemce užívat věc třetí osobě, odpovídá pronajímateli za jednání této osoby stejně, jako kdyby věc užíval
	sám.</p>
<h2>VI.</h2>
<p>Při skončení nájmu je nájemce povinen odevzdat pronajímateli věc v místě, kde ji převzal, a v takovém stavu, v jakém
	byla v době, kdy ji převzal.
	Pokud bude předmět nájmu poškozen, uhradí škodu člověk, který ji způsobil.</p>
<h2>VII.</h2>
<h2>Závěrečná ustanovení</h2>
<p>Práva a povinnosti stran touto smlouvou neupravené se řídí účinnou právní úpravou zákona č. 89/2012 Sb., občanským
	zákoníkem a předpisy s ním souvisejícími.
	Tato smlouva je vyhotovena ve dvou stejnopisech, z nichž každá ze smluvních stran obdrží po jednom.
	Smluvní strany prohlašují, že tato smlouva byla sepsána na základě pravé a svobodné vůle, nikoli v tísni, či za
	nápadně nevýhodných podmínek a na důkaz správnosti připojují své vlastnoruční podpisy.
	Tato smlouva nabývá účinnosti okamžikem podpisu všemi jejími účastníky.</p>

<p>V Hradci Králové dne [datum_vystaveni]</p>

<table>
	<tr>
		<td class="w50"><img src="[assets_path]assets/img/signature-pdf.png" /></td>
		<td class="w50">........................................</td>
	</tr>
	<tr>
		<td class="w50">Jakub Misík</td>
		<td class="w50">[jmeno_odberatele]</td>
	</tr>
	<tr>
		<td class="w50">pronajímatel</td>
		<td class="w50">nájemce</td>
	</tr>
</table>