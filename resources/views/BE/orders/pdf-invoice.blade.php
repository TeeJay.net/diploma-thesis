<style>
	body {
		font-size: 13px;
	}
	.big {
		font-size: 20px;
	}
	table {
		width: 100%;
	}
	.w25 {
		width: 25%;
	}
</style>
<table>
	<tbody>
	<tr>
		<td class="w25">&nbsp;</td>
		<td class="w25">&nbsp;</td>
		<td class="w25 big">Faktura</td>
		<td class="w25 big">[cislo_faktury]</td>
	</tr>
	<tr>
		<td colspan="4"><hr /></td>
	</tr>
	<tr>
		<td colspan="2">Dodavatel</td>
		<td colspan="2">Odběratel</td>
	</tr>
	<tr>
		<td colspan="4"><br /></td>
	</tr>
	<tr>
		<td colspan="2"><img src="[assets_path]assets/img/logo-pdf.png" /></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4"><br /></td>
	</tr>
	<tr>
		<td colspan="2"><strong>CVAKMAT, s.r.o.</strong></td>
		<td colspan="2">[jmeno_odberatele]</td>
	</tr>
	<tr>
		<td colspan="2">Zeyerova 693</td>
		<td colspan="2">[ulice_a_cp]</td>
	</tr>
	<tr>
		<td colspan="2">500 02 Hradec Králové</td>
		<td colspan="2">[mesto_a_psc]</td>
	</tr>
	<tr>
		<td colspan="4"><br /></td>
	</tr>
	<tr>
		<td>IČO: 04260996</td>
		<td>nejsme plátci DPH</td>
		<td colspan="2">IČO: [ico]</td>
	</tr>
	<tr>
		<td colspan="2">C 35511 Vedená u Krajského soudu v Hradci Králové</td>
		<td colspan="2">DIČ: [dic]</td>
	</tr>
	<tr>
		<td colspan="4"><br /><hr /></td>
	</tr>
	<tr>
		<td colspan="2"><strong>Platební podmínky</strong></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4"><br /></td>
	</tr>
	<tr>
		<td>Forma úhrady:</td>
		<td>převodem na účet</td>
		<td>Datum vystavení:</td>
		<td>[datum_vystaveni]</td>
	</tr>
	<tr>
		<td>Bankovní spojení:</td>
		<td>Fio banka</td>
		<td><strong>Datum splatnosti:</strong></td>
		<td><strong>[datum_splatnosti]</strong></td>
	</tr>
	<tr>
		<td>Číslo účtu:</td>
		<td><strong>2200846763 / 2010</strong></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Variabilní symbol:</td>
		<td>[vs]</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4"><br /><hr /></td>
	</tr>
	<tr>
		<td colspan="3"><strong>Fakturujeme Vám:</strong></td>
		<td>Cena</td>
	</tr>
	<tr>
		<td colspan="4"><br /></td>
	</tr>
	<tr>
		<td colspan="3">Zajištění fotokoutku na akci [akce] ze dne [pronajem_od]</td>
		<td><strong>[finalni_cena] Kč</strong></td>
	</tr>
	<tr>
		<td colspan="3">Doprava</td>
		<td><strong>[doprava_cena] Kč</strong></td>
	</tr>
	<tr>
		<td colspan="4"><br /></td>
	</tr>
	<tr>
		<td colspan="3"><strong>Celkem</strong></td>
		<td><strong>[cena_dohromady] Kč</strong></td>
	</tr>
	<tr>
		<td colspan="4"><br /><hr /></td>
	</tr>
	<tr>
		<td colspan="4">
			<strong>Vystavil:</strong> Jakub Misík<br /><br />
			<img src="[assets_path]assets/img/signature-pdf.png" />
		</td>
	</tr>
	</tbody>
</table>
