@extends('BE.layouts.app', [
	'title'			=> trans('models.orders.actions.edit'),
	'breadcrumbs'	=> [
		'/admin/orders'							=> trans('models.orders.plural'),
		'/admin/orders/' . $order->id . '/edit'	=> $order->name,
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.orders.form')

				</div>
			</div>
		</div>
	</div>
@endsection
