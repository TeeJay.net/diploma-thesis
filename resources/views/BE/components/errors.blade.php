@if ($errors->any())
	<div class="alert alert-danger">
		<strong>{{trans('generic.something_went_wrong')}}!</strong>

		<br>
		<br>

		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
