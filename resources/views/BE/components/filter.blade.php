@if ($isFirstFilter)
<form class="filter-form {{ $modelPlural }} clearfix" action="{{ request()->url() }}" method="GET">
@endif

	<div class="form-group filter-form-group" style="width: {{ $relativeWidth }}%;">

		@if (isset($quickfilter))
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-filter"></i></span>
				<input type="text" class="quickfilter form-control" name="quickfilter" id="quickfilter" data-target="#{{ $modelPlural }}" value="{{ $value }}" data-autofocus>
			</div>
		@endif

		<?php /** @var \App\DataTables\Filters\DateRangeFilter $dateRangeFilter */ ?>
		@if (isset($dateRangeFilter))
			<input type="hidden" name="from" value="{{ $defaultDateRange['from'] }}">
			<input type="hidden" name="to" value="{{ $defaultDateRange['to'] }}">
			<div>
				<div id="daterangepicker"
					 class="form-control input-like"
					 data-from="{{ $defaultDateRange['from'] }}"
					 data-to="{{ $defaultDateRange['to'] }}"
					 data-date-ranges="{{ $dateRanges }}"
					 data-open-side="{{ $openSide }}">
				</div>
			</div>
		@endif

		@if (isset($dropdownFilter))
			@if ($label)
				<label for="{{ $modelPlural }}-{{ $queryStringParamName }}">
					{{ $label }}
				</label>
			@endif
			<select class="form-control chosen-select dropdown-filter" id="{{ $modelPlural }}-{{ $queryStringParamName }}" name="{{ $queryStringParamName }}" data-placeholder="{{ $prompt }}">
				@foreach ($options as $key => $option)
					<option value="{{ $key }}" @if ((is_numeric($key) && is_numeric($selected) && $key == $selected) || (is_string($key) && is_string($selected) && $key == $selected)) selected @endif>{{ $option }}</option>
				@endforeach
			</select>
		@endif

	</div>

@if ($isLastFilter)
		@foreach($extraQueryParameters as $name => $value)
			<input type="hidden" name="{{ $name }}" value="{{ $value }}">
		@endforeach
</form>
@endif
