{{--
	Generates HTML for a nicely styled checkbox

	Required parameters
		string $name
		array $options
	Optional parameters
		array $checked
		string $heading
		string $class
		string $wrapperClass
		string $headingClass
		string $groupWrapperClass
--}}

@if (isset($heading) && $heading)
	<label class="{{ $headingClass ?? '' }}">{{ $heading }}</label>
@endif

@if (isset($groupWrapperClass) && $groupWrapperClass)
	<div class="{{ $groupWrapperClass }}">
@endif

@foreach($options as $value => $label)
	@include('BE.components.checkbox', [
		'name' => $name,
		'value' => $value,
		'label' => $label,
		'checked' => isset($checked) && ((isset($checked[$value]) && $checked[$value]) || in_array($value, $checked)),
		'class' => $class ?? null,
		'wrapperClass' => isset($wrapperClass) && $wrapperClass ? $wrapperClass . ' m-b-xs' : 'm-b-xs',
	])
@endforeach

		@if (isset($groupWrapperClass) && $groupWrapperClass)
	</div>
@endif
