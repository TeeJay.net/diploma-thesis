{{--
	Generates HTML for a nicely styled checkbox

	Required parameters
		string $name
	Optional parameters
		string $id
		string $class
		string $value
		string $label
		string $wrapperClass
		bool $checked
		array $additionalAttributes
--}}
<div class="checkbox-wrapper {{ $wrapperClass ?? '' }}">
	<?php
	if (!isset($id)) {
		$id = strtolower(str_random(5));
	} ?>
	<input type="checkbox"
		   name="{{ $name }}"
		   class="{{ $class ?? '' }}"
		   id="{{ $id }}"
		   value="{{ $value ?? '1' }}"
			{{ isset($checked) && $checked ? 'checked="checked"' : '' }}
			@foreach ($additionalAttributes ?? [] as $attributeName => $value)
				{{ $attributeName }}="{{ $value }}"
			@endforeach
	>
	<label for="{{ $id }}"><span></span>{{ isset($label) && $label ? $label : '' }}</label>
</div>