<div class="calendar-wrapper">
	<table class="calendar">
		<tr class="month-header">
			<td class="arrow-left"><a href="{{ updateUrl(['month' => $calendar->getPreviousMonth()]) }}"><i class="fa fa-chevron-left"></i></a></td>
			<td class="month-year text-center" colspan="5">{{ trans('generic.' . strtolower($calendar->getMonthName())) . ' ' . $calendar->getYear() }}</td>
			<td class="arrow-right text-right"><a href="{{ updateUrl(['month' => $calendar->getNextMonth()]) }}"><i class="fa fa-chevron-right"></i></a></td>
		</tr>
		<tr class="week-header">
			@foreach($calendar->getMonthCalendarHeader() as $dayName)
				<td class="day">{{ $dayName }}</td>
			@endforeach
		</tr>

		@foreach($calendar->getMonthCalendar() as $week)
			<tr class="week">
				@foreach($week as $day)
					<td class="day {{ $calendar->belongsToThisMonth($day['date']) ? '' : 'grey' }}">
						{{ $day['date']->format('j') }}<br>
						@if (isset($day['events']))
							@foreach($day['events'] as $event)
								<div class="label {{ $event['class'] }}">
									{{ $event['name'] }}
								</div>
							@endforeach
						@endif
					</td>
				@endforeach
			</tr>
		@endforeach
	</table>
</div>
