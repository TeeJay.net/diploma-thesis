<div class="table-responsive datatable" id="datatable">
	@if ($paginator->total() > 0)

		<table class="table table-hover table-minimal">
			<thead>
			<tr>

				@foreach ($columns as $column)
					<th class="{{ $column->class ? $column->class : ''}}" style="width: {{ $columnWidths[$column->name] }}%;">

						<?php
						$orderingAppliedOnThisColumn = false;
						if ($column->orderBy && $request->has('order_by') && $request->has('order_dir') && $request->input('order_by') == $column->name) {
							$orderingAppliedOnThisColumn = true;
							if ($request->has('order_dir') && $request->input('order_dir') == 'asc') {
								$orderingOptions = [
									'order_by' => $column->name,
									'order_dir' => 'desc',
								];
							} else {
								$orderingOptions = [];
							}
						} else {
							$orderingOptions = [
								'order_by' => $column->name,
								'order_dir' => 'asc',
							];
						} ?>

						@if ($column->orderBy)
							<a href="{{ updateUrl(array_merge($orderingOptions, ['page' => 1]), $orderingOptions ? [] : ['order_by', 'order_dir']) }}">
								@endif

								@if (!$column->heading)
									@if ($column->name == 'id')
										{{ trans('generic.id') }}
									@elseif ($column->name == 'actions')
										{{ trans('generic.options') }}
									@elseif ($column->name == $unreadColumnName)
										<i class="fa fa-eye"></i>
									@else
										{{ trans('models.' . $modelPlural . '.fields.' . $column->name) }}
									@endif
								@else
									{{ $column->heading }}
								@endif

								@if ($orderingAppliedOnThisColumn)
									<i class="fa fa-{{ $request->input('order_dir') == 'asc' ? 'sort-asc' : 'sort-desc' }}"></i>
								@endif

								@if ($column->orderBy)
							</a>
						@endif
					</th>
				@endforeach
			</tr>
			</thead>

			<tbody>
			@foreach ($paginator as $row)

				<tr class="{{ $bigLinkEnabled ? 'biglink' : '' }} {{$massActionsEnabled ? 'biglink-without-first-column' : ''}}">

					@if ($massActionsEnabled)
						<td>
							@include('components.checkbox', [
								'name' => 'id[]',
								'class' => 'check_row',
								'value' => $row->id,
							])
						</td>
					@endif

					@foreach ($columns as $i => $column)
						<td class="{{ $column->class ? $column->class : ''}}">

							<?php
							$value = $row->getAttributeWithDotSyntax($column->select);
							?>

							@if ($column->format)

								<span>{{ $column->format->__invoke($value, $row) }}</span>

							@elseif ($column->view)

								{!! $column->view->with('value', $value)->with('row', $row)->render() !!}

							@elseif (is_string($value) || is_numeric($value))

								<span>{{ $value }}</span>

							@elseif ($value instanceof \Illuminate\Database\Eloquent\Model)

								@if ($value->trashed())
									@include('BE.components.deleted-label', [ 'text' => $value->name, 'upper' => true ])
								@else
									{{ $value->name }}
								@endif

							@elseif ($value instanceof \Illuminate\Database\Eloquent\Collection)

								<?php
								// To display deleted name in the end of list, create separate collection for undeleted names and deleted names.
								$names = collect();
								$trashedNames = collect();
								$value->each(function (\Illuminate\Database\Eloquent\Model $item) use ($names, $trashedNames) {
									if ($item->trashed()) {
										$name = $item->name;
										$trashedNames->push($name);
									} else {
										$names->push($item->name);
									}
								});
								?>

								{{-- First list undeleted names --}}
								@if ($names->count())
									@foreach ($names as $name)
										{{ $name }}<br>
									@endforeach
								@endif
								{{-- Then deleted names --}}
								@if ($trashedNames->count())
									@foreach ($trashedNames as $key => $name)
										@include('BE.components.deleted-label', [
											'text' => $name,
											'upper' => true,
										])<br>
									@endforeach
								@endif
								{{-- Or a dash for nothing --}}
								@if (!$names->count() && !$trashedNames->count())
									-
								@endif

							@elseif ($value instanceof 	Illuminate\Support\Collection)

								@foreach ($value as $item)
									{{ $item }}<br>
								@endforeach

							@elseif ($value instanceof \Carbon\Carbon)

								{{ $value->format(str_contains($column->name, '_time') ? 'H:i' : 'd.m.Y') }}

							@elseif (method_exists($value, '__toString'))

								{{ $value }}

							@elseif ($column->class == $unreadColumnName && $highlightRowClosure && $highlightRowClosure->__invoke($row))

								<span class="unread-indicator"></span>

							@endif

							{{-- Link for the whole table row --}}
							@if ($i == 0 && isset($linkPrefix) && $bigLinkEnabled)
								<?php
								if (str_contains($linkPrefix, ['{', '}'])) {
									//								// Prefix is set to i.e. '/matter/{matter}/travel'
									$primaryStringParts = explode('{', $linkPrefix);
									$secondaryStringParts = explode('}', $primaryStringParts[1]);
									$relationshipName = $secondaryStringParts[0];
									// {matter} has to be replaced by matter ID based on the matter() relationship of the row
									$prefix = $primaryStringParts[0] . $row->$relationshipName->id . $secondaryStringParts[1];
								} else {
									$prefix = $linkPrefix;
								} ?>

								@if ($linkModal)
									<a href="{{ url($prefix . $row->id . $linkSuffix) }}" class="modal-link" data-no-instant></a>
								@elseif($linkDownload)
									<a href="{{ url($prefix . $row->id . $linkSuffix) }}" data-no-instant></a>
								@else
									<a href="{{ url($prefix . $row->id . $linkSuffix) }}"></a>
								@endif
							@endif
						</td>
					@endforeach

				</tr>
			@endforeach

			@if ($showSubtotals)
				<tr class="subtotals">
					@if ($massActionsEnabled)
						<td></td>
					@endif
					@foreach ($columns as $key => $column)
						<td {{ $column->class ? 'class=' . $column->class : '' }}>
							@if ($key === 0)
								<strong>{{ trans('generic.total') }}</strong>
							@else
								@if ($column->subtotal)
									{{ formatNumber($column->subtotal) }}
								@elseif ($column->subtotals)
									@foreach($column->subtotals as $subtotal)
										{{ $column->format->__invoke($subtotal['subtotal'], $subtotal['rowForFormat']) }}
										<br>
									@endforeach
								@endif
							@endif
						</td>
					@endforeach
				</tr>
			@endif

			</tbody>
		</table>

	@else
		<p>
			@if (@$anyFilterInUse)
				{{ trans('generic.no_search_results') }}
			@else
				{{ @$emptyTableMessage }}
			@endif
		</p>
	@endif

	@include('BE.components.pagination', [
		'request' => $request,
		'paginator' => $paginator,
	])
</div>
