<label class="btn btn-default">
	{{ trans('generic.actions.browse_or_drag') }}
	<input type="file" class="image-upload-with-preview" id="{{ $name }}" name="{{ $name }}" hidden>
</label>
<div class="upload-preview m-t-sm">
	@if ($path)
		<img class="img-preview img-responsive" alt="{{ trans('generic.image_preview') }}" src="{{ asset($path . '?w=768&version=' . $timestamp) }}">
	@elseif (isset($secondaryPath) && $secondaryPath)
		<img class="img-preview img-responsive" alt="{{ trans('generic.image_preview') }}" src="{{ asset($secondaryPath . '?w=768&version=' . $timestamp) }}">
	@else
		<p>{{ trans('generic.no_image') }}</p>
	@endif
</div>