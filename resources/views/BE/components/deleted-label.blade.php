{{--
$upper bool Defines if text in label should be upper or lower
$text string Text to display
--}}
<span class="text-muted">{{ $text }}</span> <span class="label label-default">{{ trans('generic.deleted') }}</span>
