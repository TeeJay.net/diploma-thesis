@if (Session::has('message'))
	<div class="toast hidden" data-toast-type="{{ Session::get('message_type') == 'warn' ? 'warning' : (Session::get('message_type') == 'error' ? 'error' : 'success') }}">
		{{ Session::get('message') }}
	</div>
@endif
