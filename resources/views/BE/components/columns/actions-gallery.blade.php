@extends('BE.components.columns.actions')

@php
	/*
		Two variables are available in Column templates
		@row is the current row of the current object (i.e. Task)
		@value is the raw value of this column of the current raw of the curernt object (i.e. Task's finished_at date)
	 */
@endphp

<a href="{{ url('admin/' . modelUrl($row) . '/edit-photos') }}" class="btn btn-xs btn-primary"  title="{{ trans('models.galleries.actions.edit_photos') }}" data-toggle="tooltip" data-placement="bottom">
	<i class="fa fa-picture-o"></i>
</a>
