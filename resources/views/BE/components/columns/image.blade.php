@php
/*
	Two variables are available in Column templates
	@row is the current row of the current object (i.e. Task)
	@value is the raw value of this column of the current raw of the curernt object (i.e. Task's creator)
 */
@endphp

@if ($value)
	@if ($value)
		<img alt="Obrázek {{ $row->name }}"
			 src="{{ asset($value . '?w=200&version=' . $row->updated_at->format('dmyHis')) }}"
			 data-datatable="{{ $row->name }}">
	@else
		<img alt="Obrázek {{ $row->name }}"
			 src="{{ asset('assets/img/image-placeholder.png') }}"
			 data-datatable="{{ $row->name }}">
	@endif
@endif