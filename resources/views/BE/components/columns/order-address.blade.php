@php
/*
	Two variables are available in Column templates
	@row is the current row of the current object (i.e. Task)
	@value is the raw value of this column of the current raw of the curernt object (i.e. Task's finished_at date)
 */

/**
 * @var \App\Order $row
 */

@endphp

{{ $row->street }} {{ $row->house_number }}<br>
{{ $row->postcode }} {{ $row->city }}<br>
