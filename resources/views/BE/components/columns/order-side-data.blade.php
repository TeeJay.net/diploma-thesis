@php
/*
	Two variables are available in Column templates
	@row is the current row of the current object (i.e. Task)
	@value is the raw value of this column of the current raw of the curernt object (i.e. Task's finished_at date)
 */

/**
 * @var \App\Order $row
 */

$printOptions = \App\Order::getPrintOptions();
if ($value == 'no') {
	$print = $printOptions['no'];
} else if ($value == 'print_price_per_photo') {
	$print = $printOptions['print_price_per_photo'];
} else {
	$print = $printOptions['print_free_for_all'];
}

@endphp

<span class="label label-default" title="{{ $print }}" data-toggle="tooltip" data-placement="bottom">{{ trans('models.orders.fields.print') }}</span>

@if ($row->invoice_number || $row->invoice_first_name || $row->invoice_last_name || $row->invoice_street
	|| $row->invoice_house_number || $row->invoice_city || $row->invoice_postcode || $row->invoice_ic
	|| $row->invoice_dic || $row->invoice_date_created || $row->invoice_date_due || $row->invoice_vs || $row->invoice_event)
	@php
		if ($row->invoice_event) {
			$title[] = trans('models.orders.fields.invoice_event') . ': ' . $row->invoice_event;
		}
		if ($row->invoice_number) {
			$title[] = trans('models.orders.fields.invoice_number') . ': ' . $row->invoice_;
		}
		if ($row->invoice_vs) {
			$title[] = trans('models.orders.fields.invoice_vs') . ': ' . $row->invoice_vs;
		}
		if ($row->invoice_date_created) {
			$title[] = trans('models.orders.fields.invoice_date_created') . ': ' . $row->invoice_date_created->format('d.m.Y');
		}
		if ($row->invoice_date_due) {
			$title[] = trans('models.orders.fields.invoice_date_due') . ': ' . $row->invoice_date_due->format('d.m.Y');
		}
		if ($row->invoice_first_name) {
			$title[] = trans('models.orders.fields.invoice_first_name') . ': ' . $row->invoice_first_name;
		}
		if ($row->invoice_last_name) {
			$title[] = trans('models.orders.fields.invoice_last_name') . ': ' . $row->invoice_last_name;
		}
		if ($row->invoice_street) {
			$title[] = trans('models.orders.fields.invoice_street') . ': ' . $row->invoice_street;
		}
		if ($row->invoice_house_number) {
			$title[] = trans('models.orders.fields.invoice_house_number') . ': ' . $row->invoice_house_number;
		}
		if ($row->invoice_city) {
			$title[] = trans('models.orders.fields.invoice_city') . ': ' . $row->invoice_city;
		}
		if ($row->invoice_postcode) {
			$title[] = trans('models.orders.fields.invoice_postcode') . ': ' . $row->invoice_postcode;
		}
		if ($row->invoice_ic) {
			$title[] = trans('models.orders.fields.invoice_ic') . ': ' . $row->invoice_ic;
		}
		if ($row->invoice_dic) {
			$title[] = trans('models.orders.fields.invoice_dic') . ': ' . $row->invoice_dic;
		}
	@endphp
	<span class="label label-default" title="{{ implode(', ', $title) }}" data-toggle="tooltip" data-placement="bottom">{{ trans('models.orders.fields.invoice_data') }}</span>
@endif

@if ($row->final_price_words)
	<span class="label label-default" title="{{ $row->final_price_words }}" data-toggle="tooltip" data-placement="bottom">{{ trans('models.orders.fields.final_price_words_short') }}</span>
@endif

@if ($row->gallery_id)
	<span class="label label-success" title="{{ trans('models.galleries.messages.created') }}" data-toggle="tooltip" data-placement="bottom">G</span>
@endif

@if ($row->invoice_html)
	<span class="label label-danger" title="{{ trans('models.orders.fields.invoice') }}" data-toggle="tooltip" data-placement="bottom"><a href="{{ url('admin/' . modelUrl($row) . '/invoice-pdf') }}" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;{{ trans('models.orders.fields.invoice_short') }}</a></span>
@endif

@if ($row->contract_html)
	<span class="label label-danger" title="{{ trans('models.orders.fields.contract') }}" data-toggle="tooltip" data-placement="bottom"><a href="{{ url('admin/' . modelUrl($row) . '/contract-pdf') }}" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;{{ trans('models.orders.fields.contract_short') }}</a></span>
@endif

@foreach($galleries as $galleryId => $orderId)
	@if ($orderId === $row->id)
		<span class="label label-primary" title="{{ trans('models.galleries.singular') }}" data-toggle="tooltip" data-placement="bottom">
			<a href="{{ url('admin/galleries/' . $galleryId . '/edit-photos') }}">
				<i class="fa fa-picture-o"></i>
			</a>
		</span>
	@endif
@endforeach
