@php
/*
	Two variables are available in Column templates
	@row is the current row of the current object (i.e. Task)
	@value is the raw value of this column of the current raw of the curernt object (i.e. Task's creator)
 */
@endphp

@if ($value)
	<img alt="Avatar {{ $row->name }}" class="img-circle" src="{{ asset($value . '?w=48&version=' . $row->updated_at->format('dmyHis')) }}" data-datatable="{{ $row->name }}" title="{{ $row->name }}" data-toggle="tooltip" data-placement="bottom">
@else
	<img alt="Avatar {{ $row->name }}" class="img-circle" src="{{ asset('assets/img/profile-picture-placeholder.png') }}" style="max-width: 48px;" data-datatable="{{ $row->name }}" title="{{ $row->name }}" data-toggle="tooltip" data-placement="bottom">
@endif
