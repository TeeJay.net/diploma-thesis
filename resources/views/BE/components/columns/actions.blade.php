@php
/*
	Two variables are available in Column templates
	@row is the current row of the current object (i.e. Task)
	@value is the raw value of this column of the current raw of the curernt object (i.e. Task's finished_at date)
 */
@endphp

<a href="{{ url('admin/' . modelUrl($row) . '/edit') }}" class="btn btn-xs btn-success" title="{{ trans('generic.actions.edit') }}" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-pencil"></i></a>
<form action="{{ url('admin/' . modelUrl($row)) }}" method="POST" class="delete-form ajax">
	{{ csrf_field() }}
	{{ method_field('DELETE') }}
	<button class="btn btn-xs btn-danger" title="{{ trans('generic.actions.delete') }}" data-toggle="tooltip" data-placement="bottom">
		<i class="fa fa-trash-o"></i>
	</button>
</form>
