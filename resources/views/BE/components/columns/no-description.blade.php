@php
/*
	Two variables are available in Column templates
	@row is the current row of the current object (i.e. Task)
	@value is the raw value of this column of the current raw of the curernt object (i.e. Task's finished_at date)
 */
@endphp

@if (empty($value))
	<span class="no-description" data-datatable="{{ trans('generic.no_description') }}">{{ trans('generic.no_description') }}</span>
@else
	<span data-datatable="{{ $value }}">{{ $value }}</span>
@endif
