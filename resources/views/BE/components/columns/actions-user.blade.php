@extends('BE.components.columns.actions')

@php
	/*
		Two variables are available in Column templates
		@row is the current row of the current object (i.e. Task)
		@value is the raw value of this column of the current raw of the curernt object (i.e. Task's finished_at date)
	 */
@endphp

<a href="{{ url('admin/' . modelUrl($row) . '/change-password') }}" class="btn btn-xs btn-warning">{{ trans('models.users.actions.change_password') }}</a>
<a href="{{ url('admin/' . modelUrl($row) . '/change-avatar') }}" class="btn btn-xs btn-primary">{{ trans('models.users.actions.change_avatar') }}</a>
