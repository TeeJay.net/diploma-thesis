@extends('BE.components.columns.actions')

@php
	/*
		Two variables are available in Column templates
		@row is the current row of the current object (i.e. Task)
		@value is the raw value of this column of the current raw of the curernt object (i.e. Task's finished_at date)
	 */
@endphp

@if (!$row->confirmed)
	<form action="{{ url('admin/' . modelUrl($row) . '/confirm') }}" method="POST" class="confirm-form ajax">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		<button class="btn btn-xs btn-warning" title="{{ trans('models.orders.actions.confirm') }}" data-toggle="tooltip" data-placement="bottom">
			<i class="fa fa-check"></i>
		</button>
	</form>
@else
	<a href="{{ url('admin/' . modelUrl($row) . '/send-email-order-confirmed') }}" class="btn btn-xs btn-warning"  title="{{ trans('models.orders.actions.send_email_order_confirmed') }}" data-toggle="tooltip" data-placement="bottom">
		<i class="fa fa-envelope-o"></i>
	</a>
	@if (!in_array($row->id, $galleries))
		<a href="{{ url('admin/galleries/create?order_id=' . $row->id) }}" class="btn btn-xs btn-primary"  title="{{ trans('models.orders.actions.create_gallery') }}" data-toggle="tooltip" data-placement="bottom">
			<i class="fa fa-picture-o"></i>
		</a>
	@endif
	<a href="{{ url('admin/' . modelUrl($row) . '/edit-invoice-pdf') }}" class="btn btn-xs btn-danger"  title="{{ trans('models.orders.actions.edit_invoice_pdf') }}" data-toggle="tooltip" data-placement="bottom">
		<i class="fa fa-file-pdf-o"></i> F
	</a>
	<a href="{{ url('admin/' . modelUrl($row) . '/edit-contract-pdf') }}" class="btn btn-xs btn-danger"  title="{{ trans('models.orders.actions.edit_contract_pdf') }}" data-toggle="tooltip" data-placement="bottom">
		<i class="fa fa-file-pdf-o"></i> S
	</a>
@endif
