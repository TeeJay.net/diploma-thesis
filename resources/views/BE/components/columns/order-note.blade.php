@php
	/*
		Two variables are available in Column templates
		@row is the current row of the current object (i.e. Task)
		@value is the raw value of this column of the current raw of the curernt object (i.e. Task's finished_at date)
	 */

	/**
	 * @var \App\Order $row
	 */
@endphp

@if ($value)
	<span class="label label-primary" title="{{ $value }}" data-toggle="tooltip" data-placement="bottom">{{ trans('models.orders.fields.note') }}</span>
@endif