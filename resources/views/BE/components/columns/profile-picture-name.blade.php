@php
/*
	Two variables are available in Column templates
	@row is the current row of the current object (i.e. Task)
	@value is the raw value of this column of the current raw of the curernt object (i.e. Task's creator)
 */
@endphp

@if ($value)
	@include('BE.components.profile-picture', [
		'user'		=> $value,
		'size'		=> 32,
		'showName'	=> true,
	])
@endif