<div class="form-group filter-form-group">
	@if ($label)
		<label for="{{ $queryStringParamName }}">
			{{ $label }}
		</label>
	@endif
	<select class="form-control chosen-select dropdown-filter" id="{{ $queryStringParamName }}" name="{{ $queryStringParamName }}" data-placeholder="{{ $prompt }}">
		@foreach ($options as $key => $option)
			<option value="{{ $key }}" @if ((is_numeric($key) && is_numeric($selected) && $key == $selected) || (is_string($key) && is_string($selected) && $key == $selected)) selected @endif>{{ $option }}</option>
		@endforeach
	</select>
</div>
