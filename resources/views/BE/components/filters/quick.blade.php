<div class="form-group filter-form-group">
	<div class="input-group">
		<span class="input-group-addon"><i class="fa fa-filter"></i></span>
		<input type="text" class="quickfilter form-control" name="quickfilter" id="quickfilter" data-target=".datatable:last" value="{{ $value }}" data-autofocus>
	</div>
</div>
