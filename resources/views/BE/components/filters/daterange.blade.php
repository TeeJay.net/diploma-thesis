<div class="form-group filter-form-group">
	<input type="hidden" name="from" value="{{ $queryParameters['from'] }}">
	<input type="hidden" name="to" value="{{ $queryParameters['to'] }}">
	<div>
		<div class="date-range-picker form-control input-like"
			 data-date-ranges="{{ $dateRanges }}"
			 data-open-side="{{ $openSide }}">
		</div>
	</div>
</div>
