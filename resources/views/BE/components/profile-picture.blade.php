@php

/**
 * @var \App\User $user
 */
$size = $size ?? 32;
$showName = $showName ?? false;
if ($user->hasAvatar()) {
	$imageUrl = asset($user->avatar . '?w=' . $size . '&version=' . $user->updated_at->format('dmyHis'));
} else {
	$imageUrl = asset('images/avatars/profile-picture-placeholder.png?w=' . $size);
}
$name = $user->first_name . ' ' . $user->last_name;

@endphp

<img alt="Avatar {{ $name }}" class="img-circle" src="{{ $imageUrl }}" data-datatable="{{ $name }}" title="{{ $name }}" data-toggle="tooltip" data-placement="bottom">
@if ($showName)
{{ $user->name }}
@endif
