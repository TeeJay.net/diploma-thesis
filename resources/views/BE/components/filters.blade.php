<div class="datatable-filters flex-container">
	<form id="filter-form" class="filter-form flex-item clearfix" action="{{ request()->url() }}" method="GET" style="flex-basis: 100%;">

		<div class="flex-container">
		@foreach($filters as $filter)
			<div class="flex-item {{ $filter->isInUse() ? 'applied' : '' }}"
				 style="{{ $filter instanceof \App\DataTables\Filters\DateRangeFilter
					 ? 'flex: 0 0 270px'
					 : 'flex-basis: ' . $filter->width . '%' }}"
			>
				{!! $filter->render() !!}
			</div>
		@endforeach

		@foreach($extraQueryParameters as $name => $value)
			<input type="hidden" name="{{ $name }}" value="{{ $value }}">
		@endforeach
		</div>
	</form>

	@if ($exportsAllowed)
	<div class="flex-item">
		<div class="btn-group export-button m-b">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				<i class="fa fa-download"></i> <span class="caret"></span>
			</button>
			<ul class="dropdown-menu dropdown-menu-right">
				<li class="export-xlsx"><a>{{ trans('generic.download_excel') }}</a></li>
			</ul>
		</div>
	</div>
	@endif
</div>
