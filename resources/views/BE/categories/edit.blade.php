@extends('BE.layouts.app', [
	'title'			=> trans('models.categories.actions.edit'),
	'breadcrumbs'	=> [
		'/admin/categories'								=> trans('models.categories.plural'),
		'/admin/categories/' . $category->id . '/edit'	=> $category->heading,
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.categories.form')

				</div>
			</div>
		</div>
	</div>
@endsection
