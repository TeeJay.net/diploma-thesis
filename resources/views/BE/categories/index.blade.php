@extends('BE.layouts.app', [
	'title'			=> trans('models.categories.plural'),
	'breadcrumbs'	=> [
	],
])

@section('actions')
	<a href="{{ url('admin/categories/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> {{ trans('models.categories.actions.create') }}</a>
@endsection

@section('content')

	{!! $dataTable->render() !!}

@endsection
