@php

echo BootForm::open([
	'model'		=> $category,
	'store'		=> 'BE\CategoriesController@store',
	'update'	=> 'BE\CategoriesController@update',
]);
echo BootForm::text('name', trans('models.categories.fields.name'), isset($category) ? $category->heading : null, [
	'class' => 'required',
]);
echo BootForm::text('alias', trans('models.categories.fields.alias'), isset($category) ? $category->alias : null);
echo BootForm::textarea('description', trans('models.categories.fields.description'), isset($category) ? $category->description : null);
echo BootForm::submit(trans('generic.actions.save'));
echo BootForm::close();

@endphp