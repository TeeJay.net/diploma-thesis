@extends('BE.layouts.app', [
	'title' => trans('models.dashboard.plural'),
	'breadcrumbs'	=> [
	],
])

@section('main-actions')
@endsection

@section('content')

	@php
		$user = request()->user();
	@endphp

	<div class="row equal-heights-container">
		@if ($user->hasPermission('orders'))
			<div class="col-sm-6 col-md-4 col-lg-3 navigation-page-box">
				<div class="ibox biglink">
					<div class="ibox-content">
						<div class="equal-heights-adjust">
							<div class="icon text-center">
								<i class="fa fa-file-text-o" aria-hidden="true"></i>
							</div>
							<p class="description">{{ trans('models.orders.description')}}</p>
						</div>
						<div class="btn-wrapper">
							<a href="{{ url('/admin/orders') }}" class="btn btn-success btn-block">{{ trans('models.orders.plural') }}</a>
						</div>
						<div class="btn-wrapper">
							<a href="{{ url('/admin/orders/create') }}" class="btn btn-primary btn-block">{{ trans('models.orders.actions.create') }}</a>
						</div>
					</div>
				</div>
			</div>
		@endif
		@if ($user->hasPermission('galleries'))
			<div class="col-sm-6 col-md-4 col-lg-3 navigation-page-box">
				<div class="ibox biglink">
					<div class="ibox-content">
						<div class="equal-heights-adjust">
							<div class="icon text-center">
								<i class="fa fa-camera" aria-hidden="true"></i>
							</div>
							<p class="description">{{ trans('models.galleries.description')}}</p>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/galleries') }}" class="btn btn-success btn-block">{{ trans('models.galleries.plural') }}</a>
							</div>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/galleries/create') }}" class="btn btn-primary btn-block">{{ trans('models.galleries.actions.create') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
		@if ($user->hasPermission('galleries'))
			<div class="col-sm-6 col-md-4 col-lg-3 navigation-page-box">
				<div class="ibox biglink">
					<div class="ibox-content">
						<div class="equal-heights-adjust">
							<div class="icon text-center">
								<i class="fa fa-picture-o" aria-hidden="true"></i>
							</div>
							<p class="description">{{ trans('models.backgrounds.description')}}</p>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/backgrounds') }}" class="btn btn-success btn-block">{{ trans('models.backgrounds.plural') }}</a>
							</div>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/backgrounds/create') }}" class="btn btn-primary btn-block">{{ trans('models.backgrounds.actions.create') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
		@if ($user->hasPermission('texts'))
			<div class="col-sm-6 col-md-4 col-lg-3 navigation-page-box">
				<div class="ibox biglink">
					<div class="ibox-content">
						<div class="equal-heights-adjust">
							<div class="icon text-center">
								<i class="fa fa-pencil" aria-hidden="true"></i>
							</div>
							<p class="description">{{ trans('models.texts.description')}}</p>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/texts') }}" class="btn btn-success btn-block">{{ trans('models.texts.plural') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
		@if ($user->hasPermission('articles'))
			<div class="col-sm-6 col-md-4 col-lg-3 navigation-page-box">
				<div class="ibox biglink">
					<div class="ibox-content">
						<div class="equal-heights-adjust">
							<div class="icon text-center">
								<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</div>
							<p class="description">{{ trans('models.articles.description')}}</p>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/articles') }}" class="btn btn-success btn-block">{{ trans('models.articles.plural') }}</a>
							</div>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/articles/create') }}" class="btn btn-primary btn-block">{{ trans('models.articles.actions.create') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
		@if ($user->hasPermission('tags'))
			<div class="col-sm-6 col-md-4 col-lg-3 navigation-page-box">
				<div class="ibox biglink">
					<div class="ibox-content">
						<div class="equal-heights-adjust">
							<div class="icon text-center">
								<i class="fa fa-tags" aria-hidden="true"></i>
							</div>
							<p class="description">{{ trans('models.tags.description')}}</p>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/tags') }}" class="btn btn-success btn-block">{{ trans('models.tags.plural') }}</a>
							</div>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/tags/create') }}" class="btn btn-primary btn-block">{{ trans('models.tags.actions.create') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
		@if ($user->hasPermission('categories'))
			<div class="col-sm-6 col-md-4 col-lg-3 navigation-page-box">
				<div class="ibox biglink">
					<div class="ibox-content">
						<div class="equal-heights-adjust">
							<div class="icon text-center">
								<i class="fa fa-sitemap" aria-hidden="true"></i>
							</div>
							<p class="description">{{ trans('models.categories.description')}}</p>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/categories') }}" class="btn btn-success btn-block">{{ trans('models.categories.plural') }}</a>
							</div>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/categories/create') }}" class="btn btn-primary btn-block">{{ trans('models.categories.actions.create') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
		@if ($user->hasPermission('clients'))
			<div class="col-sm-6 col-md-4 col-lg-3 navigation-page-box">
				<div class="ibox biglink">
					<div class="ibox-content">
						<div class="equal-heights-adjust">
							<div class="icon text-center">
								<i class="fa fa-male" aria-hidden="true"></i>
							</div>
							<p class="description">{{ trans('models.clients.description')}}</p>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/clients') }}" class="btn btn-success btn-block">{{ trans('models.clients.plural') }}</a>
							</div>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/clients/create') }}" class="btn btn-primary btn-block">{{ trans('models.clients.actions.create') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
		@if ($user->hasPermission('users'))
			<div class="col-sm-6 col-md-4 col-lg-3 navigation-page-box">
				<div class="ibox biglink">
					<div class="ibox-content">
						<div class="equal-heights-adjust">
							<div class="icon text-center">
								<i class="fa fa-user-o" aria-hidden="true"></i>
							</div>
							<p class="description">{{ trans('models.users.description')}}</p>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/users') }}" class="btn btn-success btn-block">{{ trans('models.users.plural') }}</a>
							</div>
							<div class="btn-wrapper">
								<a href="{{ url('/admin/users/create') }}" class="btn btn-primary btn-block">{{ trans('models.users.actions.create') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>

@endsection