@extends('BE.layouts.app', [
	'title'			=> trans('models.clients.plural'),
	'breadcrumbs'	=> [
	],
])

@section('actions')
	<a href="{{ url('admin/clients/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> {{ trans('models.clients.actions.create') }}</a>
@endsection

@section('content')

	{!! $dataTable->render() !!}

@endsection
