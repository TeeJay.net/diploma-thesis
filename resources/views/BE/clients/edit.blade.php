@extends('BE.layouts.app', [
	'title'			=> trans('models.clients.actions.edit'),
	'breadcrumbs'	=> [
		'/admin/clients'							=> trans('models.clients.plural'),
		'/admin/clients/' . $client->id . '/edit'	=> $client->name,
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.clients.form')

				</div>
			</div>
		</div>
	</div>
@endsection
