@php

echo BootForm::open([
	'model'		=> $client,
	'store'		=> 'BE\ClientsController@store',
	'update'	=> 'BE\ClientsController@update',
	'enctype'	=> 'multipart/form-data',
]);
echo BootForm::text('name', trans('models.clients.fields.name'), isset($client) ? $client->name: null, [
	'class' => 'required',
]);

@endphp
<div class="form-group">
	<label for="image" class="control-label col-sm-2 required">
		{{ trans('models.clients.fields.image') }}
	</label>
	<div class="col-sm-4">
		@include('BE.components.file-upload-with-preview', [
			'name'      => 'image',
			'path'      => isset($client) && $client->image ? $client->image : null,
			'timestamp' => isset($client) && $client->updated_at ? $client->updated_at->format('dmyHis') : null,
		])
	</div>
</div>
@php
echo BootForm::submit(trans('generic.actions.save'));
echo BootForm::close();

@endphp