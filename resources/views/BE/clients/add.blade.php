@extends('BE.layouts.app', [
	'title'			=> trans('models.clients.actions.create'),
	'breadcrumbs'	=> [
		'/admin/clients'	=> trans('models.clients.plural'),
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.clients.form')

				</div>
			</div>
		</div>
	</div>
@endsection
