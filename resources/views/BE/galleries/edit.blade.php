@extends('BE.layouts.app', [
	'title'			=> trans('models.galleries.actions.edit'),
	'breadcrumbs'	=> [
		'/admin/galleries'							    => trans('models.galleries.plural'),
		'/admin/galleries/' . $gallery->id . '/edit'	=> $gallery->name,
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.galleries.form')

				</div>
			</div>
		</div>
	</div>
@endsection
