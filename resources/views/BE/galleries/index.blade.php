@extends('BE.layouts.app', [
	'title'			=> trans('models.galleries.plural'),
	'breadcrumbs'	=> [
	],
])

@section('actions')
	<a href="{{ url('admin/galleries/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> {{ trans('models.galleries.actions.create') }}</a>
@endsection

@section('content')

	{!! $dataTable->render() !!}

@endsection
