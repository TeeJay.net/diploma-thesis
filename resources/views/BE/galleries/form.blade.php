@php

echo BootForm::open([
	'model'		=> $gallery,
	'store'		=> 'BE\GalleriesController@store',
	'update'	=> 'BE\GalleriesController@update',
	'enctype'	=> 'multipart/form-data',
]);
echo BootForm::text('name', trans('models.galleries.fields.name'), isset($gallery) ? $gallery->name: null, [
	'class' => 'required',
]);
echo BootForm::text('alias', trans('models.galleries.fields.alias'), isset($article) ? $article->alias : null);
echo BootForm::password('password', trans('models.galleries.fields.password_overwrite'));
echo BootForm::select('order_id', trans('models.orders.singular'),
	isset($gallery)
		? modelItems(\App\Order::class, collect([$gallery->order_id]), true)
		: modelItems(\App\Order::class, null, true),
	isset($gallery)
		? $gallery->order_id
		: null,
	[
		'class' => 'chosen-select',
		'data-placeholder' => trans('models.galleries.actions.choose_order'),
	]);
echo BootForm::textarea('description', trans('models.galleries.fields.description'), isset($gallery) ? $gallery->description : null);
echo BootForm::submit(trans('generic.actions.save'));
echo BootForm::close();

@endphp