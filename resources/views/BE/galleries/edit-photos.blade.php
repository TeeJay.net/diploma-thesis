@extends('BE.layouts.app', [
	'title'			=> trans('models.galleries.actions.edit'),
	'breadcrumbs'	=> [
		'/admin/galleries'							    => trans('models.galleries.plural'),
		'/admin/galleries/' . $gallery->id . '/edit'	=> $gallery->name,
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')

					<form class="upload-form" action="{{ url('admin/galleries/' . $gallery->id . '/upload-photo') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group">
							<label class="btn btn-default upload-drop-area">
								{{ trans('generic.actions.browse_or_drag') }}
								<input type="file" class="photos" name="photos" multiple hidden>
							</label>
						</div>
					</form>

					<div class="row">
						<div class="upload-preview m-t-md clearfix">
						</div>
						<br>
						<div class="gallery-content clearfix">
							@if ($gallery->photos)
								<h2 class="col-xs-12">{{ trans('models.galleries.misc.uploaded') }}</h2>
							@endif
							@foreach($gallery->photos as $photo)
								<div class="col-sm-2">
									<div class="file-box">
										<div class="file">
											<span class="corner"></span>
											<div class="image">
												<img class="img-responsive" src="{{ asset($photo . '?w=768') }}" alt="">
											</div>
											<div class="file-name">
												<form class="upload-form ajax" action="{{ url('admin/galleries/' . $gallery->id . '/delete-photo') }}" method="POST">
													{{ csrf_field() }}
													<input type="hidden" name="_method" value="DELETE">
													@php
													$pathParts = explode('/', $photo);
													@endphp
													<input type="hidden" name="name" value="{{ $pathParts[count($pathParts) - 1] }}">
													<button class="delete-image-btn text-danger">{{ trans('generic.actions.delete') }}</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection
