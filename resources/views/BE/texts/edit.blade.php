@extends('BE.layouts.app', [
	'title'			=> trans('models.texts.actions.edit'),
	'breadcrumbs'	=> [
		'/admin/texts'							=> trans('models.texts.plural'),
		'/admin/texts/' . $text->id . '/edit'	=> $text->name,
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.texts.form')

				</div>
			</div>
		</div>
	</div>
@endsection
