@php

echo BootForm::open([
	'model'		=> $text,
	'store'		=> 'BE\TextsController@store',
	'update'	=> 'BE\TextsController@update',
]);
echo BootForm::text('name', trans('models.texts.fields.name'), isset($text) ? $text->name : null);
echo BootForm::textarea('text', trans('models.texts.fields.text'), isset($text) ? $text->text : null);
echo BootForm::submit(trans('generic.actions.save'));
echo BootForm::close();

@endphp