@extends('BE.layouts.app', [
	'title'			=> trans('models.texts.plural'),
	'breadcrumbs'	=> [
	],
])

@section('content')

	{!! $dataTable->render() !!}

@endsection
