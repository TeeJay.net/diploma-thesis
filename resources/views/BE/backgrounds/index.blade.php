@extends('BE.layouts.app', [
	'title'			=> trans('models.backgrounds.plural'),
	'breadcrumbs'	=> [
	],
])

@section('actions')
	<a href="{{ url('admin/backgrounds/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> {{ trans('models.backgrounds.actions.create') }}</a>
@endsection

@section('content')

	{!! $dataTable->render() !!}

@endsection
