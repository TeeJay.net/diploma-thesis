@extends('BE.layouts.app', [
	'title'			=> trans('models.backgrounds.actions.create'),
	'breadcrumbs'	=> [
		'/admin/backgrounds'	=> trans('models.backgrounds.plural'),
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.backgrounds.form')

				</div>
			</div>
		</div>
	</div>
@endsection
