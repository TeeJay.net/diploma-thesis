@php

echo BootForm::open([
	'model'		=> $background,
	'store'		=> 'BE\BackgroundsController@store',
	'update'	=> 'BE\BackgroundsController@update',
	'enctype'	=> 'multipart/form-data',
]);
echo BootForm::text('name', trans('models.backgrounds.fields.name'), isset($background) ? $background->name: null, [
	'class' => 'required',
]);
echo BootForm::textarea('description', trans('models.backgrounds.fields.description'), isset($background) ? $background->description : null);

@endphp
<div class="form-group">
	<label for="image" class="control-label col-sm-2 required">
		{{ trans('models.backgrounds.fields.image') }}
	</label>
	<div class="col-sm-4">
		@include('BE.components.file-upload-with-preview', [
			'name'      => 'image',
			'path'      => isset($background) && $background->image ? $background->image : null,
			'timestamp' => isset($background) && $background->updated_at ? $background->updated_at->format('dmyHis') : null,
		])
	</div>
</div>
@php
echo BootForm::submit(trans('generic.actions.save'));
echo BootForm::close();

@endphp