@php

echo BootForm::open([
	'model'		=> $user,
	'store'		=> 'BE\UsersController@store',
	'update'	=> 'BE\UsersController@update',
]);
echo BootForm::text('first_name', trans('models.users.fields.first_name'), isset($user) ? $user->first_name : '', [
	'class' => 'required',
]);
echo BootForm::text('last_name', trans('models.users.fields.last_name'), isset($user) ? $user->last_name : '', [
	'class' => 'required',
]);
echo BootForm::text('public_name', trans('models.users.fields.public_name'), isset($user) ? $user->public_name : '');
echo BootForm::text('email', trans('models.users.fields.email'), isset($user) ? $user->email : '', [
	'class' => 'required',
]);
if (!isset($user)) {
	echo BootForm::password('password', trans('models.users.fields.password'));
}
@endphp

@if (!isset($user) || $user->id != request()->user()->id)  {{-- cannot change access rights for himself --}}
	<div class="form-group">
		@include('BE.components.checkboxes', [
			'name' => 'permissions[]',
			'heading' => trans('models.users.fields.permissions'),
			'options' => $permissions,
			'checked' => isset($user) ? $user->permissions->toArray() : [],
			'headingClass' => 'col-sm-2 text-right',
			'groupWrapperClass' => 'col-sm-10 m-b',
		])
	</div>
@endif

@php
echo BootForm::submit(trans('generic.actions.save'));
echo BootForm::close();
@endphp