@extends('BE.layouts.app', [
	'title'			=> trans('models.users.plural'),
	'breadcrumbs'	=> [
	],
])

@section('actions')
	<a href="{{ url('admin/users/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> {{ trans('models.users.actions.create') }}</a>
@endsection

@section('content')

	{!! $dataTable->render() !!}

@endsection
