<form method="POST" action="{{ url('admin/users/' . $user->id . '/change-avatar') }}" class="form-horizontal avatar-form" enctype="multipart/form-data">
	{{ csrf_field() }}
	<input name="_method" type="hidden" value="PUT">
	<div class="form-group">
		<label for="avatar-upload" class="control-label col-sm-2">
			{{ trans('models.users.fields.avatar') }}
		</label>
		<div class="col-sm-10">
			@include('BE.components.file-upload-with-preview', [
				'name'      => 'avatar-upload',
				'path'      => $user->avatar ?: null,
				'timestamp' => $user->updated_at->format('dmyHis') ?: null,
			])
		</div>
	</div>
	{!! BootForm::submit(trans('generic.actions.save')) !!}
</form>