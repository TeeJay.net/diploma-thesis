@extends('BE.layouts.app', [
	'title'			=> trans('models.users.actions.create'),
	'breadcrumbs'	=> [
		'/admin/users'	=> trans('models.users.plural'),
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.users.form')

				</div>
			</div>
		</div>
	</div>
@endsection
