@extends('BE.layouts.app', [
	'title'			=> trans('models.users.actions.edit'),
	'breadcrumbs'	=> [
		'/admin/users'							=> trans('models.users.plural'),
		'/admin/users/' . $user->id . '/edit'	=> $user->first_name . ' ' . $user->last_name,
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content clearfix">

					@include('BE.components.errors')
					@include('BE.users.form')

				</div>
			</div>
		</div>
	</div>
@endsection
