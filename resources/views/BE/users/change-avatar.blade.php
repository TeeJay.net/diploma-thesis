@extends('BE.layouts.app', [
	'title'			=> trans('models.users.actions.change_avatar'),
	'breadcrumbs'	=> [
		'/admin/users'									=> trans('models.users.plural'),
		'/admin/users/' . $user->id . '/change-avatar'	=> $user->first_name . ' ' . $user->last_name,
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.users.change-avatar-form')

				</div>
			</div>
		</div>
	</div>
@endsection
