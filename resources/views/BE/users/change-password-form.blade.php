@php

echo BootForm::open([
	'model'				=> $user,
	'update'			=> 'BE\UsersController@storeChangePassword',
]);
echo BootForm::password('old_password', trans('models.users.fields.old_password'));
echo BootForm::password('new_password', trans('models.users.fields.new_password'));
echo BootForm::password('new_password_again', trans('models.users.fields.new_password_again'));
echo BootForm::submit(trans('generic.actions.save'));
echo BootForm::close();

@endphp