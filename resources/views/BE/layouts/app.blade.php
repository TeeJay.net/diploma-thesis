<?php
$request = request();
$secondUrlSegment = $request->segment(2);
$user = $request->user();
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>{{ $title }} | {{ config('app.name') }} {{ trans('generic.title_last_part') }}</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex, nofollow">

	@include('BE.layouts.head-assets')
</head>

<body data-language="{{ app()->getLocale() }}">
<div id="wrapper">

	@include('BE.layouts.nav', [
		'user' => $user,
		'secondUrlSegment' => $secondUrlSegment,
	])

	<div id="page-wrapper" class="gray-bg">
		<div class="row border-bottom">
			<nav class="navbar navbar-static-top white-bg">
				<div class="navbar-header">
					<a class="navbar-minimalize minimalize-styl-2 btn btn-primary pull-left" href="#"><i class="fa fa-bars"></i> </a>
					<h2 class="main-heading pull-left">
						@if (isset($breadcrumbs))
							<ol class="breadcrumb">
								@foreach ($breadcrumbs as $link => $name)
									<li>
										<a href="{{ url($link) }}">{{ $name }}</a>
									</li>
								@endforeach
								<li class="active">
									<span class="font-normal">{{ $title }}</span>
								</li>
							</ol>
						@else
							{{$title}}
						@endif
					</h2>
				</div>
			</nav>
		</div>

		@if (View::hasSection('actions'))
		<div class="wrapper text-right">
			@yield('actions')
		</div>
		@endif

		<div class="wrapper wrapper-content animated">
			@include('BE.components.toast')
			@yield('content')
		</div>

		<div class="footer hidden-print">
			<div class="pull-right">
				<strong> <a href="https://tomasjanecek.cz">Tomáš Janeček</a> &copy; {{ date('Y') }}</strong>
			</div>
		</div>

	</div>
</div>


@include('BE.layouts.body-assets')

</body>
</html>