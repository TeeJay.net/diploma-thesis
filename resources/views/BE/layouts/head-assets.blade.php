<link href="{{ asset('favicon.ico') }}" rel="icon">

@foreach(json_decode(file_get_contents(public_path('assets/css/vendor/styles.json'))) as $script)
	<link href="{{ asset(preg_replace('#^./public/#', '', $script)) }}" rel="stylesheet">
@endforeach
@foreach(json_decode(file_get_contents(public_path('assets/css/styles.json'))) as $script)
	<link href="{{ asset(preg_replace('#^./public/#', '', $script)) }}" rel="stylesheet">
@endforeach
