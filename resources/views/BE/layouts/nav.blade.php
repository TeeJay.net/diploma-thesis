<nav class="navbar-default navbar-static-side">
	<div class="sidebar-collapse">
		<ul class="nav" id="side-menu">
			<li class="nav-header">
				<img class="img-responsive" src="{{ asset('assets/img/menu-logo.png') }}" alt="{{ config('app.name') }}">
				<div class="dropdown profile-element">
					<div class="m-t-sm">
						@include('BE.components.profile-picture', [
							'value' => request()->user(),
							'size' => 48,
						])
					</div>
					<strong class="block font-bold m-t-xs">{{ request()->user()->name }}</strong>
				</div>
				<div class="logo-element">
					{{ config('app.name_short') }}
				</div>
			</li>

			<li @if ($secondUrlSegment === 'dashboard')class="active"@endif>
				<a href="{{ url('/admin/dashboard') }}">
					<i class="fa fa-table"></i>
					<span class="nav-label">{{ trans('models.dashboard.plural') }}</span>
				</a>
			</li>
			@if ($user->hasPermission('orders'))
				<li @if ($secondUrlSegment === 'orders')class="active"@endif>
					<a href="{{ url('/admin/orders') }}">
						<i class="fa fa-file-text-o"></i>
						<span class="nav-label">{{ trans('models.orders.plural') }}</span>
					</a>
				</li>
			@endif
			@if ($user->hasPermission('galleries'))
				<li @if ($secondUrlSegment === 'galleries')class="active"@endif>
					<a href="{{ url('/admin/galleries') }}">
						<i class="fa fa-camera"></i>
						<span class="nav-label">{{ trans('models.galleries.plural') }}</span>
					</a>
				</li>
			@endif
			@if ($user->hasPermission('backgrounds'))
				<li @if ($secondUrlSegment === 'backgrounds')class="active"@endif>
					<a href="{{ url('/admin/backgrounds') }}">
						<i class="fa fa-picture-o"></i>
						<span class="nav-label">{{ trans('models.backgrounds.plural') }}</span>
					</a>
				</li>
			@endif
			@if ($user->hasPermission('texts'))
				<li @if ($secondUrlSegment === 'texts')class="active"@endif>
					<a href="{{ url('/admin/texts') }}">
						<i class="fa fa-pencil"></i>
						<span class="nav-label">{{ trans('models.texts.plural') }}</span>
					</a>
				</li>
			@endif
			@if ($user->hasPermission('articles'))
				<li @if ($secondUrlSegment === 'articles')class="active"@endif>
					<a href="{{ url('/admin/articles') }}">
						<i class="fa fa-pencil-square-o"></i>
						<span class="nav-label">{{ trans('models.articles.plural') }}</span>
					</a>
				</li>
			@endif
			@if ($user->hasPermission('tags'))
				<li @if ($secondUrlSegment === 'tags')class="active"@endif>
					<a href="{{ url('/admin/tags') }}">
						<i class="fa fa-tags"></i>
						<span class="nav-label">{{ trans('models.tags.plural') }}</span>
					</a>
				</li>
			@endif
			@if ($user->hasPermission('categories'))
				<li @if ($secondUrlSegment === 'categories')class="active"@endif>
					<a href="{{ url('/admin/categories') }}">
						<i class="fa fa-sitemap"></i>
						<span class="nav-label">{{ trans('models.categories.plural') }}</span>
					</a>
				</li>
			@endif
			@if ($user->hasPermission('clients'))
				<li @if ($secondUrlSegment === 'clients')class="active"@endif>
					<a href="{{ url('/admin/clients') }}">
						<i class="fa fa-users"></i>
						<span class="nav-label">{{ trans('models.clients.plural') }}</span>
					</a>
				</li>
			@endif
			@if ($user->hasPermission('users'))
				<li @if ($secondUrlSegment === 'users')class="active"@endif>
					<a href="{{ url('/admin/users') }}">
						<i class="fa fa-user-o"></i>
						<span class="nav-label">{{ trans('models.users.plural') }}</span>
					</a>
				</li>
			@endif
			@if ($user->hasPermission('settings'))
			<li @if ($secondUrlSegment === 'settings')class="active"@endif>
				<a href="{{ url('/admin/settings') }}">
					<i class="fa fa-cog"></i>
					<span class="nav-label">{{ trans('models.settings.plural') }}</span>
				</a>
			</li>
			@endif

			<br>

			<li>
				<a href="{{ url('/admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit()" data-no-instant>
					<i class="fa fa-sign-out"></i> <span class="nav-label">{{ trans('auth.logout') }}</span>
				</a>
				<form class="hidden" id="logout-form" action="{{ url('/admin/logout') }}" method="POST">
					{{ csrf_field() }}
				</form>
			</li>
		</ul>
	</div>
</nav>