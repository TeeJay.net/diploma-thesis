<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>{{ $title }} | {{ config('app.name') }} Admin</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex, nofollow">

	@include('BE.layouts.head-assets')
</head>

<body class="full-black-bg">

	<div class="middle-box text-center loginscreen animated fadeInDown">

		<img class="img-responsive" alt="{{ config('app.name') }}" src="{{ url('/assets/img/logo.png') }}">

		@if (Session::has('message'))
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<?php switch (Session::get('message_type')) {
							case 'warn':
								$class = 'warning';
								$icon = '<i class="fa fa-exclamation-triangle"></i> ';
								break;
							case 'fail':
								$class = 'danger';
								$icon = '<i class="fa fa-times"></i> ';
								break;
							default:
								$class = 'primary';
								$icon = '<i class="fa fa-check"></i> ';
								break;
						} ?>
						<div class="panel panel-{{ $class }}">
							<div class="panel-heading">
								<strong>
									{!! $icon !!}
									{!! Session::get('message') !!}
								</strong>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif

		@yield('content')

	</div>

	@include('BE.layouts.body-assets')

</body>

</html>
