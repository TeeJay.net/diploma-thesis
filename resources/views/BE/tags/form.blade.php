@php

echo BootForm::open([
	'model'		=> $tag,
	'store'		=> 'BE\TagsController@store',
	'update'	=> 'BE\TagsController@update',
]);
echo BootForm::text('name', trans('models.tags.fields.name'), isset($tag) ? $tag->heading : null, [
	'class' => 'required',
]);
echo BootForm::text('alias', trans('models.tags.fields.alias'), isset($tag) ? $tag->alias : null);
echo BootForm::textarea('description', trans('models.tags.fields.description'), isset($tag) ? $tag->description : null);
echo BootForm::submit(trans('generic.actions.save'));
echo BootForm::close();

@endphp