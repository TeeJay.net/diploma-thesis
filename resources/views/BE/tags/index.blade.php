@extends('BE.layouts.app', [
	'title'			=> trans('models.tags.plural'),
	'breadcrumbs'	=> [
	],
])

@section('actions')
	<a href="{{ url('admin/tags/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> {{ trans('models.tags.actions.create') }}</a>
@endsection

@section('content')

	{!! $dataTable->render() !!}

@endsection
