@extends('BE.layouts.app', [
	'title'			=> trans('models.tags.actions.edit'),
	'breadcrumbs'	=> [
		'/admin/tags'						=> trans('models.tags.plural'),
		'/admin/tags/' . $tag->id . '/edit'	=> $tag->heading,
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.tags.form')

				</div>
			</div>
		</div>
	</div>
@endsection
