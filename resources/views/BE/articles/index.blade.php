@extends('BE.layouts.app', [
	'title'			=> trans('models.articles.plural'),
	'breadcrumbs'	=> [
	],
])

@section('actions')
	<a href="{{ url('admin/articles/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> {{ trans('models.articles.actions.create') }}</a>
@endsection

@section('content')

	{!! $dataTable->render() !!}

@endsection
