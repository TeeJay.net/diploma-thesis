@php

echo BootForm::open([
	'model'		=> $article,
	'store'		=> 'BE\ArticlesController@store',
	'update'	=> 'BE\ArticlesController@update',
	'enctype'	=> 'multipart/form-data',
]);
echo BootForm::text('heading', trans('models.articles.fields.heading'), isset($article) ? $article->heading : null, [
	'class' => 'required',
]);
echo BootForm::text('alias', trans('models.articles.fields.alias'), isset($article) ? $article->alias : null);
echo BootForm::select('category_id', trans('models.articles.fields.category_id'),
	isset($article)
		? modelItems(\App\Category::class, collect([$article->category_id]), true)
		: modelItems(\App\Category::class, null, true),
	isset($article)
		? $article->category_id
		: null,
	[
		'class' => 'chosen-select required',
		'data-placeholder' => 'Vyberte kategorii',
	]
);
echo BootForm::select('published_by_user_id', trans('models.articles.fields.published_by_user_id'),
	isset($article)
		? modelItems(\App\User::class, collect([$article->publishedByUser]))
		: modelItems(\App\User::class),
	isset($article)
		? $article->published_by_user_id
		: request()->user()->id,
	[
		'class' => 'chosen-select required'
	]
);
echo BootForm::select('tag_ids[]', trans('models.articles.fields.tags'),
	isset($article)
		? modelItems(\App\Tag::class, $article->tags, true)
		: modelItems(\App\Tag::class, null, true),
	isset($article)
		? $article->tags->pluck('id')->toArray()
		: null,
	[
		'class' => 'chosen-select-deselect',
		'data-placeholder' => 'Vyberte tagy',
		'multiple',
	]
);
echo BootForm::text('published_at', trans('models.articles.fields.published_at'), isset($article) ? formatDate($article->published_at) : date('d.m.Y'), [
	'class' => 'date required'
]);
@endphp
<div class="form-group">
	<label for="image" class="control-label col-sm-2">
		{{ trans('models.articles.fields.image') }}
	</label>
	<div class="col-sm-4">
		@include('BE.components.file-upload-with-preview', [
			'name'      => 'image',
			'path'      => isset($article) && $article->image ? $article->image : null,
			'timestamp' => isset($article) && $article->updated_at ? $article->updated_at->format('dmyHis') : null,
		])
	</div>
	<label for="fb_image" class="control-label col-sm-2">
		{{ trans('models.articles.fields.fb_image') }}
	</label>
	<div class="col-sm-4">
		@include('BE.components.file-upload-with-preview', [
			'name'          => 'fb_image',
			'path'          => isset($article) && $article->fb_image ? $article->fb_image : null,
			'secondaryPath' => isset($article) && $article->image ? $article->image : null,
			'timestamp'     => isset($article) && $article->updated_at ? $article->updated_at->format('dmyHis') : null,
		])
	</div>
</div>
@php
echo BootForm::textarea('intro', trans('models.articles.fields.intro'), isset($article) ? $article->intro : null, [
	'class' => 'required',
]);
echo BootForm::textarea('text', trans('models.articles.fields.text'), isset($article) ? $article->text : null);
echo BootForm::submit(trans('generic.actions.save'));
echo BootForm::close();

@endphp