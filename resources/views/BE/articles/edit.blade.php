@extends('BE.layouts.app', [
	'title'			=> trans('models.articles.actions.edit'),
	'breadcrumbs'	=> [
		'/admin/articles'							=> trans('models.articles.plural'),
		'/admin/articles/' . $article->id . '/edit'	=> $article->heading,
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.articles.form')

				</div>
			</div>
		</div>
	</div>
@endsection
