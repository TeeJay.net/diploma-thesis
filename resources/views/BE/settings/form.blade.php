@php

echo BootForm::open([
	'update'	=> 'BE\SettingsController@update',
]);

foreach ($settings as $setting) {
	$bootFormType = $setting->bootform_type;
	echo BootForm::$bootFormType($setting->key, trans('models.settings.fields.' . $setting->key), $setting->value ?: null, [
		'class' => str_contains($setting->validation_rule, 'required') ? 'required' : '',
	]);
}

echo BootForm::submit(trans('generic.actions.save'));
echo BootForm::close();

@endphp