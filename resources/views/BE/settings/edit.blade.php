@extends('BE.layouts.app', [
	'title'			=> trans('models.settings.plural'),
	'breadcrumbs'	=> [
	],
])

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">

					@include('BE.components.errors')
					@include('BE.settings.form')

				</div>
			</div>
		</div>
	</div>
@endsection
