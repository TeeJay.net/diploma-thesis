<?php

return [
	'singular' => 'Přihlášení',
	'plural' => 'Přihlášení',
	'email' => 'Email',
	'password' => 'Heslo',
	'login' => 'Přihlásit se',
	'logout' => 'Odhlásit se',
	'remember' => 'Zapamatovat',
	'failed' => 'Nesprávný login nebo heslo.',
	'wrong_password' => 'Nesprávné heslo.',
	'throttle' => 'Příliš mnoho pokusů o přihlášení. Zkuste to znovu za :seconds sekund.',
];
