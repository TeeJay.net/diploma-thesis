<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
/ Note: Works with $this-> instead of Route:: but leads to some errors (Debugbar...)
*/

Route::get('/', 'FE\FrontendController@home');
Route::get('/cenik', 'FE\FrontendController@pricing');
Route::get('/co-to-je-cvakmat', 'FE\FrontendController@whatIsIt');
Route::get('/objednat-cvakmat', 'FE\FrontendController@order');
Route::post('/objednat-cvakmat', 'FE\FrontendController@storeOrder');
Route::get('/pozadi', 'FE\FrontendController@backgrounds');
Route::get('/galerie', 'FE\FrontendController@galleries');
Route::get('/galerie/{alias}', 'FE\FrontendController@gallery');
Route::post('/galerie/{gallery}/login', 'FE\FrontendController@galleryLogin');
Route::get('/blog', 'FE\FrontendController@blog');
Route::get('/blog/{alias}', 'FE\FrontendController@article');
Route::get('/blog/tag/{alias}', 'FE\FrontendController@tag');
Route::get('/blog/kategorie/{alias}', 'FE\FrontendController@category');
Route::get('/kontakt', 'FE\FrontendController@contact');

// Authentication routes...
Route::get('/admin', 'LoginController@showLoginForm');
Route::post('/admin/login', 'LoginController@login');
Route::post('/admin/logout', 'LoginController@logout');

// Admin routes
Route::group(['namespace' => 'BE', 'prefix' => 'admin'], function() {

	Route::get('dashboard', 'DashboardController@index');

	Route::resource('articles', 'ArticlesController', [
		'except' => ['show']
	]);
	Route::resource('categories', 'CategoriesController', [
		'except' => ['show']
	]);
	Route::resource('tags', 'TagsController', [
		'except' => ['show']
	]);
	Route::resource('texts', 'TextsController', [
		'only' => ['index', 'edit', 'update']
	]);
	Route::resource('backgrounds', 'BackgroundsController', [
		'except' => ['show']
	]);
	Route::resource('clients', 'ClientsController', [
		'except' => ['show']
	]);
	Route::resource('galleries', 'GalleriesController', [
		'except' => ['show']
	]);
	Route::get('galleries/{gallery}/edit-photos', 'GalleriesController@editPhotos');
	Route::post('galleries/{gallery}/upload-photo', 'GalleriesController@uploadPhoto');
	Route::delete('galleries/{gallery}/delete-photo', 'GalleriesController@deletePhoto');

	// Orders
	Route::resource('orders', 'OrdersController', [
		'except' => ['show']
	]);
	Route::put('orders/{order}/confirm', 'OrdersController@confirmOrder');
	Route::get('orders/{order}/send-email-order-confirmed', 'OrdersController@sendEmailOrderConfirmed');
	// Order invoices
	Route::get('orders/{order}/edit-invoice-pdf', 'OrdersController@editInvoicePdf');
	Route::put('orders/{order}/edit-invoice-pdf', 'OrdersController@updateInvoicePdf');
	Route::get('orders/{order}/invoice-pdf', 'OrdersController@getInvoicePdf');
	Route::get('orders/{order}/refresh-invoice-pdf', 'OrdersController@refreshInvoicePdf');
	// Order contracts
	Route::get('orders/{order}/edit-contract-pdf', 'OrdersController@editContractPdf');
	Route::put('orders/{order}/edit-contract-pdf', 'OrdersController@updateContractPdf');
	Route::get('orders/{order}/contract-pdf', 'OrdersController@getContractPdf');
	Route::get('orders/{order}/refresh-contract-pdf', 'OrdersController@refreshContractPdf');

	// Users
	Route::resource('users', 'UsersController', [
		'except' => ['show']
	]);
	Route::get('users/{user}/change-password', 'UsersController@showChangePassword');
	Route::put('users/{user}/change-password', 'UsersController@storeChangePassword');
	Route::get('users/{user}/change-avatar', 'UsersController@showChangeAvatar');
	Route::put('users/{user}/change-avatar', 'UsersController@storeChangeAvatar');

	// Settings
	Route::get('settings', 'SettingsController@edit');
	Route::post('settings', 'SettingsController@update');
});

// Glide images
Route::get('images/{path}', 'ImageController@show')->where('path', '.+');

// Cache and clearing cache
Route::get('cache', 'Controller@cache');
Route::get('clear-cache', 'Controller@clearCache');