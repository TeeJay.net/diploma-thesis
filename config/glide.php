<?php

return [

	// 'source' => Illuminate\Contracts\Filesystem\Factory::class,                  // Source filesystem
	// 'source' => Illuminate\Contracts\Filesystem\Factory::class,                  // Source filesystem
	'source_path_prefix' => 'images',      // Source filesystem path prefix
	// 'cache' => Illuminate\Contracts\Filesystem\Factory::class,                   // Cache filesystem
	'cache_path_prefix' => 'images/.cache',       // Cache filesystem path prefix
	// 'group_cache_in_folders' =>  // Whether to group cached images in folders
	// 'watermarks' =>              // Watermarks filesystem
	// 'watermarks_path_prefix' =>  // Watermarks filesystem path prefix
	// 'driver' =>                  // Image driver (gd or imagick)
	// 'max_image_size' =>          // Image size limit
	// 'defaults' =>                // Default image manipulations
	// 'presets' =>                 // Preset image manipulations
	// 'base_url' =>                // Base URL of the images
	// 'response' =>                // Response factory
];
