<?php

return [
	'orders',
	'galleries',
	'backgrounds',
	'texts',
	'articles',
	'clients',
	'tags',
	'categories',
	'users',
	'settings',
];
