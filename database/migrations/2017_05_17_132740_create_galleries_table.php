<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galleries', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->string('name');
			$table->string('alias');
			$table->string('password')->nullable();
			$table->text('description')->nullable();
			$table->integer('order_id')->unsigned()->nullable();
			$table->foreign('order_id')->references('id')->on('orders');
			$table->integer('created_by_user_id')->unsigned();
			$table->foreign('created_by_user_id')->references('id')->on('users');
			$table->integer('updated_by_user_id')->unsigned();
			$table->foreign('updated_by_user_id')->references('id')->on('users');
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('galleries', function (Blueprint $table) {
			$table->dropForeign('galleries_order_id_foreign');
			$table->dropForeign('galleries_created_by_user_id_foreign');
			$table->dropForeign('galleries_updated_by_user_id_foreign');
		});
        Schema::dropIfExists('galleries');
    }
}
