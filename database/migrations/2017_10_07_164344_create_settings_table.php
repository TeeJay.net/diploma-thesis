<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class CreateSettingsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings', function (Blueprint $table) {
			$table->string('key')->primary();
			$table->text('value')->nullable();
			$table->integer('order')->nullable();
			$table->string('bootform_type')->nullable();
			$table->string('validation_rule')->nullable();
			$table->timestamps();
		});

		$arrayDates = ['created_at' => DB::raw('NOW()'), 'updated_at' => DB::raw('NOW()')];
		DB::table('settings')->insert([
			array_merge(['key' => 'admin_mail1', 'value' => '', 'order' => 1, 'bootform_type' => 'text', 'validation_rule' => 'required|max:191'], $arrayDates),
			array_merge(['key' => 'admin_mail2', 'value' => '', 'order' => 2, 'bootform_type' => 'text', 'validation_rule' => 'max:191'], $arrayDates),
			array_merge(['key' => 'cvakmat_count', 'value' => '3', 'order' => 5, 'bootform_type' => 'text', 'validation_rule' => 'required|integer|max:191'], $arrayDates),
			array_merge(['key' => 'price_per_km', 'value' => '6', 'order' => 6, 'bootform_type' => 'text', 'validation_rule' => 'required|integer|max:191'], $arrayDates),
			array_merge(['key' => 'free_km_one_way', 'value' => '15', 'order' => 7, 'bootform_type' => 'text', 'validation_rule' => 'required|integer|max:191'], $arrayDates),
			array_merge(['key' => 'next_invoice_number', 'value' => '1', 'order' => 10, 'bootform_type' => 'text', 'validation_rule' => 'required|integer|max:191'], $arrayDates),
			array_merge(['key' => 'print_price_no', 'value' => '4999', 'order' => 15, 'bootform_type' => 'text', 'validation_rule' => 'required|max:191'], $arrayDates),
			array_merge(['key' => 'print_price_photo', 'value' => '30', 'order' => 16, 'bootform_type' => 'text', 'validation_rule' => 'required|max:191'], $arrayDates),
			array_merge(['key' => 'print_price_free_for_all', 'value' => '11999', 'order' => 17, 'bootform_type' => 'text', 'validation_rule' => 'required|max:191'], $arrayDates),
			array_merge(['key' => 'invoice_template', 'value' => view('BE.orders.pdf-invoice')->render(), 'order' => 20, 'bootform_type' => 'textarea', 'validation_rule' => 'required'], $arrayDates),
			array_merge(['key' => 'contract1_template', 'value' => view('BE.orders.pdf-contract1')->render(), 'order' => 25, 'bootform_type' => 'textarea', 'validation_rule' => 'required'], $arrayDates),
			array_merge(['key' => 'contract2_template', 'value' => view('BE.orders.pdf-contract2')->render(), 'order' => 26, 'bootform_type' => 'textarea', 'validation_rule' => 'required'], $arrayDates),
			array_merge(['key' => 'contract3_template', 'value' => view('BE.orders.pdf-contract3')->render(), 'order' => 27, 'bootform_type' => 'textarea', 'validation_rule' => 'required'], $arrayDates),
			array_merge(['key' => 'contract_header', 'value' => view('BE.orders.contract_header')->render(), 'order' => 30, 'bootform_type' => 'textarea', 'validation_rule' => 'required'], $arrayDates),
			array_merge(['key' => 'contract_footer', 'value' => view('BE.orders.contract_footer')->render(), 'order' => 31, 'bootform_type' => 'textarea', 'validation_rule' => 'required'], $arrayDates),
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}
}
