<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->string('company')->nullable();
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email');
			$table->string('phone');
			$table->enum('print', ['no', 'print_price_per_photo', 'print_free_for_all']);
			$table->string('location');
			$table->timestamp('rental_from')->useCurrent();
			$table->timestamp('rental_to')->useCurrent();
			$table->text('note')->nullable();
			$table->integer('final_price');
			$table->string('final_price_words')->nullable();
			$table->boolean('confirmed');
			$table->integer('background_id')->unsigned();
			$table->foreign('background_id')->references('id')->on('backgrounds');
			$table->integer('created_by_user_id')->unsigned()->nullable();
			$table->foreign('created_by_user_id')->references('id')->on('users');
			$table->integer('updated_by_user_id')->unsigned()->nullable();
			$table->foreign('updated_by_user_id')->references('id')->on('users');
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('orders', function (Blueprint $table) {
			$table->dropForeign('orders_background_id_foreign');
			$table->dropForeign('orders_created_by_user_id_foreign');
			$table->dropForeign('orders_updated_by_user_id_foreign');
		});
        Schema::dropIfExists('orders');
    }
}
