<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFullAddressToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	// change() doesn't work on tables with an enum colkumn
		Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('location');
            $table->string('street')->after('last_name')->nullable();
            $table->string('house_number')->after('street')->nullable();
            $table->string('city')->after('house_number')->nullable();
            $table->string('postcode')->after('city')->nullable();
            $table->integer('distance')->after('postcode')->unsigned()->default(0)->nullable();
            $table->integer('delivery_price')->after('note')->unsigned()->default(0);
            $table->integer('final_price')->after('delivery_price')->unsigned()->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		// change() doesn't work on tables with an enum colkumn
		Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('orders', function (Blueprint $table) {
			$table->string('location')->after('print');
			$table->dropColumn('street');
			$table->dropColumn('house_number');
			$table->dropColumn('city');
			$table->dropColumn('postcode');
			$table->dropColumn('distance');
			$table->dropColumn('delivery_price');
			$table->integer('final_price')->after('note')->default(0)->change();
		});
    }
}
