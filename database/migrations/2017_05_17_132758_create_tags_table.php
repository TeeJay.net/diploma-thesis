<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->string('name');
			$table->string('alias');
			$table->text('description')->nullable();
			$table->integer('created_by_user_id')->unsigned();
			$table->foreign('created_by_user_id')->references('id')->on('users');
			$table->integer('updated_by_user_id')->unsigned();
			$table->foreign('updated_by_user_id')->references('id')->on('users');
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('tags', function (Blueprint $table) {
			$table->dropForeign('tags_created_by_user_id_foreign');
			$table->dropForeign('tags_updated_by_user_id_foreign');
		});
        Schema::dropIfExists('tags');
    }
}
