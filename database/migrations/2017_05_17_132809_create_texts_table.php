<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('texts', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->string('name');
			$table->string('alias');
			$table->text('text');
			$table->integer('created_by_user_id')->unsigned()->nullable();
			$table->foreign('created_by_user_id')->references('id')->on('users');
			$table->integer('updated_by_user_id')->unsigned()->nullable();
			$table->foreign('updated_by_user_id')->references('id')->on('users');
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('texts', function (Blueprint $table) {
			$table->dropForeign('texts_updated_by_user_id_foreign');
		});
        Schema::dropIfExists('texts');
    }
}
