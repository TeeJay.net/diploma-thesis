<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->string('name');
			$table->string('image')->nullable();
			$table->integer('created_by_user_id')->unsigned();
			$table->foreign('created_by_user_id')->references('id')->on('users');
			$table->integer('updated_by_user_id')->unsigned();
			$table->foreign('updated_by_user_id')->references('id')->on('users');
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('clients', function (Blueprint $table) {
			$table->dropForeign('clients_created_by_user_id_foreign');
			$table->dropForeign('clients_updated_by_user_id_foreign');
		});
        Schema::dropIfExists('clients');
    }
}
