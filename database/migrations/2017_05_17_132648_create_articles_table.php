<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id')->unsigned();
			$table->string('heading');
			$table->string('alias');
			$table->text('intro')->nullable();
			$table->text('text')->nullable();
			$table->string('image')->nullable();
			$table->string('fb_image')->nullable();
			$table->integer('category_id')->unsigned();
			$table->foreign('category_id')->references('id')->on('categories');
			$table->integer('published_by_user_id')->unsigned();
			$table->foreign('published_by_user_id')->references('id')->on('users');
			$table->integer('created_by_user_id')->unsigned();
			$table->foreign('created_by_user_id')->references('id')->on('users');
			$table->integer('updated_by_user_id')->unsigned();
			$table->foreign('updated_by_user_id')->references('id')->on('users');
			$table->timestamp('published_at')->nullable();
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('articles', function (Blueprint $table) {
			$table->dropForeign('articles_category_id_foreign');
			$table->dropForeign('articles_published_by_user_id_foreign');
			$table->dropForeign('articles_created_by_user_id_foreign');
			$table->dropForeign('articles_updated_by_user_id_foreign');
		});
        Schema::dropIfExists('articles');
    }
}
