<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unsigned();
			$table->string('first_name');
			$table->string('last_name');
			$table->string('public_name');
            $table->string('email')->unique();
			$table->string('password');
			$table->string('avatar')->unique()->nullable();
			$table->string('permissions')->nullable();
			$table->integer('created_by_user_id')->unsigned()->nullable();
			$table->foreign('created_by_user_id')->references('id')->on('users');
			$table->integer('updated_by_user_id')->unsigned()->nullable();;
			$table->foreign('updated_by_user_id')->references('id')->on('users');
			$table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
