<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('audit', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamp('timestamp')->useCurrent();
			$table->string('table');
			$table->string('key');
			$table->enum('action', ['create', 'update', 'delete']);
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users');
			$table->text('attributes');

			$table->index(['table', 'key', 'timestamp']);
			$table->index(['user_id', 'timestamp']);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('audit', function (Blueprint $table) {
			$table->dropForeign('audit_user_id_foreign');
		});
		Schema::drop('audit');
    }
}
