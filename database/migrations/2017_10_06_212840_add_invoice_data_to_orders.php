<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceDataToOrders extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function (Blueprint $table) {
			$table->string('invoice_number')->after('background_id')->nullable();
			$table->string('invoice_first_name')->after('invoice_number')->nullable();
			$table->string('invoice_last_name')->after('invoice_first_name')->nullable();
			$table->string('invoice_street')->after('invoice_last_name')->nullable();
			$table->string('invoice_house_number')->after('invoice_street')->nullable();
			$table->string('invoice_city')->after('invoice_house_number')->nullable();
			$table->string('invoice_postcode')->after('invoice_city')->nullable();
			$table->string('invoice_ic')->after('invoice_postcode')->nullable();
			$table->string('invoice_dic')->after('invoice_ic')->nullable();
			$table->timestamp('invoice_date_created')->after('invoice_dic')->nullable();
			$table->timestamp('invoice_date_due')->after('invoice_date_created')->nullable();
			$table->string('invoice_vs')->after('invoice_date_due')->nullable();
			$table->string('invoice_event')->after('invoice_vs')->nullable();
			$table->text('invoice_html')->after('invoice_event')->nullable();
			$table->text('contract_html')->after('invoice_event')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function (Blueprint $table) {
			$table->dropColumn('invoice_number');
			$table->dropColumn('invoice_first_name');
			$table->dropColumn('invoice_last_name');
			$table->dropColumn('invoice_street');
			$table->dropColumn('invoice_house_number');
			$table->dropColumn('invoice_city');
			$table->dropColumn('invoice_postcode');
			$table->dropColumn('invoice_ic');
			$table->dropColumn('invoice_dic');
			$table->dropColumn('invoice_date_created');
			$table->dropColumn('invoice_date_due');
			$table->dropColumn('invoice_vs');
			$table->dropColumn('invoice_event');
			$table->dropColumn('invoice_html');
		});
	}
}
