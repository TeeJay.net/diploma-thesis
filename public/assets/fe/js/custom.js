$(window).scroll(function() {
	if ($(window).scrollTop() > 1) {
		$('.header-fixed .header-sticky').addClass('header-fixed-shrink');
	} else {
		$('.header-fixed .header-sticky').removeClass('header-fixed-shrink');
	}
});

$('.fancybox').fancybox({
	groupAttr: 'data-rel',
	prevEffect: 'fade',
	nextEffect: 'fade',
	openEffect: 'fade',
	closeEffect: 'fade',
	openSpeed: 100,
	closeSpeed: 100,
	nextSpeed: 100,
	prevSpeed: 100,
	closeBtn: true,
	helpers: {
		title: {
			type: 'float'
		}
	}
});

$.datetimepicker.setLocale('cs');
$('.datetimepicker').datetimepicker({
	timepicker: true,
	format: 'd.m.Y H:i',
	lang: 'cs'
});
$('.datepicker').datetimepicker({
	timepicker: false,
	format: 'd.m.Y',
	lang: 'cs'
});

$('#is_company').on('change', function() {
	var $this = $(this);
	if ($this.is(':checked')) {
		$('#company-data').show();
	} else {
		$('#company-data').hide();
	}
});
$('#invoice_data_are_same').on('change', function() {
	var $this = $(this);
	if (!$this.is(':checked')) {
		$('#invoice_data').show();
	} else {
		$('#invoice_data').hide();
	}
});
$('#is_company_invoice_data').on('change', function() {
	var $this = $(this);
	if ($this.is(':checked')) {
		$('#invoice-company-data').show();
	} else {
		$('#invoice-company-data').hide();
	}
});