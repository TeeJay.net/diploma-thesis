/**
 * Functions definitions
 */
$.showSpinner = function() {
	$('body').append(
		'<div class="on-top">' +
		'	<div id="spinner" class="centerer" style="">' +
		'		<div class="sk-fading-circle">' +
		'			<div class="sk-circle1 sk-circle"></div>' +
		'			<div class="sk-circle2 sk-circle"></div>' +
		'			<div class="sk-circle3 sk-circle"></div>' +
		'			<div class="sk-circle4 sk-circle"></div>' +
		'			<div class="sk-circle5 sk-circle"></div>' +
		'			<div class="sk-circle6 sk-circle"></div>' +
		'			<div class="sk-circle7 sk-circle"></div>' +
		'			<div class="sk-circle8 sk-circle"></div>' +
		'			<div class="sk-circle9 sk-circle"></div>' +
		'			<div class="sk-circle10 sk-circle"></div>' +
		'			<div class="sk-circle11 sk-circle"></div>' +
		'			<div class="sk-circle12 sk-circle"></div>' +
		'		</div>' +
		'	</div>' +
		'</div>'
	);
};
$.hideSpinner = function() {
	$('#spinner').parent().remove();
};

/**
 * Initializations that run only once
 */
$(function() {
	// Spinner CSS3 animation for ajax requests
	$(document).on('ajaxSend', function(event, jqxhr, settings) {
		$.showSpinner();
	});
	$(document).on('ajaxComplete', function() {
		$.hideSpinner();
	});

	// Sidebar toggling
	$(document).on('click', '.toggle-sidebar', function() {
		$('body').toggleClass('mini-navbar');
	});

	// Datatable filters
	$(document).on('change', '.dropdown-filter', function() {
		$(this).closest('.filter-form').trigger('submit');
	});

	// Equal heights
	$('.equal-heights-container').each(function() {
		var maxHeight = 0;
		var $container = $(this);
		var containerWidth = $container.css('width');

		$container.find('.equal-heights-adjust').css('padding-bottom', 0);

		requestAnimationFrame(function() {
			$container.children().each(function() {
				var height = $(this).height();
				if (height > maxHeight) {
					maxHeight = height;
				}
			}).each(function() {
				if ($(this).css('width') !== containerWidth) {
					$(this).
						find('.equal-heights-adjust').
						css('padding-bottom', maxHeight - $(this).height());
				}
			});
		});
	});

	// Find sorted datatables and group the rows into distinct values.
	$('.table-minimal', this).each(function() {
		var $table = $(this);
		var $sortHeading = $table.find('th>a>.fa').parent();
		var sortIndex = $sortHeading.parent().index();

		if (sortIndex === -1) {
			return;
		}

		var $sortCells = $table.children('tbody').
			children('tr').
			children(':nth-child(' + (sortIndex + 1) + ')');
		var $startsOfRowGroups = $();

		var previousValue = '';
		$sortCells.each(function() {
			var $sortCell = $(this);
			var currentValue = $sortCell.text();

			if (currentValue.trim() !== previousValue.trim()) {
				$startsOfRowGroups = $startsOfRowGroups.add($sortCell.parent());
			}

			previousValue = currentValue;
		});

		if ($startsOfRowGroups.length <
			$table.children('tbody').children().length) {
			$startsOfRowGroups.each(function() {
				if ($(this).prev('tr').length) {
					$(this).before('<tr><td class="divider"></td></tr>');
				}
			});
		}
	});

	// Don't forget to trigger init on page load
	$('body').trigger('init');
});

/**
 * Initializations that need to run every time new content is loaded
 */
$.init(function() {

	// Chosen
	var config = {
		'.chosen-select': {width: '100%', search_contains: true},
		'.chosen-select-deselect': {
			width: '100%',
			allow_single_deselect: true,
			search_contains: true
		},
		'.chosen-select-no-single': {
			width: '100%',
			disable_search_threshold: 10,
			search_contains: true
		},
		'.chosen-select-no-results': {
			width: '100%',
			no_results_text: 'Oops, nothing found!'
		},
		'.chosen-select-width': {width: '95%'}
	};
	for (var selector in config) {
		$(selector, this).chosen(config[selector]);
	}

	// Datepicker
	$.fn.datepicker.dates.cs = {
		days: ['Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota'],
		daysShort: ['Ned', 'Pon', 'Úte', 'Stř', 'Čtv', 'Pát', 'Sob'],
		daysMin: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
		months: ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec'],
		monthsShort: ['Led', 'Úno', 'Bře', 'Dub', 'Kvě', 'Čer', 'Čnc', 'Srp', 'Zář', 'Říj', 'Lis', 'Pro'],
		today: 'Dnes',
		clear: 'Vymazat',
		weekStart: 1,
		format: 'd.m.yyyy'
	};
	var startDate = new Date();
	startDate.setDate(startDate.getDate() + 3);
	$('.date').datepicker({
		todayBtn: 'linked',
		keyboardNavigation: true,
		forceParse: false,
		calendarWeeks: true,
		autoclose: true,
		language: $('body').data('language'),
		startDate: startDate,
		endDate: new Date('2100-01-01'),
		todayHighlight: true
	});

	// DateTimePicker
	$.datetimepicker.setLocale('cs');
	$('.datetimepicker').datetimepicker({
		timepicker: true,
		format: 'd.m.Y H:i',
		lang: 'cs'
	});
	$('.datepicker').datetimepicker({
		timepicker: false,
		format: 'd.m.Y',
		lang: 'cs'
	});

	// Tooltips
	$('[data-toggle="tooltip"]').tooltip();

	// Autofocus
	$('input[autofocus]:first', this).focus();

	// Toaster
	toastr.options = {
		'closeButton': true,
		'debug': false,
		'progressBar': false,
		'preventDuplicates': false,
		'positionClass': 'toast-top-center',
		'onclick': null,
		'timeOut': '2000',
		'showEasing': 'swing',
		'hideEasing': 'linear'
	};
	var $toast = $('.toast');
	if ($toast.length && $toast.data('toast-type')) {
		toastr[$toast.data('toast-type')]($toast.html());
		$toast.remove();
	}

	// CKEditor
	if ($('#intro').length) CKEDITOR.replace('intro');
	if ($('#text').length) CKEDITOR.replace('text');
	if ($('[name=description]').length) CKEDITOR.replace('description');
	if ($('[name=invoice_template]').length) CKEDITOR.replace('invoice_template');
	if ($('[name=contract1_template]').length) CKEDITOR.replace('contract1_template');
	if ($('[name=contract2_template]').length) CKEDITOR.replace('contract2_template');
	if ($('[name=contract3_template]').length) CKEDITOR.replace('contract3_template');
	if ($('[name=invoice_html]').length) CKEDITOR.replace('invoice_html');
	if ($('[name=contract_html]').length) CKEDITOR.replace('contract_html');
	if ($('[name=contract_header]').length) CKEDITOR.replace('contract_header');
	if ($('[name=contract_footer]').length) CKEDITOR.replace('contract_footer');

	// Calendar month switching
	$('.calendar .arrow-left a, .calendar .arrow-right a').on('click', function(e) {
		e.preventDefault();
		var link = $(this).attr('href');
		$.reloadPartOfPage('.calendar', link);
	});

	// Responsiveness all the time
	if ($(this).width() < 769) {
		$('body').addClass('body-small')
	} else {
		$('body').removeClass('body-small')
	}
	$(window).bind('resize', function () {
		if ($(this).width() < 769) {
			$('body').addClass('body-small')
		} else {
			$('body').removeClass('body-small')
		}
	});
	$('.navbar-minimalize').on('click', function () {
		$('body').toggleClass('mini-navbar');
	});
});