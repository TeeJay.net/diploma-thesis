// Init event for running DOM manipulations after new content is loaded
(function() {
	$.init = function(callback) {
		$(document).on('init', 'body, div', callback);
	};
})();

$.reloadPartOfPage = function(selector, url) {
	if (!url) {
		url = document.location.pathname;
		if (document.location.search) {
			url += document.location.search;
		}
	}

	// Make use of profile page that is cheap to load
	$(selector).parent().load(url + ' ' + selector, function() {
		$(this).closest('body').trigger('init');
	});
};

$.getQueryParameters = function(url) {
	var result = {};
	if (url !== undefined) {
		var parser = document.createElement('a');
		parser.href = url;
		if (parser.search !== '') {
			$.each(parser.search.substring(1).split('&'), function(i) {
				var split = this.split('=');
				result[decodeURIComponent(split[0])] = decodeURIComponent(split[1]);
			});
		}
		return result;
	}
	if (window.location.search !== '') {
		$.each(window.location.search.substring(1).split('&'), function(i) {
			var split = this.split('=');
			result[decodeURIComponent(split[0])] = decodeURIComponent(split[1]);
		});
	}
	return result;
};
$.updatedQueryParameters = function(map) {
	var result = $.getQueryParameters();
	$.each(map, function(key, value) {
		result[key] = value;
	});
	return result;
};

$(document).on('keyup', '.quickfilter', function(event) {
	var $input = $(this);
	if ($input.data('searching')) {
		return;
	}
	var targetSelector = $input.data('target');

	var performSearch = function(searchString) {
		$input.data('searching', true);
		var queryString = $.param($.updatedQueryParameters({
			'quickfilter': $input.val(),
			'page': 1
		}));
		$(targetSelector).
			load(document.location.pathname + '?' + queryString + ' ' + targetSelector + '>*', function() {
				if ($input.val() === searchString) {
					$input.data('searching', false);
				}
				else {
					performSearch($input.val());
				}
				$(this).trigger('init');
			});
	};
	performSearch($input.val());
});
