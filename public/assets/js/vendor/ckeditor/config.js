/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// Allow keeping empty <i></i> in the content (FontAwesome, ...)
CKEDITOR.dtd.$removeEmpty.i = 0;

CKEDITOR.editorConfig = function( config ) {
	// Custom toolbar buttons
	config.toolbar = [
		{ name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', '-', 'Undo', 'Redo' ] },
		{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat', 'TextColor' ] },
		{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
		{ name: 'styles', items: [ 'Format' ] },
		{ name: 'links', items : [ 'Link', 'Unlink' ] },
		{ name: 'insert', items: [ 'Image', 'Youtube', 'Table', '-','HorizontalRule', 'PageBreak', 'SpecialChar' ] },
		{ name: 'document', items: [ 'Source', '-', 'Maximize' ] }
	];
	// Language set for toolbar
	config.language = 'cs';

	// Autoexpand the editor textarea according to the height of the content using AutoGrow plugin
	config.autoGrow_onStartup = true;

	// Just don't touch the content
	config.allowedContent = true;

	// Don't convert accented characters to html entities
	config.htmlEncodeOutput = false;
	config.entities = false;

	// Enable added YT plugin
	config.extraPlugins = 'youtube';
};
