/**
 * Support submitting with CTRL+Enter, or just Enter where appropriate.
 */
$(document).on('keydown', 'form input,select', function (event) {
	if (event.which === 13) {
		event.preventDefault();
		$(this).closest('form').submit();
	}
});
$(document).on('keydown', 'form tfileExtensionarea', function (event) {
	if (event.which === 13 && event.ctrlKey) {
		event.preventDefault();
		$(this).closest('form').submit();
	}
});


/**
 * Ajaxify forms
 */
$(document).on('submit', 'form[method=POST].ajax', function (e) {
	e.preventDefault();
	var $form = $(this);
	var request = $.post($form.attr('action'), $form.serialize(), null, 'json');
	request.done(function (responseData) {
		$form.trigger('ajax-success', responseData);
		$.reloadPartOfPage('#wrapper');
	});
});
$(document).on('click', 'a.ajax', function (e) {
	e.preventDefault();
	var $link = $(this);
	var request = $.get($link.attr('href'), null, null, 'json');
	request.done(function (responseData) {
		$link.trigger('ajax-success', responseData);
		$.reloadPartOfPage('#wrapper');
	});
});

/**
 * Add required class to labels of required inputs/selects/tfileExtensionareas
 */
$('input.required, select.required, textarea.required, input.required-asterisk-after, select.required-asterisk-after, textarea.required-asterisk-after').each(function () {
	var $label = $(this).closest('.form-group').find('label');
	var className = $(this).hasClass('required')
		? 'required'
		: 'required-asterisk-after';
	if (!$label.hasClass(className)) {
		$label.addClass(className);
	}
});

/**
 * Preview for single file upload forms
 */
$(document).on('change', '.image-upload-with-preview', function() {
	if (typeof (FileReader) === 'undefined') {
		return;
	}
	var $input = $(this);
	var file = $input[0].files[0];
	var fileName = file.name;
	var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
	var $imageHolder = $input.closest('div').find('.upload-preview');
	$imageHolder.empty();

	if (fileExtension !== 'gif' && fileExtension !== 'png' && fileExtension !== 'jpg' && fileExtension !== 'jpeg') {
		return;
	}

	var reader = new FileReader();
	reader.fileName = file.name;
	reader.fileSize = Math.round(100 * file.size / 1024) / 100;
	reader.onload = function(file) {
		$imageHolder.append('<img class="img-preview img-responsive" src="' + file.target.result + '" alt="">');
	};
	reader.readAsDataURL(file);
});

/**
 * Preview for multi file upload forms (gallery)
 */
$('.photos').on('change', function () {

	// Image previews
	var $input = $(this);
	var files = $input[0].files;
	var filesCount = $input[0].files.length;
	var loadedPreviewsCount = 0;
	var $imageHolder = $('.upload-preview');
	$imageHolder.empty();

	for (var i = 0; i < filesCount; i++) {
		var fileName = files[i].name;
		var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
		if (fileExtension !== 'jpg' && fileExtension !== 'png' && fileExtension !== 'gif' && fileExtension !== 'jpeg' && fileExtension !== 'webp') {
			continue;
		}
		var reader = new FileReader();
		reader.fileName = files[i].name;
		reader.fileSize = Math.round(100 * files[i].size / 1024) / 100;
		reader.onload = function (file) {
			$imageHolder.append(
				'<div class="col-sm-2">' +
				'	<div class="file-box" data-filename="' + file.target.fileName + '">' +
				'		<div class="file">' +
				'			<span class="corner"></span>' +
				'			<div class="image">' +
				'				<img class="img-responsive" src="' + file.target.result + '" alt="preview-image">' +
				'			</div>' +
				'			<div class="file-name">' +
				'				<div class="name">' +
				'					' + file.target.fileName +
				'				</div>' +
				'				<small>' + file.target.fileSize + ' kB</small>' +
				'				<div class="progress"><div class="progress-bar"></div></div>' +
				'			</div>' +
				'		</div>' +
				'	</div>' +
				'</div>'
			);

			loadedPreviewsCount++;
			if (loadedPreviewsCount % 6 === 0) {
				$imageHolder.append('<div class="clearfix"></div>');
			}
		};
		reader.readAsDataURL(files[i]);
	}

	// Image upload with progress tracking
	var queuedUploads = 0;
	$.each(files, function (i, file) {

		var updateProgress = function (file, loaded) {
			var percentage = loaded / file.size * 100;
			var $progressBar = $imageHolder.find('.file-box[data-filename="' + file.name + '"] .progress-bar');
			$progressBar.width(percentage + '%');
		};

		var xhr = new XMLHttpRequest();
		if (typeof xhr.upload !== 'undefined') {
			xhr.upload.addEventListener('progress', function (event) {
				updateProgress(file, event.loaded);
			});
		}

		xhr.onreadystatechange = function (event) {
			if (xhr.readyState === 4) {
				updateProgress(file, file.size);
				queuedUploads--;
				if (queuedUploads === 0) {
					toastr.success('Všechny soubory nahrány');
					$.reloadPartOfPage('#wrapper');

				}
			}
		};
		queuedUploads++;

		xhr.open('POST', $input.closest('form').attr('action') + '?' + jQuery.param({'name' : file.name}));
		xhr.setRequestHeader('X-CSRF-TOKEN', $input.closest('form').find('input[name="_token"]').val());
		xhr.send(file);
	});
});

