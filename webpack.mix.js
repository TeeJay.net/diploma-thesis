const { mix } = require('laravel-mix');
const fs = require('fs');

const styles = JSON.parse(fs.readFileSync('public/assets/css/vendor/styles.json'))
	.concat(JSON.parse(fs.readFileSync('public/assets/css/styles.json')))
const js = JSON.parse(fs.readFileSync('public/assets/js/vendor/scripts.json'))
	.concat(JSON.parse(fs.readFileSync('public/assets/js/scripts.json')))

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles(styles, 'public/compiled/css.css')
	.js(js, 'public/compiled/js.js')
	.version();

mix.copy('public/assets/fonts', 'public/fonts');
