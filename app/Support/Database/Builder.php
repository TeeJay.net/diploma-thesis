<?php

namespace App\Support\Database;

use Cache;
use Illuminate\Database\Query\Builder as QueryBuilder;

class Builder extends QueryBuilder
{
	protected $cached = true;

	/**
	 * Run the query as a "select" statement against the connection.
	 *
	 * @return array
	 */
	protected function runSelect()
	{
		if (!$this->cached) {
			return parent::runSelect();
		}

		if (($request = request()) && $request->isMethod('GET')) {
			return Cache::store('array')->remember($this->getCacheKey(), 1, function () {
				return parent::runSelect();
			});
		}

		return parent::runSelect();
	}

	/**
	 * Returns a Unique String that can identify this Query.
	 *
	 * @return string
	 */
	protected function getCacheKey()
	{
		return md5($this->toSql() . implode(',', $this->getBindings()));
	}

	public function withoutCache()
	{
		$this->cached = false;
		return $this;
	}
}
