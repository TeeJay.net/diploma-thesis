<?php

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Pass in an Eloquent model object and receive the relative URL to a page representing that object.
 *
 * @param Model $model
 * @return string|null
 */
function modelUrl(Model $model) {
	switch(get_class($model)) {
		case \App\Article::class: return 'articles/' . $model->id;
		case \App\Audit::class: return 'audit/' . $model->id;
		case \App\Background::class: return 'backgrounds/' . $model->id;
		case \App\Category::class: return 'categories/' . $model->id;
		case \App\Client::class: return 'clients/' . $model->id;
		case \App\Gallery::class: return 'galleries/' . $model->id;
		case \App\Order::class: return 'orders/' . $model->id;
		case \App\Tag::class: return 'tags/' . $model->id;
		case \App\Text::class: return 'texts/' . $model->id;
		case \App\User::class: return 'users/' . $model->id;
		default: return null;
	}
}

/**
 * Get items of model for a select. Adds all necessary trashed items too if they are selected
 *
 * @param string $model
 * @param Collection|null $possiblyTrashedItems Array of items of a given model, NOT an array of given model properties
 * @param bool $withEmptyOption
 * @param string $value
 * @param string $key
 * @return array
 */
function modelItems(string $model, Collection $possiblyTrashedItems = null, bool $withEmptyOption = false, string $value = 'name', string $key = 'id')
{
	if (!$possiblyTrashedItems) {
		$array = $model::all()->pluck($value, $key)->all();
		if ($withEmptyOption) {
			return ['' => ''] + $array;
		}
		return $array;
	}

	$nonTrasheditems = $model::withoutTrashed()
		->get();
	$trashedItems = $model::onlyTrashed()
		->whereIn('id', $possiblyTrashedItems->pluck('id'))
		->get();

	$array = $nonTrasheditems->merge($trashedItems)->pluck($value, $key)->all();
	if ($withEmptyOption) {
		return ['' => ''] + $array;
	}
	return $array;
}

/**
 * Gets an arry of permissions in the application
 *
 * @return mixed
 */
function getPermissions()
{
	$permissions = \Config::get('permissions');
	foreach ($permissions as $key => $permission) {
		if (str_contains($permission, '.')) {
			// Support for permissions like articles.delete
			$permissionPath = explode('.', $permission);
			$modelName = $permissionPath[0];
			unset($permissionPath[0]);
			$stringPath = implode('.', $permissionPath);
			$permissions[$permission] = trans('models.' . $modelName . '.actions.' . $stringPath);
		} else {
			$permissions[$permission] = trans('models.' . $permission . '.plural');
		}
		unset($permissions[$key]);
	}

	return $permissions;
}

/**
 * Formats date, can be a string or a Carbon object
 *
 * @param        $value
 * @param string $format
 *
 * @return string|static
 */
function formatDate($value, $format = 'd.m.Y')
{
	if ($value == null) {
		return '';
	} else if ($value instanceof Carbon) {
		return $value->year > 0 ? $value->format($format) : '';
	} else if (is_string($value)) {
		return $value[0] > '0' ? Carbon::createFromFormat('d.m.Y', $value) : '';
	}

	return $value;
}

/**
 * Format big numbers, defaulting to the current language's formatting settings. Handles large numbers using string operations.
 * Necessary for DataTables
 *
 * @param number $number        The number to format
 * @param int    $decimals      How many digits after the decimal point?
 * @param null   $decimal_sep   Thousands separator
 * @param null   $thousand_sep  Decimal separator
 *
 * @return string
 */
function formatNumber($number, $decimals = 2, $decimal_sep = null, $thousand_sep = null){

	$decimal_sep = $decimal_sep ?? trans('generic.decimal_point');
	$thousand_sep = $thousand_sep ?? trans('generic.thousands_separator');

	if(is_int($number) && $number < 2147483647) {
		return number_format($number, $decimals, $decimal_sep, $thousand_sep);
	}

	$decimals = $decimals * 1;
	$number_parts = explode(".", $number);
	$integer_part = $number_parts[0];
	$decimal_part = isset($number_parts[1]) ? $number_parts[1] : '';

	if ($decimals > 0){
		if (strlen($decimal_part) < $decimals){
			$decimal_part = str_pad($decimal_part, $decimals, "0");
		} else {
			$decimal_part = substr($decimal_part, 0, $decimals);
		}
	} else {
		$decimal_sep = '';
		$decimal_part = '';
	}

	$integer_parts_rev = strrev($integer_part);
	$integer_part = '';
	for ($i = strlen($integer_parts_rev) - 1; $i >= 0; $i--) {
		if ($i % 3 == 0 && $i != 0) {
			$integer_part .= $integer_parts_rev[$i] . $thousand_sep;
		} else {
			$integer_part .= $integer_parts_rev[$i];
		}
	}

	return $integer_part . $decimal_sep . $decimal_part;
}

/**
 * Updates URLs GET parameters, optionally unsetting some GET parameters
 * Necessary for DataTables
 *
 * @param array $params
 * @param array $paramsToUnset
 *
 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
 */
function updateUrl(array $params = [], array $paramsToUnset = [])
{
	$mergedParams = array_merge(request()->all(), $params);
	foreach ($paramsToUnset as $param) {
		if (isset($mergedParams[$param])) {
			unset($mergedParams[$param]);
		}
	}
	return url(request()->url() . ($mergedParams ? '?' : '') . http_build_query($mergedParams));
}

function getSettings(array $settingsToReturn = null) {
	$settings = DB::table('settings')->get();
	foreach ($settings as $key => $setting) {
		if (!$settingsToReturn || ($settingsToReturn && in_array($setting->key, $settingsToReturn))) {
			$settings[$setting->key] = $setting;
		}
		unset($settings[$key]);
	}
	return $settings;
}

function menu(int $index, \Illuminate\Database\Eloquent\Collection $menuCollection) {
	return strip_tags($menuCollection->get('menu_' . $index)->text);
}





/**
 * Convert duration from a float representing hours to a string in MM:SS format.
 * @param float $hours Duration in hours.
 * @return string
 */
function formatDuration($hours) {
	return sprintf('%02d:%02d', floor($hours), round($hours * 60)%60);
}

/**
 * Format an integer using the current locale's settings.
 * @param int $number Number.
 * @return string
 */
function formatInteger($number) {
	return number_format($number, 0, ',', '.');
}

/**
 * Cram a date into a maximum of 5 characters.
 * @param \Carbon\Carbon $carbon
 * @return string
 */
function formatMiniDate(\Carbon\Carbon $carbon) {
	$now = \Carbon\Carbon::now();

	switch($carbon->year) {

		// Dates from current year are always formatted 'd.m'
		case $now->year:
			return $carbon->format('d.m');

		// Dates from the latter half of last year are formatted 'd.m' during the first two months of the current year
		case $now->year - 1:
			if($carbon->month > 6 && $now->month < 3) {
				return $carbon->format('d.m');
			} else {
				return $carbon->format('Y');
			}

		// All other years are formatted 'Y'
		default:
			return $carbon->format('Y');
	}
}

function formatEmptyValue($value, $placeholder = '-')
{
	return empty($value) ? $placeholder : $value;
}

function formatPercentage($value, $numberOfDecimals = 2)
{
	return $value ? formatNumber($value, $numberOfDecimals) . '%' : '';
}

function formatWholePercentage($value)
{
	return $value ? round(formatNumber($value)) . '%' : '';
}

function formatAmount($value, $decimals = 0)
{
	return formatNumber($value ?? 0, $decimals);
}

function formatWholeHours($value)
{
	return round($value).' '.strtolower(trans('generic.hours'));
}

function formatHourOfDay($value)
{
	return sprintf('%02d:00 - %02d:59', $value, $value);
}

function formatBoolean($value)
{
	return (bool)($value);
}

function formatDateTime($value, $format = 'd.m.Y, H:i')
{
	if($value == NULL || substr($value,0,1) == '0') {
		return '';
	}

	return Carbon::createFromFormat($format, $value);
}

function formatTime($value, $format = 'H:i')
{
	if ($value === NULL) {
		return '';
	} else {
		return Carbon::createFromFormat($format, $value);
	}
}

function icon($class)
{
	return "<i class='$class'></i>";
}
