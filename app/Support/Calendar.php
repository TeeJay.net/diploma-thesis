<?php

namespace app\Support;

use Carbon\Carbon;

class Calendar
{
	/**
	 * @var Carbon $dayOfTheMonth
	 */
	protected $firstDayOfTheMonth;
	/**
	 * @var Carbon $dayOfTheMonth
	 */
	protected $lastDayOfTheMonth;
	/**
	 * @var Carbon $monthViewStartDate
	 */
	protected $monthViewStartDate;
	protected $events = [];

	public function __construct(Carbon $dayOfTheMonth = null)
	{
		// Set any day of the month to build calendar for
		$this->firstDayOfTheMonth = $dayOfTheMonth ?: Carbon::today()->startOfMonth();
		$lastDayOfTheMonth = clone $this->firstDayOfTheMonth;
		$this->lastDayOfTheMonth = $lastDayOfTheMonth->lastOfMonth();
		// Set the date when the calendar starts
		$date = Carbon::createFromDate($this->firstDayOfTheMonth->year, $this->firstDayOfTheMonth->month,1);
		$this->monthViewStartDate = $date->startOfWeek();
	}

	public function getMonthName()
	{
		return $this->firstDayOfTheMonth->format('F');
	}

	public function getYear()
	{
		return $this->firstDayOfTheMonth->format('Y');
	}

	public function getMonthCalendarHeader()
	{
		return [
			trans('generic.monday'),
			trans('generic.tuesday'),
			trans('generic.wednesday'),
			trans('generic.thursday'),
			trans('generic.friday'),
			trans('generic.saturday'),
			trans('generic.sunday'),
		];
	}
	public function getMonthCalendar()
	{
		$date = clone $this->monthViewStartDate;

		$calendarArray = [];
		// Each time, render 5 weeks
		for ($i = 0; $i < 5; $i++) {
			$calendarArray[$date->weekOfYear] = [];
			// Render all 7 days each time
			for ($j = 0; $j < 7; $j++) {
				$calendarArray[$date->weekOfYear][$date->dayOfWeek]['date'] = clone $date;
				if (isset($this->events[$date->format('d-m-Y')])) {
					$calendarArray[$date->weekOfYear][$date->dayOfWeek]['events'] = $this->events[$date->format('d-m-Y')];
				}
				$date->addDay();
			}
		}

		return $calendarArray;
	}

	public function belongsToThisMonth(Carbon $date)
	{
		if ($date->between($this->firstDayOfTheMonth, $this->lastDayOfTheMonth)) {
			return true;
		}
		return false;
	}

	public function addEvent(Carbon $date, string $eventName, string $class = null)
	{
		$event = [
			'name' => $eventName,
			'class' => $class ?? 'label-danger',
		];
		$this->events[$date->format('d-m-Y')][$date->format('H-i-s')] = $event;
	}

	public function getPreviousMonth()
	{
		$date = clone $this->firstDayOfTheMonth;
		return $date->subDay()->firstOfMonth()->format('d-m-Y');
	}

	public function getNextMonth()
	{
		$date = clone $this->lastDayOfTheMonth;
		return $date->addDay()->firstOfMonth()->format('d-m-Y');
	}
}