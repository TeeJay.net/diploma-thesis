<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

/**
 * App\Audit
 *
 * @property int                 $id
 * @property \Carbon\Carbon      $timestamp
 * @property string              $table
 * @property string              $key
 * @property string              $action
 * @property int|null            $user_id
 * @property string              $attributes
 * @property-read \App\User|null $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Audit onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Audit whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Audit whereAttributes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Audit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Audit whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Audit whereTable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Audit whereTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Audit whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Audit withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Audit withoutTrashed()
 * @mixin \Eloquent
 */
class Audit extends BaseModel
{
	use SoftDeletes;

	protected $table = 'audit';
	protected $guarded = [];
	protected $dates = [
		'timestamp',
	];

	/*
	 * Relationships
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/*
	 * Other methods
	 */
	public static function audit(string $class)
	{
		if ($class::$auditing) {
			$audit = function(BaseModel $object, $action, $attributes) {
				try {
					DB::table('audit')->insert([
						'timestamp'  => date('Y-m-d H:i:s'),
						'table'      => $object->getTable(),
						'key'        => $object->getKey(),
						'action'     => $action,
						'user_id'    => Auth::check() ? Auth::user()->id : null,
						'attributes' => json_encode($attributes),
					]);
				} catch (QueryException $e) {
					if ($e->getCode() === '42S02') {
						// Table not found. The migration may not have run yet. Ignore.
					} else {
						throw $e;
					}
				}
			};

			$class::created(function(BaseModel $object) use ($audit) {
				$audit($object, 'create', $object->getAttributes());
			});

			$class::updating(function(BaseModel $object) use ($audit) {
				$audit($object, 'update', $object->getDirty());
			});

			$class::deleting(function(BaseModel $object) use ($audit) {
				$audit($object, 'delete', $object->getAttributes());
			});
		}
	}
}
