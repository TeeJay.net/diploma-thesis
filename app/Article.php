<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Article
 *
 * @property int                                                      $id
 * @property string                                                   $heading
 * @property string                                                   $alias
 * @property string                                                   $intro
 * @property string                                                   $text
 * @property string                                                   $image
 * @property string                                                   $fb_image
 * @property int                                                      $category_id
 * @property int                                                      $published_by_user_id
 * @property int                                                      $created_by_user_id
 * @property int                                                      $updated_by_user_id
 * @property \Carbon\Carbon                                           $published_at
 * @property \Carbon\Carbon|null                                      $created_at
 * @property \Carbon\Carbon|null                                      $updated_at
 * @property \Carbon\Carbon|null                                      $deleted_at
 * @property-read \App\Category                                       $category
 * @property-read \App\User                                           $createdByUser
 * @property-read \App\User                                           $publishedByUser
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tag[] $tags
 * @property-read \App\User                                           $updatedByUser
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Article onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereFbImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereHeading($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereIntro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article wherePublishedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereUpdatedByUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Article withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Article withoutTrashed()
 * @mixin \Eloquent
 */
class Article extends BaseModel
{
	use SoftDeletes;

	protected $table = 'articles';
	protected $guarded = [];
	protected $dates = [
		'published_at',
		'deleted_at',
	];

	/*
	 * Relationships
	 */
	public function category()
	{
		return $this->belongsTo(Category::class)->withTrashed();
	}

	public function publishedByUser()
	{
		return $this->belongsTo(User::class)->withTrashed();
	}

	public function createdByUser()
	{
		return $this->belongsTo(User::class)->withTrashed();
	}

	public function updatedByUser()
	{
		return $this->belongsTo(User::class)->withTrashed();
	}

	public function tags()
	{
		return $this->belongsToMany(Tag::class)->withTrashed()->withTimestamps();
	}
}
