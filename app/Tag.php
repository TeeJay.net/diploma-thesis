<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Tag
 *
 * @property int                                                          $id
 * @property string                                                       $name
 * @property string                                                       $alias
 * @property string                                                       $description
 * @property int                                                          $created_by_user_id
 * @property int                                                          $updated_by_user_id
 * @property \Carbon\Carbon|null                                          $created_at
 * @property \Carbon\Carbon|null                                          $updated_at
 * @property \Carbon\Carbon|null                                          $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Article[] $articles
 * @property-read \App\User                                               $createdByUser
 * @property-read \App\User                                               $updatedByUser
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Tag onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag whereUpdatedByUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tag withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Tag withoutTrashed()
 * @mixin \Eloquent
 */
class Tag extends BaseModel
{
	use SoftDeletes;

	protected $table = 'tags';
	protected $guarded = [];
	protected $dates = [
		'deleted_at',
	];

	/*
	 * Relationships
	 */
	public function createdByUser()
	{
		return $this->belongsTo(User::class);
	}

	public function updatedByUser()
	{
		return $this->belongsTo(User::class);
	}

	/*
	 * Inverse relationships
	 */
	public function articles()
	{
		return $this->belongsToMany(Article::class)->withTimestamps();
	}
}

