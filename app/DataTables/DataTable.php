<?php

namespace App\DataTables;

use App\DataTables\Columns\Column;
use App\DataTables\Filters\FilterInterface;
use Carbon\Carbon;
use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection as SupportCollection;

class DataTable
{
	private $query;
	private $request;

	private $model;
	private $modelInstance;
	private $modelSingular;
	private $modelPlural;

	/**
	 * @var LengthAwarePaginator $paginator
	 */
	private $paginator;
	private $itemsPerPage = 30;

	private $columns;
	private $columnWidths;
	private $orderByColumnName;
	private $secondaryOrderColumnName;
	private $orderByRaw;
	private $orderDir = 'asc';
	private $secondaryOrderDir;
	private $emptyTableMessage;

	private $linkPrefix;
	private $linkSuffix = '';
	private $linkModal = false;
	private $linkDownload = false;

	private $filters = [];
	private $filterWidths = [];

	private $exportsAllowed = false;

	private $highlightRowClosure;
	const unreadColumnName = 'is_unread_for_user';
	const UNREAD_COLUMN_WIDTH = 1;

	private $readyForRendering;
	private $bigLinkEnabled = true;
	private $extraQueryParameters = [];

	private $massActionsAllowed = false;
	private $massActionButtons = [];

	/**
	 * DataTable constructor.
	 * Any ORDER BY on the $query will be removed. It is applied later by either setDefaultOrderBy(), setDefaultOrderbyRaw() or order_by $_GET request
	 *
	 * @param Builder $query
	 * @param Request $request
	 * @param null    $emptyTableMessage
	 */
	public function __construct(Builder $query, Request $request, $emptyTableMessage = null)
	{
		$this->query = $query;
		$this->request = $request;

		$this->modelInstance = $query->getModel();
		$this->model = get_class($this->modelInstance);
		$this->modelSingular = snake_case(preg_replace('/^.*[^a-z]([A-Z])/', '$1', $this->model));
		$this->modelPlural = str_plural($this->modelSingular);

		$this->linkPrefix = $this->modelSingular . '/';

		if (is_null($emptyTableMessage)) {
			if (substr_count(trans('models.' . $this->modelPlural . '.messages.no_results'), '.') > 2) {
				$this->emptyTableMessage = trans('generic.emptyTableMessage');
			} else {
				$this->emptyTableMessage = trans('models.' . $this->modelPlural . '.messages.no_results');
			}
		} else {
			$this->emptyTableMessage = $emptyTableMessage;
		}
	}

	/**
	 * Adds a set of columns to the datatable
	 *
	 * @param Column[] ...$columns
	 */
	public function setColumns(Column ...$columns)
	{
		foreach ($columns as $column) {
			$this->columns[] = $column;
		}
	}

	/**
	 * Adds a single column to the datatable
	 *
	 * @param Column $column
	 */
	public function addColumn(Column $column)
	{
		$this->columns[] = $column;
	}

	/**
	 * Adds a set of filters to the datatable and applies them if desired
	 *
	 * @param FilterInterface[] ...$filters
	 */
	public function setFilters(FilterInterface ...$filters)
	{
		foreach ($filters as $key => $filter) {
			$filter->init($this->request, $key);
			if ($filter->isInUse()) {
				$this->query = $filter->apply($this->query);
			}
			$this->filters[] = $filter;
		}
	}

	/**
	 * Adds a closure (with the row object as an argument) that decides whether a row should be highlighted
	 *
	 * @param Closure $closure
	 * @param string  $accessorProperty
	 */
	public function highlightRows(Closure $closure, string $accessorProperty)
	{
		$this->highlightRowClosure = $closure;
		// New "unread" column to enable manual sorting by unread indicator and overall code simplification
		$this->addColumn(
			(new Column(self::unreadColumnName, ''))
				->orderByAccessor($accessorProperty)
				->class(self::unreadColumnName)
		);
	}

	public function allowMassActions()
	{
		$this->massActionsAllowed = true;
	}

	public function addMassActionButton(string $label, array $additionalAttributes = [])
	{
		$this->massActionButtons[] = [
			'label' => $label,
			'additionalAttributes' => $additionalAttributes,
		];
	}

	/**
	 * Sets widths to given columns, associative array key has to be the name of the column
	 *
	 * @param array $columnWidths
	 */
	public function setColumnWidths(array $columnWidths)
	{
		$this->columnWidths = $columnWidths;
	}

	/**
	 * Sets widths to given filters, associative array key has to be the name of the filter
	 *
	 * @param array $filterWidths
	 */
	public function setFilterWidths(array $filterWidths)
	{
		$this->filterWidths = $filterWidths;
	}

	/**
	 * Sets link prefix.
	 * In case a variable ID based on a relationship of the row is necessary within the prefix,
	 * set prefix to i.e. '/matter/{matter}/travel/' and it will be automatically converted to a number
	 *
	 * @param string $linkPrefix
	 */
	public function setLinkPrefix(string $linkPrefix)
	{
		$this->linkPrefix = $linkPrefix;
	}

	/**
	 * Sets link suffix
	 *
	 * @param string $linkSuffix
	 */
	public function setLinkSuffix(string $linkSuffix)
	{
		$this->linkSuffix = $linkSuffix;
	}

	/**
	 * Sets whether the link should open a modal or not
	 *
	 * @param bool $bool
	 */
	public function setLinkModal(bool $bool = true)
	{
		$this->linkModal = $bool;
	}

	/**
	 * Sets whether the link is a download link
	 *
	 * @param bool $bool
	 */
	public function setLinkDownload(bool $bool = true)
	{
		$this->linkDownload = $bool;
	}


	/**
	 * Sets default ORDER BY clause. Secondary order will take effect only if the primary column for ordering is an accessor
	 * for multiple order by columns, use setDefaultOrderByRaw()
	 *
	 * @param string      $orderByColumnName
	 * @param string      $orderDir
	 * @param string|null $secondaryOrderColumnName
	 * @param string      $secondaryOrderDir
	 */
	public function setDefaultOrderBy(string $orderByColumnName, string $orderDir = 'asc', string $secondaryOrderColumnName = null, string $secondaryOrderDir = 'asc')
	{
		$this->orderByColumnName = $orderByColumnName;
		$this->orderDir = $orderDir;
		$this->secondaryOrderColumnName = $secondaryOrderColumnName;
		$this->secondaryOrderDir = $secondaryOrderDir;
	}

	/**
	 * Sets default ORDER BY clause by raw SQL string
	 *
	 * @param string $rawOrderBy
	 */
	public function setDefaultOrderByRaw(string $rawOrderBy)
	{
		$this->orderByRaw = $rawOrderBy;
	}

	/**
	 * Sets number of items per page
	 *
	 * @param int $count
	 */
	public function setItemsPerPage(int $count)
	{
		$this->itemsPerPage = $count;
	}

	/**
	 * Sets whether big link™ feature is enabled
	 *
	 * @param bool $enabled
	 */
	public function setBigLinkEnabled(bool $enabled)
	{
		$this->bigLinkEnabled = $enabled;
	}

	/**
	 * Returns whether big link™ feature is enabled
	 *
	 * @return bool $enabled
	 */
	public function isBigLinkEnabled()
	{
		return $this->bigLinkEnabled;
	}

	/**
	 * Sets whether exports are allowed for this table
	 */
	public function setExportEnabled(bool $enabled = true)
	{
		$this->exportsAllowed = $enabled;
	}

	public function isExportEnabled()
	{
		return $this->exportsAllowed;
	}

	public function getRequest()
	{
		return $this->request;
	}

	public function getModelPlural()
	{
		return $this->modelPlural;
	}

	/**
	 * Applies ordering and pagination to the query
	 */
	private function setFinalOrderAndPagination(bool $onePagePagination = false)
	{
		if ($onePagePagination) {
			$this->itemsPerPage = 99999;
		}

		if (!$this->readyForRendering || $onePagePagination) {

			// If sorting (even default) should be applied
			if ($this->request->has('order_by') || $this->orderByColumnName) {

				// Find the column to sort by by name in $_GET, it may not exist
				if ($this->request->has('order_by')) {
					$columnNameToCompare = $this->request->input('order_by');
				} else {
					$columnNameToCompare = $this->orderByColumnName;
				}
				foreach ($this->columns as $column) {
					if ($column->name == $columnNameToCompare) {
						/** @var Column */
						$sortColumn = $column;
						break;
					}
				}

				// If the column wasn't found, never mind, but it should not happen unless the URL has been modified
				if (!isset($sortColumn)) {
					$this->paginator = $this->query->paginate($this->itemsPerPage);

					return;
				}

				// Set desired order direction
				if ($this->request->has('order_dir') && in_array(strtolower($this->request->input('order_dir')), ['asc', 'desc'])) {
					$this->orderDir = strtolower($this->request->input('order_dir'));
				}

				// Append ORDER BY to the SQL query if the column isn't an accessor, therefore can be sorted by the DB
				if (!$sortColumn->isAccessor) {
					$this->query = $sortColumn->applyOrdering($this->query, $this->orderDir, $this->modelInstance, $this->modelSingular);
				}

			} else if ($this->orderByRaw) {
				$this->query = $this->query->orderByRaw($this->orderByRaw);
			}

			// Paginate the results
			if ($this->request->has('quickfilter') || !isset($sortColumn) || (isset($sortColumn) && !$sortColumn->isAccessor)) {

				$this->paginator = $this->query->paginate($this->itemsPerPage);

			} else {

				// Sorting by an accessor column
				// Get complete collection
				$rows = $this->query->get();

				// Sort the collection by accessor
				if (!isset($this->secondaryOrderColumnName)) {
					$rows = $rows->sortBy($sortColumn->name, SORT_REGULAR, $this->orderDir == 'asc' ? false : true);
				} else {

					$criteria = [
						$sortColumn->name               => $this->orderDir,
						$this->secondaryOrderColumnName => $this->secondaryOrderDir,
					];
					$makeComparer = function($criteria) {
						$comparer = function($first, $second) use ($criteria) {
							foreach ($criteria as $key => $orderType) {
								if ($first[$key] < $second[$key]) {
									return $orderType === 'asc' ? -1 : 1;
								} else if ($first[$key] > $second[$key]) {
									return $orderType === 'asc' ? 1 : -1;
								}
							}

							return 0;
						};

						return $comparer;
					};
					$comparer = $makeComparer($criteria);

					$rows = $rows->sort($comparer);
				}

				// Convert the collection back into paginator
				$page = 1;
				if ($this->request->has('page') && is_numeric($this->request->input('page'))) {
					$page = $this->request->input('page');
				}
				$rowsForPage = $rows->forPage($page, $this->itemsPerPage);
				$this->paginator = new LengthAwarePaginator($rowsForPage, $rows->count(), $this->itemsPerPage, $page, [
					'path' => LengthAwarePaginator::resolveCurrentPath(),
				]);
			}

			// Prevent calling this method's body all over again
			$this->readyForRendering = true;
		}
	}


	/**
	 * Renders the datatable as a whole, including filters and pagination
	 * @return string
	 */
	public function render()
	{
		$this->setFinalOrderAndPagination();

		$view = $this->renderFilters();
		$view .= $this->renderTable();

		return $view;
	}

	/**
	 * Renders the datatable filters only. An exclusive part of render() method, don't use on its own
	 * @return string
	 */
	private function renderFilters()
	{
		if ($this->filters || $this->exportsAllowed) {
			$this->calculateFilterWidths();
			return view('BE.components.filters', [
				'filters'              => $this->filters,
				'exportsAllowed'       => $this->exportsAllowed,
				'extraQueryParameters' => $this->extraQueryParameters,
				'modelPlural'          => $this->modelPlural,
			])->render();
		}

		return '';
	}

	/**
	 * Calculates widths of the datatable filters
	 */
	private function calculateFilterWidths()
	{
		$filtersWithNoWidthSetArrayKeys = [];
		$widthLeft = 100;

		foreach ($this->filters as $filter) {
			if (isset($this->filterWidths[$filter->name]) && is_numeric($this->filterWidths[$filter->name])) {
				$widthLeft -= $this->filterWidths[$filter->name];
			} else {
				$filtersWithNoWidthSetArrayKeys[] = $filter->name;
			}
		}

		$widthPerFilter = round($widthLeft / count($filtersWithNoWidthSetArrayKeys), 4);
		foreach ($this->filters as $filter) {
			if (in_array($filter->name, $filtersWithNoWidthSetArrayKeys)) {
				$this->filterWidths[$filter->name] = $widthPerFilter;
			}
		}

		foreach ($this->filters as $filter) {
			$filter->setWidth($this->filterWidths[$filter->name]);
		}
	}

	/**
	 * Renders the datatable only. An exclusive part of render() method, don't use on its own
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	private function renderTable()
	{
		// Subtotals counts of each desired column
		foreach ($this->columns as &$column) {
			if ($column->subtotalAllowed) {
				$column->subtotal = 0;
				$column->subtotals = [];
				foreach ($this->paginator as $row) {
					if ($column->format) {
						// Complicated case, column values are formatted and can differ per row (i.e. currencies) => multiple subtotals
						$formatKey = $column->format->__invoke(1, $row);
						if (!isset($column->subtotals[$formatKey])) {
							$column->subtotals[$formatKey]['subtotal'] = 0;
							$column->subtotals[$formatKey]['rowForFormat'] = $row;
						}
						$column->subtotals[$formatKey]['subtotal'] += $row->{$column->name};
					} else {
						// Simple case, column values are not formatted
						$column->subtotal += $row->{$column->name};
					}
				}
				$showSubtotals = true;
			}
		}

		$anyFilterInUse = false;
		foreach ($this->filters as $filter) {
			if ($filter->isInUse()) {
				$anyFilterInUse = true;
				break;
			}
		}

		$this->calculateColumnWidths();

		return view('BE.components.datatable', [
			'modelSingular'                  => $this->modelSingular,
			'modelPlural'                    => $this->modelPlural,
			'request'                        => $this->request,
			'columns'                        => $this->columns,
			'columnWidths'                   => $this->columnWidths,
			'orderDir'                       => $this->orderDir,
			'emptyTableMessage'              => $this->emptyTableMessage,
			'linkPrefix'                     => $this->linkPrefix,
			'linkSuffix'                     => $this->linkSuffix,
			'linkModal'                      => $this->linkModal,
			'linkDownload'                   => $this->linkDownload,
			'highlightRowClosure'            => $this->highlightRowClosure,
			'unreadColumnName'               => self::unreadColumnName,
			'paginator'                      => $this->paginator,
			'showSubtotals'                  => isset($showSubtotals) ? true : false,
			'anyFilterInUse'                 => $anyFilterInUse,
			'bigLinkEnabled'                 => $this->bigLinkEnabled,
			'exportsEnabled'                 => $this->exportsAllowed,
			'massActionsEnabled'             => $this->massActionsAllowed,
			'massActionButtons'              => $this->massActionButtons,
		])->render();
	}

	/**
	 * Calculates widths of the datatable columns
	 */
	private function calculateColumnWidths()
	{
		$columnsWithNoWidthSetArrayKeys = [];
		$widthLeft = 100;

		// Check if there is "unread" type of column and if so, set its width to 1% unless the width has been already set by a programmer
		foreach ($this->columns as $column) {
			if ($column->name == self::unreadColumnName) {
				$columnWidthAlreadySet = false;
				foreach ($this->columnWidths as $key => $column_width) {
					if ($key == self::unreadColumnName) {
						$columnWidthAlreadySet = true;
						break;
					}
				}
				if (!$columnWidthAlreadySet) {
					$this->columnWidths[self::unreadColumnName] = self::UNREAD_COLUMN_WIDTH;
					$widthLeft -= self::UNREAD_COLUMN_WIDTH;
				}
				break;
			}
		}

		// Find columns with no width set
		foreach ($this->columns as $column) {
			if (isset($this->columnWidths[$column->name]) && is_numeric($this->columnWidths[$column->name])) {
				$widthLeft -= $this->columnWidths[$column->name];
			} else {
				$columnsWithNoWidthSetArrayKeys[] = $column->name;
			}
		}

		// Calculate width per column of those with no width set and set it
		$widthPerColumn = round($widthLeft / count($columnsWithNoWidthSetArrayKeys), 4);
		foreach ($this->columns as $column) {
			if (in_array($column->name, $columnsWithNoWidthSetArrayKeys)) {
				$this->columnWidths[$column->name] = $widthPerColumn;
			}
		}
	}


	/**
	 * Exports the datatable if exporting is requested & allowed for this datatable
	 * @return bool|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\StreamedResponse
	 */
	public function export()
	{
		if ($this->request->has('export') && $this->isExportEnabled()) {
			$exportType = $this->request->input('export');
			$exporter = new DataTableExporter($this);
			return $exporter->export($exportType);
		}
		return false;
	}

	/**
	 * Converts the datatable to a two-dimensional array
	 *
	 * @param bool $withoutHeader
	 *
	 * @return array
	 */
	public function toArray($withoutHeader = false)
	{
		$this->setFinalOrderAndPagination(true);
		$tableArray = [];

		// Add header array to the array
		if (!$withoutHeader) {
			$rowArray = [];
			foreach ($this->columns as $column) {
				if (!$column->heading) {
					if ($column->name == 'id') {
						$rowArray[] = trans('generic.number_abbr_cap');
					} else if ($column->name == self::unreadColumnName) {
						continue;
					} else {
						$rowArray[] = trans('models.' . $this->modelPlural . '.fields.' . $column->name);
					}
				} else {
					$rowArray[] = $column->heading;
				}
			}
			$tableArray[] = $rowArray;
		}

		foreach ($this->paginator as $row) {
			$rowArray = [];
			foreach ($this->columns as $column) {
				$value = $row->getAttributeWithDotSyntax($column->select);
				if ($column->format) {
					$rowArray[] = $column->format->__invoke($value, $row);
				} else if ($column->view) {
					$newValue = '';
					// Parse the HTML of the view and get the value of data-datatable attribute instead
					$html = $column->view->with('value', $value)->with('row', $row)->render();
					if ($html) {
						$dom = new \DOMDocument();
						$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
						$xpath = new \DOMXPath($dom);
						$elements = $xpath->query("//*[@data-datatable]");
						foreach ($elements as $key => $element) {
							$dataTableValue = $element->getAttribute("data-datatable");
							if ($dataTableValue) {
								if ($newValue) {
									$newValue .= ', ';
								}
								$newValue .= $dataTableValue;
							}
						}
					}
					$rowArray[] = $newValue;
				} else if (is_string($value) || is_numeric($value)) {
					$rowArray[] = $value;
				} else if ($value instanceof Model) {
					$rowArray[] = $value->name;
				} else if ($value instanceof SupportCollection) {
					$names = $value->lists('name');
					$rowArray[] = implode(', ', $names);
				} else if ($value instanceof Carbon) {
					$rowArray[] = $value->format(str_contains($column->name, '_time') ? 'H:i' : 'd.m.Y');
				} else if (method_exists($value, '__toString')) {
					$rowArray[] = $value;
				} else if (is_null($value)) {
					$rowArray[] = '';
				} else if ($column->name == self::unreadColumnName) {
					continue;
				}
			}
			$tableArray[] = $rowArray;
		}

		return $tableArray;
	}

	/**
	 * Sets array of extra query parameters to DataTable class. These extra parameters are added to query parameters
	 * of filters. This way we can add extra parameters that are preserved after filtering is applied.
	 * The query parameter array has this format:
	 *   - 'name_of_query_parameter' => 'value_of_query_parameter'
	 *
	 * @param array $queryParameters Array of extra query parameters
	 */
	public function setExtraQueryParameters(array $queryParameters)
	{
		$this->extraQueryParameters = $queryParameters;
	}
}
