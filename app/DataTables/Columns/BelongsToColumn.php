<?php

namespace App\DataTables\Columns;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;

class BelongsToColumn extends Column
{
	/**
	 * Ordering by a property of a related model
	 *
	 * @param  Builder $query
	 * @param  string  $orderDir
	 * @param  Model   $modelInstance
	 * @param  string  $modelSingular
	 *
	 * @return Builder
	 */
	public function applyOrdering(Builder $query, string $orderDir, Model $modelInstance, string $modelSingular)
	{
		$relationshipParts = explode('.', $this->orderBy);
		$relationshipName = $relationshipParts[1]; // e.g. "owner" or "vat_rate"
		$getRelatedModelMethodName = camel_case($relationshipParts[1]); // e.g. "owner" or "vatRate"
		$relatedTableColumnName = $relationshipParts[2]; // e.g. "name" of a matter owner

		// Calling relationship method of a model, e.g. Matter::owner() through $getRelatedModelMethodName()
		$relatedTable = $modelInstance->$getRelatedModelMethodName()->getRelated()->getTable();

		// E.g.: select('matters.*')->leftJoin('users', 'matters.owner_id', '=', 'users.id')
		$query = $query
			->select($modelInstance->getTable() . '.*')
			->leftJoin($relatedTable, function(JoinClause $join) use ($modelInstance, $relationshipName, $relatedTable) {
				$join->on($modelInstance->getTable() . '.' . $relationshipName . '_id', '=', $relatedTable . '.id');
			});

		// E.g.: 'users.id'
		$orderBy = $relatedTable . '.' . $relatedTableColumnName;

		return $query->orderBy($orderBy, $orderDir);
	}
}
