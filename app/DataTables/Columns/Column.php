<?php

namespace App\DataTables\Columns;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\View\View;

class Column
{
	public $name;
	public $heading;
	public $select;
	public $orderBy;
	public $isAccessor;
	public $format;
	public $view;
	public $class;
	public $subtotalAllowed;
	public $subtotal;
	public $subtotals;

	public function __construct(string $name, string $heading = null)
	{
		$this->name = $name;
		$this->select = $name;
		if ($heading) {
			$this->heading = $heading;
		}
	}

	/**
	 * Set the property the column will show, default is the name of the column
	 *
	 * @param string $columnName
	 *
	 * @return $this
	 */
	public function select(string $columnName)
	{
		$this->select = $columnName;

		return $this;
	}

	/**
	 * Set the property by which the datatable will be sorted if sorted by $this column
	 *
	 * @param string $columnName
	 *
	 * @return $this
	 */
	public function orderBy(string $columnName)
	{
		$this->orderBy = $columnName;
		$this->isAccessor = false;

		return $this;
	}

	/**
	 * Set the accessor property by which the datatable will be sorted if sorted by $this column
	 *
	 * @param string $columnName
	 *
	 * @return $this
	 */
	public function orderByAccessor(string $columnName)
	{
		$this->orderBy = $columnName;
		$this->isAccessor = true;

		return $this;
	}

	/**
	 * Set closure to format the column's content by
	 *
	 * @param Closure $format
	 *
	 * @return $this
	 */
	public function format(Closure $format)
	{
		$this->format = $format;

		return $this;
	}

	/**
	 * Set the view to format the column's content by
	 *
	 * @param View $view
	 *
	 * @return $this
	 */
	public function formatView(View $view)
	{
		$this->view = $view;

		return $this;
	}

	/**
	 * Set class this column will have in the datatable
	 *
	 * @param string $class
	 *
	 * @return $this
	 */
	public function class(string $class)
	{
		$this->class = $class;

		return $this;
	}

	/**
	 * Set whether this column should have subtotals below the datatable
	 * @return $this
	 */
	public function subtotals()
	{
		$this->subtotalAllowed = true;

		return $this;
	}

	/**
	 * Ordering simply by a property of this model
	 *
	 * @param  Builder $query
	 * @param  string  $orderDir
	 * @param  Model   $modelInstance
	 * @param  string  $modelSingular
	 *
	 * @return Builder
	 */
	public function applyOrdering(Builder $query, string $orderDir, Model $modelInstance, string $modelSingular)
	{
		return $query->orderBy($this->orderBy, $orderDir);
	}
}
