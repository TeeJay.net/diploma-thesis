<?php

namespace App\DataTables\Columns;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;

class AggregateColumn extends Column
{
	/**
	 * Ordering by an aggregate function on a property of a related model
	 *
	 * @param  Builder $query
	 * @param  string  $orderDir
	 * @param  Model   $modelInstance
	 * @param  string  $modelSingular
	 *
	 * @return Builder
	 */
	public function applyOrdering(Builder $query, string $orderDir, Model $modelInstance, string $modelSingular)
	{
		$relationshipParts = explode('.', $this->orderBy);
		$getRelatedModelMethodName = camel_case($relationshipParts[1]); // e.g. "matters" of a client or "vatRate"
		$relatedTablePropertyAndValue = $relationshipParts[2]; // e.g. "open|1" of a client's matter
		$tmp = explode('|', $relatedTablePropertyAndValue); // e.g. [ 0 => "open", 1 => "1" ]
		$relatedTableProperty = $tmp[0]; // e.g. "open"
		$desiredValue = $tmp[1]; // e.g. "1"
		$aggregateFunction = $relationshipParts[3]; // e.g. "count" of a client's open matters

		// Calling relationship method of a model, e.g. Client::matters() through $getRelatedModelMethodName()
		$relatedTable = $modelInstance->$getRelatedModelMethodName()->getRelated()->getTable();

		// E.g.: selectRaw('clients.*, count(matters.open) aggregate_order_by')->leftJoin('users', function($join) {
		//		$join->on('clients.id', '=', 'matters.client_id');
		//		$join->where('matters.open', '=', 1);
		//		$join->whereNull('matters.deleted_at');
		//	 })->groupBy('clients.id');
		$query = $query
			->selectRaw($modelInstance->getTable() . '.*, ' . $aggregateFunction . '(' . $relatedTable . '.' . $relatedTableProperty . ') aggregate_order_by')
			->leftJoin($relatedTable, function(JoinClause $join) use ($modelInstance, $relatedTable, $relatedTableProperty, $desiredValue, $modelSingular) {
				$join->on($modelInstance->getTable() . '.' . 'id', '=', $relatedTable . '.' . $modelSingular . '_id');
				$join->where($relatedTable . '.' . $relatedTableProperty, '=', $desiredValue);
				$join->whereNull($relatedTable . '.deleted_at');
			})->groupBy($modelInstance->getTable() . '.id');

		$orderBy = 'aggregate_order_by';

		return $query->orderBy($orderBy, $orderDir);
	}
}
