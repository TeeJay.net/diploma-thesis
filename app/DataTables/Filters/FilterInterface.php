<?php

namespace App\DataTables\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

interface FilterInterface
{
	public function __construct(array $options = null);

	/**
	 * Initializes everything the filter needs in order to work
	 *
	 * @param Request $request
	 */
	public function init(Request $request);

	/**
	 * Sets name of the filter and the name used in the query string
	 *
	 * @param string $filterName
	 *
	 * @return $this
	 */
	public function setName(string $filterName);

	/**
	 * Returns whether the filter is currently in use
	 * @return bool
	 */
	public function isInUse();

	/**
	 * Applies the filter on the query
	 *
	 * @param Builder $query
	 *
	 * @return Builder
	 */
	public function apply(Builder $query);

	/**
	 * Renders the filter
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function render();
}
