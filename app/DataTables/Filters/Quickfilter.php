<?php

namespace App\DataTables\Filters;

use Illuminate\Database\Eloquent\Builder;

class QuickFilter extends Filter
{
	public $name = 'quickfilter';
	protected $queryStringName = 'quickfilter';

	/**
	 * Returns whether the filter is currently in use
	 * @return bool
	 */
	public function isInUse()
	{
		return $this->request->has($this->queryStringName) && strlen(trim($this->request->input($this->queryStringName)));
	}

	/**
	 * Applies the filter on the query
	 *
	 * @param Builder $query
	 *
	 * @return Builder
	 */
	public function apply(Builder $query)
	{
		return $query->search($this->request->input($this->queryStringName));
	}

	/**
	 * Renders the filter
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function render()
	{
		return view('BE.components.filters.quick', [
			'quickfilter'   => true,
			'value'         => $this->request->input($this->queryStringName),
			'relativeWidth' => $this->width,
		]);
	}
}
