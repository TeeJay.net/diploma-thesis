<?php

namespace App\DataTables\Filters;

use Closure;
use Illuminate\Database\Eloquent\Builder;

class DropdownFilter extends Filter
{
	protected $label = '';
	protected $emptyOptionValue = 'All';
	protected $prompt = '';
	protected $options;
	protected $callback;
	protected $defaultOptionKey;

	public function __construct(array $options = null)
	{
		$this->options = $options;
	}

	/**
	 * Sets value of the dropdown filter that is chosen by default on page load
	 *
	 * @param string $optionKey
	 *
	 * @return $this
	 */
	public function setDefault(string $optionKey)
	{
		$this->defaultOptionKey = $optionKey;

		return $this;
	}

	/**
	 * Sets label of the dropdown filter
	 *
	 * @param string $label
	 *
	 * @return $this
	 */
	public function setLabel(string $label)
	{
		$this->label = $label;

		return $this;
	}

	/**
	 * Sets the value of the empty option that is applied by default and does not cause any filtering to occur
	 *
	 * @param string $description
	 *
	 * @return $this
	 */
	public function setEmptyOptionValue(string $description)
	{
		$this->emptyOptionValue = $description;

		return $this;
	}

	/**
	 * Sets callback closure on the filter to enable modifying the Builder $query for custom filtering options
	 *
	 * @param Closure $callback
	 *
	 * @return $this
	 */
	public function setCallback(Closure $callback)
	{
		$this->callback = $callback;

		return $this;
	}

	/**
	 * Returns whether the filter is currently in use
	 * @return bool
	 */
	public function isInUse()
	{
		// Return true if the filter is manually applied ($request has proper input with content)
		// or applied automatically (has $defaultOptionKey but is not set in $request)
		return ($this->request->has($this->queryStringName) && strlen(trim($this->request->input($this->queryStringName))))
			|| ($this->defaultOptionKey && !array_key_exists($this->queryStringName, $this->request->input()));
	}

	/**
	 * Applies the filter on the query
	 *
	 * @param Builder $query
	 *
	 * @return Builder
	 */
	public function apply(Builder $query)
	{
		// Determine the desired value to use for filtering
		$desiredValue = $this->request->get($this->queryStringName, $this->defaultOptionKey);

		if ($this->callback) {
			return $this->callback->__invoke($query, $desiredValue);
		} else {
			return $query->where($this->name, '=', $desiredValue);
		}
	}

	/**
	 * Renders the filter
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function render()
	{
		return view('BE.components.filters.dropdown', [
			'dropdownFilter'       => true,
			'relativeWidth'        => $this->width,
			'queryStringParamName' => $this->queryStringName,
			'label'                => $this->label,
			'prompt'               => $this->prompt,
			'options'              => ['' => $this->emptyOptionValue] + $this->options,
			'selected'             => $this->request->has($this->queryStringName) && strlen(trim($this->request->input($this->queryStringName)))
				? trim($this->request->input($this->queryStringName))
				: (array_key_exists($this->queryStringName, $this->request->input())
					? ''
					: ($this->defaultOptionKey ?: '')
				),
		]);
	}
}
