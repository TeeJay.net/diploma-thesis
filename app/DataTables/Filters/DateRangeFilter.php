<?php


namespace App\DataTables\Filters;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Log;

class DateRangeFilter extends Filter
{
	const FROM_QUERY_STRING = 'from';

	const TO_QUERY_STRING = 'to';

	/** @var Carbon */
	private $startDate;

	/** @var string */
	private $startDateFormatted;

	/** @var Carbon */
	private $endDate;

	/** @var string */
	private $endDateFormatted;

	/**
	 * @var callable Callback for filtering results. These parameters are passed to callback: Builder $query, Carbon $from, Carbon $to.
	 */
	private $filter;

	/**
	 * @var array Array of date ranges that will appear in the date range picker. It consists of key, which is used to
	 *            get translation (e.g. "this_month" key get translation from daterangepicker.ranges.this_month), and
	 *            value, which is array where the first element is the start of date range and the second element is the
	 *            end of date range.
	 */
	private $dateRanges = [];

	/**
	 * @var string Key from the $dateRanges array that is used to get default date range.
	 */
	private $defaultDateRangeKey = 'this_month';

	private $openSide = 'right';

	/**
	 * DateRangeFilter constructor.
	 */
	public function __construct(array $options = null)
	{
		parent::__construct($options);

		$this->dateRanges = [
			'today' => [Carbon::now()->startOfDay()->format('d.m.Y'), Carbon::now()->endOfDay()->format('d.m.Y')],
			'yesterday' => [Carbon::yesterday()->startOfDay()->format('d.m.Y'), Carbon::yesterday()->endOfDay()->format('d.m.Y')],
			'last_seven_days' => [Carbon::now()->subDays(7)->startOfDay()->format('d.m.Y'), Carbon::now()->format('d.m.Y')],
			'last_thirty_days' => [Carbon::now()->subDays(30)->startOfDay()->format('d.m.Y'), Carbon::now()->format('d.m.Y')],
			'this_month' => [Carbon::now()->startOfMonth()->format('d.m.Y'), Carbon::now()->endOfMonth()->format('d.m.Y')],
			'last_month' => [Carbon::now()->subMonth()->startOfMonth()->format('d.m.Y'), Carbon::now()->subMonth()->endOfMonth()->format('d.m.Y')],
		];
	}

	public function addDateRange(string $key, Carbon $start, Carbon $end): DateRangeFilter
	{
		$this->dateRanges[$key] = [$start->format('d.m.Y'), $end->format('d.m.Y')];
		return $this;
	}

	public function addDateRangeByQuery(string $key, string $dateColumn, Builder $query): DateRangeFilter
	{
		$newest = Carbon::now();
		$oldest = Carbon::now();

		$queryClone1 = clone $query;
		$oldestBillable = $queryClone1->orderBy($dateColumn)->first();
		if ($oldestBillable) {
			/** @var Carbon $oldestBillableDate */
			$oldestBillableDate = $oldestBillable->$dateColumn;
			if ($oldestBillableDate->lt($oldest)) {
				$oldest = $oldestBillableDate;
			}
		}

		$queryClone2 = clone $query;
		$newestBillable = $queryClone2->orderBy($dateColumn, 'desc')->first();
		if ($newestBillable) {
			/** @var Carbon $oldestBillableDate */
			$newestBillableDate = $newestBillable->$dateColumn;
			if ($oldestBillableDate->lt($oldest)) {
				$newest = $newestBillableDate;
			}
		}

		$this->dateRanges[$key] = [
			$oldest->startOfDay()->format('d.m.Y'),
			$newest->endOfDay()->format('d.m.Y')
		];

		return $this;
	}

	public function removeDateRange(string $key): DateRangeFilter
	{
		unset($this->dateRanges[$key]);
		return $this;
	}

	public function setDefaultDateRangeByKey(string $key): DateRangeFilter
	{
		if (!array_key_exists($key, $this->dateRanges)) {
			throw new \OutOfRangeException("$key doesn\'t exist");
		}
		$this->defaultDateRangeKey = $key;

		return $this;
	}

	public function getDefaultDateRange(): array
	{
		return [
			'from' => $this->dateRanges[$this->defaultDateRangeKey][0],
			'to' => $this->dateRanges[$this->defaultDateRangeKey][1],
		];
	}

	public function setDateRange(Carbon $startDate, Carbon $endDate)
	{
		$this->startDate = $startDate;
		$this->startDateFormatted = $startDate->format('Y-m-d H:i:s');

		$this->endDate = $endDate;
		$this->endDateFormatted = $endDate->format('Y-m-d H:i:s');
	}

	public function setFilter(callable $filter)
	{
		$this->filter = $filter;

		return $this;
	}

	public function init(Request $request)
	{
		parent::init($request);

		try {
			if ($request->has(self::FROM_QUERY_STRING)) {
				$this->startDate = carbonAutodetect($request->get(self::FROM_QUERY_STRING))
					->setTime(0, 0, 0);
			} else {
				$this->startDate = Carbon::createFromFormat('d.m.Y', $this->getDefaultDateRange()['from']);
			}
		} catch (\InvalidArgumentException $e) {
			$this->startDate = Carbon::create(1970, 01, 01);
			Log::error($e->getMessage() . PHP_EOL . $e->getTraceAsString());
		}

		try {
			if ($request->has(self::TO_QUERY_STRING)) {
				$this->endDate = carbonAutodetect($request->get(self::TO_QUERY_STRING))
					->setTime(23, 59, 59);
			} else {
				$this->endDate = Carbon::createFromFormat('d.m.Y', $this->getDefaultDateRange()['to']);
			}
		} catch (\InvalidArgumentException $e) {
			$this->endDate = Carbon::now();
			Log::error($e->getMessage() . PHP_EOL . $e->getTraceAsString());
		}

		$this->startDateFormatted = $this->startDate->format('Y-m-d H:i:s');
		$this->endDateFormatted = $this->endDate->format('Y-m-d H:i:s');
	}


	public function apply(Builder $query)
	{
		if ($this->filter) {
			return $this->filter->__invoke($query, $this->startDate, $this->endDate);
		} else {
			return $query->whereBetween('created_at', [$this->startDateFormatted, $this->endDateFormatted]);
		}
	}

	public function isInUse()
	{
		if ($this->request->has(self::FROM_QUERY_STRING) && $this->request->has(self::TO_QUERY_STRING)) {
			return true;
		}
		return false;
	}

	public function render()
	{
		return view('BE.components.filters.daterange', [
			'dateRangeFilter'      => true,
			'relativeWidth'        => $this->width,
			'queryParameters'      => [
				'from' => $this->startDate->format('d.m.Y'),
				'to'   => $this->endDate->format('d.m.Y'),
			],
			'dateRanges' => json_encode($this->dateRanges),
			'defaultDateRange' => $this->getDefaultDateRange(),
			'openSide' => $this->openSide,
		]);
	}

	public function setOpenSide(string $openSide): DateRangeFilter
	{
		$this->openSide = $openSide;
		return $this;
	}

	public function getOpenSide(): string
	{
		return $this->openSide;
	}
}
