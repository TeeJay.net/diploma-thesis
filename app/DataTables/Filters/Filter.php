<?php

namespace App\DataTables\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class Filter implements FilterInterface
{
	/**
	 * @var Request
	 */
	protected $request;
	public $name;
	protected $queryStringName;
	public $width;

	public function __construct(array $options = null)
	{
	}

	/**
	 * Initializes everything the filter needs in order to work
	 *
	 * @param Request $request
	 */
	public function init(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * Sets name of the filter (default is 'quickfilter') and the name used in the query string ('quickfilter' too)
	 *
	 * @param string $filterName
	 *
	 * @return $this
	 */
	public function setName(string $filterName)
	{
		$this->name = $filterName;
		$this->queryStringName = strtr($filterName, '.', '_');

		return $this;
	}

	public function setWidth(float $widthPercentage)
	{
		$this->width = $widthPercentage;
	}

	/**
	 * Returns whether the filter is currently in use
	 * @return bool
	 */
	public function isInUse()
	{
		return false;
	}

	/**
	 * Applies the filter on the query
	 *
	 * @param Builder $query
	 *
	 * @return Builder
	 */
	public function apply(Builder $query)
	{
		return $query->search($this->request->input($this->queryStringName));
	}

	/**
	 * Renders the filter
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function render()
	{
		return '';
	}
}
