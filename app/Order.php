<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

/**
 * App\Order
 *
 * @property int                  $id
 * @property string|null          $company
 * @property string               $first_name
 * @property string               $last_name
 * @property string|null          $street
 * @property string|null          $house_number
 * @property string|null          $city
 * @property string|null          $postcode
 * @property int|null             $distance
 * @property string               $email
 * @property string               $phone
 * @property string               $print
 * @property \Carbon\Carbon       $rental_from
 * @property \Carbon\Carbon       $rental_to
 * @property string|null          $note
 * @property int                  $delivery_price
 * @property int                  $final_price
 * @property string|null          $final_price_words
 * @property int                  $confirmed
 * @property int                  $background_id
 * @property string|null          $invoice_number
 * @property string|null          $invoice_first_name
 * @property string|null          $invoice_last_name
 * @property string|null          $invoice_street
 * @property string|null          $invoice_house_number
 * @property string|null          $invoice_city
 * @property string|null          $invoice_postcode
 * @property string|null          $invoice_ic
 * @property string|null          $invoice_dic
 * @property \Carbon\Carbon|null  $invoice_date_created
 * @property \Carbon\Carbon|null  $invoice_date_due
 * @property string|null          $invoice_vs
 * @property string|null          $invoice_event
 * @property string|null          $contract_html
 * @property string|null          $invoice_html
 * @property int|null             $created_by_user_id
 * @property int|null             $updated_by_user_id
 * @property \Carbon\Carbon|null  $created_at
 * @property \Carbon\Carbon|null  $updated_at
 * @property \Carbon\Carbon|null  $deleted_at
 * @property-read \App\Background $background
 * @property-read \App\User|null  $createdByUser
 * @property-read \App\Gallery    $gallery
 * @property-read mixed           $name
 * @property-read \App\User|null  $updatedByUser
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Order onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order search($string)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereBackgroundId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereContractHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereDeliveryPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereDistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereFinalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereFinalPriceWords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereHouseNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceDateCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceDateDue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceDic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceEvent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceHouseNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceIc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoicePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceVs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePrint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereRentalFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereRentalTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUpdatedByUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Order withoutTrashed()
 * @mixin \Eloquent
 */
class Order extends BaseModel
{
	use SoftDeletes;

	protected $table = 'orders';
	protected $guarded = [];
	protected $dates = [
		'deleted_at', 'rental_from', 'rental_to', 'invoice_date_created', 'invoice_date_due',
	];
	protected $printOptions;

	/*
	 * Relationships
	 */
	public function background()
	{
		return $this->belongsTo(Background::class);
	}

	public function gallery()
	{
		return $this->belongsTo(Gallery::class);
	}

	public function createdByUser()
	{
		return $this->belongsTo(User::class);
	}

	public function updatedByUser()
	{
		return $this->belongsTo(User::class);
	}

	/*
	 * Scopes
	 */
	public function scopeSearch(Builder $query, string $string)
	{
		$query = $query->where(function(Builder $query) use ($string) {
			$queryWithSearchByDate = $this->quickSearchByCreatedAtDate($query, $string, $this->table, ['rental_from', 'rental_to']);
			if ($queryWithSearchByDate) {
				$query = $queryWithSearchByDate;
			};
			return $query->orWhere('first_name', 'LIKE', '%' . $string . '%')
				->orWhere('last_name', 'LIKE', '%' . $string . '%')
				->orWhere('email', 'LIKE', '%' . $string . '%')
				->orWhere('phone', 'LIKE', '%' . $string . '%')
				->orWhere('location', 'LIKE', '%' . $string . '%')
				->orWhere('note', 'LIKE', '%' . $string . '%')
				->orWhere('final_price', 'LIKE', $string . '%');
		});

		return $this->orderByIdIfNumber($query, $string, $this->table);
	}

	/*
	 * Other methods
	 */
	public function getNameAttribute()
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	public function getPriceTogetherAttribute()
	{
		return $this->final_price + $this->delivery_price;
	}

	public static function getPrintOptions()
	{
		return Cache::rememberForever('printOptions', function() {
			$settings = getSettings(['print_price_no', 'print_price_photo', 'print_price_free_for_all']);
			$priceNo = 0;
			$pricePhoto = 0;
			$priceAll = 0;
			foreach ($settings as $setting) {
				if ($setting->key == 'print_price_no') {
					$priceNo = $setting->value;
				} else if ($setting->key == 'print_price_photo') {
					$pricePhoto = $setting->value;
				} else {
					$priceAll = $setting->value;
				}
			}

			return [
				'no'                    => trans('models.orders.fields.print_options.no') . ' (' . $priceNo . ' Kč)',
				'print_price_per_photo' => trans('models.orders.fields.print_options.print_price_per_photo') . ' ' . $pricePhoto . ' Kč',
				'print_free_for_all'    => trans('models.orders.fields.print_options.print_free_for_all') . ' (' . $priceAll . ' Kč)',
			];
		});
	}

	public function sendEmailOrderCreated()
	{
		Mail::send('emails.order-created', [
			'order' => $this,
		], function(Message $message) {
			$message->to($this->email);
			$message->subject(trans('models.orders.emails.subject_created') . ' | ' . config('app.name'));
		});

	}

	public function sendEmailOrderConfirmed()
	{
		Mail::send('emails.order-confirmed', [
			'order' => $this,
		], function(Message $message) {
			$message->to($this->email);
			$message->subject(trans('models.orders.emails.subject_confirmed') . ' | ' . config('app.name'));

			$name = $this->invoice_first_name
				? $this->invoice_first_name . ' ' . $this->invoice_last_name
				: $this->first_name . ' ' . $this->last_name;
			$pathToInvoice = storage_path('app/invoices/' . str_slug($this->id . '-faktura-' . $name) . '.pdf');
			$pathToContract = storage_path('app/contracts/' . str_slug($this->id . '-smlouva-' . $name) . '.pdf');

			$message->attach($pathToInvoice);
			$message->attach($pathToContract);
		});
	}

	public function createInvoice(string $presetHtml = null)
	{
		$settings = getSettings(['invoice_template']);

		$mpdf = new \mPDF('cs_CZ', 'A4', 0, 'opensanslight');

		$replaceables = [
			'cislo_faktury'    => $this->invoice_number ?: '-',
			'jmeno_odberatele' => $this->invoice_first_name
				? $this->invoice_first_name . ' ' . $this->invoice_last_name
				: $this->first_name . ' ' . $this->last_name,
			'ulice_a_cp'       => $this->invoice_street . ' ' . $this->invoice_house_number,
			'mesto_a_psc'      => $this->invoice_city . ' ' . $this->invoice_postcode,
			'ico'              => $this->invoice_ic ?: '-',
			'dic'              => $this->invoice_dic ?: '-',
			'datum_vystaveni'  => $this->invoice_date_created ? $this->invoice_date_created->format('d.m.Y') : '-',
			'datum_splatnosti' => $this->invoice_date_due ? $this->invoice_date_due->format('d.m.Y') : '-',
			'vs'               => $this->invoice_vs ?: '-',
			'akce'             => $this->invoice_event ?: '-',
			'pronajem_od'      => $this->rental_from->format('d.m.Y'),
			'doprava_cena'     => $this->delivery_price,
			'finalni_cena'     => $this->final_price,
			'cena_dohromady'   => $this->final_price + $this->delivery_price,
			'assets_path'      => asset('/'),
		];

		if (!$presetHtml) {
			$html = $settings->get('invoice_template')->value;
			foreach ($replaceables as $key => $value) {
				$html = str_replace('[' . $key . ']', $value, $html);
			}
		} else {
			$html = $presetHtml;
		}

		$mpdf->WriteHTML($html);
		$filePath = $this->getPathTo('invoices');
		$fileName = str_slug($this->id . '-faktura-' . $replaceables['jmeno_odberatele']);
		$this->invoice_html = $html;
		$this->save();

		// Save the invoice for historical purposes and emails
		$mpdf->Output($filePath . $fileName . '.pdf', 'F');
	}

	public function createContract(string $presetHtml = null)
	{
		$settings = getSettings(['contract1_template', 'contract2_template', 'contract3_template', 'contract_header', 'contract_footer']);
		$mpdf = new \mPDF('cs_CZ', 'A4', 0, 'opensanslight', 15, 15, 35, 35);

		$header = str_replace('[assets_path]', asset('/'), $settings->get('contract_header')->value);
		$footer = str_replace('[assets_path]', asset('/'), $settings->get('contract_footer')->value);
		$mpdf->SetHTMLHeader($header);
		$mpdf->SetHTMLFooter($footer);
		$mpdf->setAutoTopMargin = 'stretch';
		$mpdf->setAutoBottomMargin = 'stretch';

		$replaceables = [
			'firma_nebo_jmeno_odberatele' => $this->company
				? $this->company
				: ($this->invoice_first_name
					? $this->invoice_first_name . ' ' . $this->invoice_last_name
					: $this->first_name . ' ' . $this->last_name),
			'ulice_a_cp'                  => $this->invoice_street . ' ' . $this->invoice_house_number,
			'mesto_a_psc'                 => $this->invoice_city . ' ' . $this->invoice_postcode,
			'ico'                         => $this->invoice_ic ?: '-',
			'jmeno_odberatele'            => $this->invoice_first_name
				? $this->invoice_first_name . ' ' . $this->invoice_last_name
				: $this->first_name . ' ' . $this->last_name,
			'pronajem_od_datum_cas'       => $this->rental_from->format('d.m.Y H:i'),
			'pronajem_do_datum_cas'       => $this->rental_from->format('d.m.Y H:i'),
			'finalni_cena'                => $this->final_price,
			'finalni_cena_slovy'          => $this->final_price_words,
			'cislo_pozadi'                => $this->background_id,
			'datum_vystaveni'             => $this->invoice_date_created ? $this->invoice_date_created->format('d.m.Y') : '-',
			'assets_path'                 => asset('/'),
		];

		if (!$presetHtml) {
			if ($this->print == 'no') {
				$html = $settings->get('contract1_template')->value;
			} else if ($this->print == 'print_price_per_photo') {
				$html = $settings->get('contract2_template')->value;
			} else {
				$html = $settings->get('contract3_template')->value;
			}

			foreach ($replaceables as $key => $value) {
				$html = str_replace('[' . $key . ']', $value, $html);
			}
		} else {
			$html = $presetHtml;
		}

		$mpdf->WriteHTML($html);
		$filePath = $this->getPathTo('contracts');
		$fileName = str_slug($this->id . '-smlouva-' . $replaceables['jmeno_odberatele']);
		$this->contract_html = $html;
		$this->save();

		// Save the invoice for historical purposes and emails
		$mpdf->Output($filePath . $fileName . '.pdf', 'F');
	}

	public function setInvoiceNumber()
	{
		if ($this->invoice_number) {
			return true;
		}

		$invoiceNumber = getSettings(['next_invoice_number'])->get('next_invoice_number')->value;

		\DB::update('
			update settings
			set `value` = value + 1
			where `key` = "next_invoice_number"
		');

		$this->invoice_number = $invoiceNumber;
		$this->save();
	}

	public function setDistance()
	{
		if ($this->distance != 0) { // if a distance is set and the new one should be zero, keep the old one
			return;
		}

		// See https://developers.google.com/maps/documentation/distance-matrix/intro
		$googleMapsDistanceMatrixAPI = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric';
		$originParam = '&origins=50.2035283,15.8145034'; // Zeyerova 693/4 Hradec Králové
		$destinationsParam = '&destinations=' . urlencode($this->street . ' ' . $this->house_number . ', ' . $this->postcode . ' ' . $this->city);
		$keyParam = '&key=AIzaSyAqnAyubJNmXD2joD2Sx_Sj6fGZMWBMnkk';
		$url = $googleMapsDistanceMatrixAPI . $originParam . $destinationsParam . $keyParam;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // don't verify SSL, no harm possible
		$results = json_decode(curl_exec($ch), true);
		curl_close($ch);

		$distance = 0;
		if (isset($results['rows'][0]['elements'][0]['distance']['value'])) {
			$distance = $results['rows'][0]['elements'][0]['distance']['value'] / 1000;
		}

		if ($distance == 0) { // if a distance is set and the new one should be zero, keep the old one
			return;
		}
		$this->distance = round($distance);
		$this->save();
	}

	public function setDeliveryPrice()
	{
		if ($this->delivery_price != 0) { // if a distance is set and the new one should be zero, keep the old one
			return;
		}

		$settings = getSettings(['price_per_km', 'free_km_one_way']);

		$pricePerKm = $settings->get('price_per_km')->value;
		$freeKmOneWay = $settings->get('free_km_one_way')->value;
		$totalDistanceToPay = ($this->distance - $freeKmOneWay) * 2;
		$this->delivery_price = $totalDistanceToPay * $pricePerKm;
		$this->save();
	}
}
