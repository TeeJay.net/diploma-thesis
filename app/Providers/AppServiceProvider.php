<?php

namespace App\Providers;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use League\Glide\Responses\LaravelResponseFactory;
use League\Glide\Server;
use League\Glide\ServerFactory;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		// Necessary when running MySQL older than 5.7.7 or MariaDB 10.2.2 release
		Schema::defaultStringLength(191);

		// Setting up Glide
		$this->app->singleton(Server::class, function($app) {

			$filesystem = $app->make(Filesystem::class);
			$config = config('glide');

			return ServerFactory::create([
				'source'             => $filesystem->getDriver(),
				'cache'              => $filesystem->getDriver(),
				'source_path_prefix' => $config['source_path_prefix'],
				'cache_path_prefix'  => $config['cache_path_prefix'],
				'response'           => new LaravelResponseFactory($app['request']),
			]);
		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
