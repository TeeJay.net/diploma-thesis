<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class MakeUser extends Command
{
	protected $signature = 'make:user';
	protected $description = 'Adds a user';

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$firstName = $this->ask('Type your first name (without accents) and then hit enter');
		$lastName = $this->ask('Type your last name (without accents) and then hit enter');
		$email = $this->ask('Type your email and then hit enter');
		$password = $this->secret("Type your password and then hit enter");

		$user = new User();
		$user->fill([
			'first_name'  => $firstName,
			'last_name'   => $lastName,
			'public_name' => $firstName . ' ' . $lastName,
			'email'       => $email,
			'password'    => bcrypt($password),
			'permissions' => json_encode(\Config::get('permissions')),
		]);
		$user->save();

		$this->info("User $email was created");
	}
}
