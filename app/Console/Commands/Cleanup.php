<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Cleanup extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'cleanup';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Clears all cache including config, route, view and complied-view.';

    /**
     * Execute the command.
     *
     * @return int
     */
    public function handle()
    {
		$this->line('Clearing aplication cache');
		$this->call('cache:clear');
		$this->line('Clearing config cache');
		$this->call('config:clear');
		$this->line('Clearing route cache');
		$this->call('route:clear');
		$this->line('Clearing view cache');
		$this->call('view:clear');
		$this->line('Clearing compiled views cache');
		$this->call('clear-compiled');
		$this->line('Clearing debugbar cache');
		$this->call('debugbar:clear');
    }
}
