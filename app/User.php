<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Support\Facades\Storage;

/**
 * App\User
 *
 * @property int                                                             $id
 * @property string                                                          $first_name
 * @property string                                                          $last_name
 * @property string                                                          $public_name
 * @property string                                                          $email
 * @property string                                                          $password
 * @property string|null                                                     $permissions
 * @property int|null                                                        $created_by_user_id
 * @property int|null                                                        $updated_by_user_id
 * @property string|null                                                     $remember_token
 * @property \Carbon\Carbon|null                                             $created_at
 * @property \Carbon\Carbon|null                                             $updated_at
 * @property \Carbon\Carbon|null                                             $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Audit[]      $audit
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Article[]    $createdArticles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Background[] $createdBackgrounds
 * @property-read \App\User|null                                             $createdByUser
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[]   $createdCategories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Gallery[]    $createdGalleries
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[]      $createdOrders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tag[]        $createdTags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Text[]       $createdTexts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Article[]    $publishedArticles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Article[]    $updatedArticles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Background[] $updatedBackgrounds
 * @property-read \App\User|null                                             $updatedByUser
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[]   $updatedCategories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Gallery[]    $updatedGalleries
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[]      $updatedOrders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tag[]        $updatedTags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Text[]       $updatedTexts
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePermissions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePublicName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedByUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\User withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $avatar
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAvatar($value)
 * @property-read mixed $name
 */
class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
	use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;

	protected $table = 'users';
	protected $guarded = [];
	protected $dates = [
		'deleted_at',
	];
	protected $hidden = [
		'password', 'remember_token',
	];

	/*
	 * Relationships
	 */
	public function createdByUser()
	{
		return $this->belongsTo(User::class);
	}

	public function updatedByUser()
	{
		return $this->belongsTo(User::class);
	}

	/*
	 * Inverse relationships
	 */
	public function publishedArticles()
	{
		return $this->hasMany(Article::class, 'published_by_user_id');
	}

	public function createdArticles()
	{
		return $this->hasMany(Article::class, 'created_by_user_id');
	}

	public function updatedArticles()
	{
		return $this->hasMany(Article::class, 'updated_by_user_id');
	}

	public function createdBackgrounds()
	{
		return $this->hasMany(Background::class, 'created_by_user_id');
	}

	public function updatedBackgrounds()
	{
		return $this->hasMany(Background::class, 'updated_by_user_id');
	}

	public function createdCategories()
	{
		return $this->hasMany(Category::class, 'created_by_user_id');
	}

	public function updatedCategories()
	{
		return $this->hasMany(Category::class, 'updated_by_user_id');
	}

	public function createdGalleries()
	{
		return $this->hasMany(Gallery::class, 'created_by_user_id');
	}

	public function updatedGalleries()
	{
		return $this->hasMany(Gallery::class, 'updated_by_user_id');
	}

	public function createdOrders()
	{
		return $this->hasMany(Order::class, 'created_by_user_id');
	}

	public function updatedOrders()
	{
		return $this->hasMany(Order::class, 'updated_by_user_id');
	}

	public function createdTags()
	{
		return $this->hasMany(Tag::class, 'created_by_user_id');
	}

	public function updatedTags()
	{
		return $this->hasMany(Tag::class, 'updated_by_user_id');
	}

	public function createdTexts()
	{
		return $this->hasMany(Text::class, 'created_by_user_id');
	}

	public function updatedTexts()
	{
		return $this->hasMany(Text::class, 'updated_by_user_id');
	}

	public function audit()
	{
		return $this->hasMany(Audit::class);
	}

	/*
	 * Other methods
	 */
	public function getPermissionsAttribute($permissions)
	{
		if ($permissions) {
			$permissions = json_decode($permissions);
			return collect(($permissions));
		}
		return collect([]);
	}

	public function hasPermission(string $permission)
	{
		foreach ($this->permissions as $value) {
			if ($value == $permission) {
				return true;
			}
		}
		return false;
	}

	public function getNameAttribute()
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	public function hasAvatar()
	{
		if ($this->avatar && Storage::has('images/avatars/' . $this->id . '.png')) {
			return true;
		}
		return false;
	}
}
