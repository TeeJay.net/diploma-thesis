<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Category
 *
 * @property int                                                          $id
 * @property string                                                       $name
 * @property string                                                       $alias
 * @property string                                                       $description
 * @property int                                                          $created_by_user_id
 * @property int                                                          $updated_by_user_id
 * @property \Carbon\Carbon|null                                          $created_at
 * @property \Carbon\Carbon|null                                          $updated_at
 * @property \Carbon\Carbon|null                                          $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Article[] $articles
 * @property-read \App\User                                               $createdByUser
 * @property-read \App\User                                               $updatedByUser
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Category onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUpdatedByUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Category withoutTrashed()
 * @mixin \Eloquent
 */
class Category extends BaseModel
{
	use SoftDeletes;

	protected $table = 'categories';
	protected $guarded = [];
	protected $dates = [
		'deleted_at',
	];

	/*
	 * Relationships
	 */
	public function createdByUser()
	{
		return $this->belongsTo(User::class);
	}

	public function updatedByUser()
	{
		return $this->belongsTo(User::class);
	}

	/*
	 * Inverse relationships
	 */
	public function articles()
	{
		return $this->hasMany(Article::class);
	}
}

