<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

/**
 * App\Gallery
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property string|null $password
 * @property string|null $description
 * @property int|null $order_id
 * @property int $created_by_user_id
 * @property int $updated_by_user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\User $createdByUser
 * @property-read mixed $path_to_photos
 * @property-read mixed $photos
 * @property-read \App\Order $order
 * @property-read \App\User $updatedByUser
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gallery whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gallery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gallery whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gallery whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gallery whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gallery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gallery whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gallery whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gallery wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gallery whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gallery whereUpdatedByUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Gallery withoutTrashed()
 * @mixin \Eloquent
 */
class Gallery extends BaseModel
{
	use SoftDeletes;

	protected $table = 'galleries';
	protected $guarded = [];
	protected $dates = [
		'deleted_at',
	];
	public static $storagePath = 'images/galleries';

	/*
	 * Relationships
	 */
	public function order()
	{
		return $this->hasOne(Order::class);
	}

	public function createdByUser()
	{
		return $this->belongsTo(User::class);
	}

	public function updatedByUser()
	{
		return $this->belongsTo(User::class);
	}

	public function getPathToPhotosAttribute()
	{
		return self::$storagePath . '/' . $this->id;
	}

	public function getPhotosAttribute()
	{
		return Storage::files($this->path_to_photos);
	}
}
