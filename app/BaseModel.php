<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * Class BaseModel
 *
 * @package App
 * @mixin \Eloquent
 */
class BaseModel extends Model
{
	/**
	 * @var bool Whether all saved changes should be logged in the "audit" table.
	 */
	protected static $auditing = true;
	protected static $userActivityTracking = true;

	/**
	 * Auditing of models excepts those with static $auditing set to false
	 */
	protected static function boot()
	{
		parent::boot();

		$class = get_called_class();
		Audit::audit($class);

		if ($class::$userActivityTracking) {
			$user = Auth::user();
			static::creating(function(BaseModel $object) use ($user) {
				$object->created_by_user_id = $user ? $user->id : 1;
				$object->updated_by_user_id = $user ? $user->id : 1;
			});

			static::updating(function(BaseModel $object) use ($user) {
				$object->updated_by_user_id = $user ? $user->id : 1;
			});

			static::deleting(function(BaseModel $object) use ($user) {
				$object->updated_by_user_id = $user ? $user->id : 1;
			});
		}
	}

	public function getAttributeWithDotSyntax(string $pathToAttribute)
	{
		$relations = explode('.', $pathToAttribute);
		$attribute = array_pop($relations); // last element is desired attribute

		if (!empty($relations)) {
			$model = $this;

			foreach ($relations as $relation) {

				if (!$model->relationLoaded($relation)) {
					$model->load($relation);
				}

				$model = $model->$relation;
			}

			return $model->$attribute;
		}

		return $this->$attribute;
	}

	/**
	 * Adds order by $tableName.id match if $string contains an integer only
	 *
	 * @param string  $string
	 * @param string  $tableName
	 * @param Builder $query
	 *
	 * @return Builder
	 */
	public function orderByIdIfNumber(Builder $query, string $string, string $tableName) {
		if (preg_match('/^[0-9]+$/', $string)) {
			return $query->orderByRaw($tableName . '.id = ? DESC, ' . $tableName . '.id LIKE ? DESC', [$string, $string . '%']);
		}
		return $query;
	}

	/**
	 * Adds where condition to search by created_at date of the model in $query by dd.mm.(YYYY) date string
	 *
	 * @param Builder $query
	 * @param string  $string
	 *
	 * @return Builder|false
	 */
	public function quickSearchByCreatedAtDate(Builder $query, string $string, string $tableName, array $additionalDateFieldsToSearchIn = [])
	{
		if (preg_match('/^[0-9]{1,2}(\.)?([0-9]{1,2})?(\.)?([0-9]{1,4})?$/', $string)) {
			$query = $query->where($tableName . '.created_at', 'LIKE', $this->getDateTimestampFromString($string) . '%');
			foreach ($additionalDateFieldsToSearchIn as $dateField) {
				$query = $query->orWhere($tableName . '.' . $dateField, 'LIKE', $this->getDateTimestampFromString($string) . '%');
			}
			return $query;
		}
		return false;
	}

	/**
	 * Takes an input date of certain format (d, d., dd.mm, dd.mm., dd.mm.Y, ..., dd.mm.YYYY)
	 * and formats it into a YYYY-mm-dd string that corresponds with DB timestamp format
	 *
	 * @param string $string
	 *
	 * @return string
	 */
	private function getDateTimestampFromString(string $string)
	{
		$explodedString = explode('.', $string);

		$day = $explodedString[0];
		$month = isset($explodedString[1]) && $explodedString[1] ? $explodedString[1] : '__';
		$year = isset($explodedString[2]) && $explodedString[2] ? $explodedString[2] : '____';

		$day = str_pad($day, 2, '0', STR_PAD_LEFT);
		$month = str_pad($month, 2, '0', STR_PAD_LEFT);
		$year = str_pad($year, 4, '_');

		return $year . '-' . $month . '-' . $day;
	}

	public static function getPathTo(string $folderName)
	{
		$filePath = storage_path('app/' . $folderName) . '/';
		if (!Storage::has($folderName)) {
			Storage::makeDirectory($folderName);
		}
		return $filePath;
	}
}
