<?php

namespace App\Http\Controllers\FE;

use App\Article;
use App\Background;
use App\Category;
use App\Client;
use App\Gallery;
use App\Http\Controllers\Controller;
use App\Order;
use App\Tag;
use App\Text;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
	protected $menu;

	public function __construct()
	{
		$this->menu = Text::where('id', '>', 99)->get()->keyBy('alias');
		view()->share([
			'menu' => $this->menu,
		]);
	}

	/*
	 * Texts part
	 */
	public function home()
	{
		return view('FE.homepage', [
			'texts'     => Text::whereIn('id', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])->get()->keyBy('alias'),
			'clients'   => Client::orderBy('created_at', 'DESC')->get(),
			'articles'  => Article::orderBy('created_at', 'DESC')->limit(3)->get(),
			'galleries' => Gallery::orderBy('created_at', 'DESC')->limit(3)->get(),
		]);
	}

	public function pricing()
	{
		$text = Text::findOrFail(14)->text;
		$text = str_replace('{link-objednat}', url('objednat-cvakmat'), $text);

		return view('FE.text', [
			'title'   => menu(2, $this->menu),
			'content' => $text,
		]);
	}

	public function whatIsIt()
	{
		return view('FE.text-no-container', [
			'title'   => menu(8, $this->menu),
			'content' => Text::findOrFail(18)->text,
		]);
	}

	public function contact()
	{
		return view('FE.text', [
			'title'   => menu(7, $this->menu),
			'content' => Text::findOrFail(13)->text,
		]);
	}

	public function backgrounds()
	{
		return view('FE.backgrounds', [
			'content'     => Text::findOrFail(16)->text,
			'backgrounds' => Background::all(),
		]);
	}


	/*
	 * Gallery part
	 */
	public function galleries()
	{
		return view('FE.galleries.index', [
			'galleries' => Gallery::orderBy('created_at', 'DESC')->paginate(30),
		]);
	}

	public function gallery(Request $request, string $alias)
	{
		if (!$alias) {
			abort(404);
		}
		$gallery = Gallery::where('alias', $alias)->first();
		if (!$gallery) {
			abort(404);
		}

		if ($gallery->password && !$request->session()->has('access-to-gallery-' . $gallery->id)) {
			return view('FE.galleries.gallery-password-form', [
				'content' => Text::findOrFail(17)->text,
				'gallery' => $gallery,
			]);
		}

		return view('FE.galleries.gallery', [
			'gallery' => $gallery,
		]);
	}

	public function galleryLogin(Request $request, Gallery $gallery)
	{
		if (!$request->has('password') || !Hash::check($request->password, $gallery->password)) {
			$request->session()->flash('message', trans('auth.wrong_password'));
			return redirect('galerie/' . $gallery->alias);
		}

		$request->session()->put('access-to-gallery-' . $gallery->id, 1);

		return redirect('galerie/' . $gallery->alias);
	}

	/*
	 * Order part
	 */
	public function order()
	{
		return view('FE.order', [
			'content' => Text::findOrFail(15)->text,
		]);
	}

	public function storeOrder(Request $request)
	{
		$this->validate($request, [
			// Contact info
			'first_name'           => 'required|max:191',
			'last_name'            => 'required|max:191',
			'street'               => 'required|max:191',
			'house_number'         => 'required|integer',
			'city'                 => 'required|max:191',
			'postcode'             => 'required|digits:5',
			'email'                => 'required|email|max:191',
			'phone'                => 'required|max:191',
			// Event info
			'invoice_event'        => 'required|max:191',
			'background_id'        => 'required|integer',
			'print'                => 'required|max:191',
			'rental_from'          => 'required|date_format:d.m.Y H:i',
			'rental_to'            => 'required|date_format:d.m.Y H:i|after:rental_from',
			// Company additional info
			'company'              => 'required_if:is_company,on|nullable|max:191',
			'ic'                   => 'required_if:is_company,on|nullable|digits:8',
			'dic'                  => 'nullable|max:191',
			// Invoice data
			'invoice_first_name'   => 'required_if:invoice_data_are_same,off|nullable|max:191',
			'invoice_last_name'    => 'required_if:invoice_data_are_same,off|nullable|max:191',
			'invoice_street'       => 'required_if:invoice_data_are_same,off|nullable|max:191',
			'invoice_house_number' => 'required_if:invoice_data_are_same,off|nullable|integer',
			'invoice_city'         => 'required_if:invoice_data_are_same,off|nullable|max:191',
			'invoice_postcode'     => 'required_if:invoice_data_are_same,off|nullable|digits:5|max:191',
			'invoice_company'      => 'required_if:invoice_data_are_same,off|required_if:is_company_invoice_data,on|nullable|max:191',
			'invoice_ic'           => 'required_if:invoice_data_are_same,off|required_if:is_company_invoice_data,on|nullable|digits:8',
			'invoice_dic'          => 'nullable|max:191',
		], [
			// Contact info
			'first_name.required'              => 'Jméno je povinné',
			'first_name.max'                   => 'Jméno nesmí mít více než 191 znaků',
			'last_name.required'               => 'Příjmení je povinné',
			'last_name.max'                    => 'Příjmení nesmí mít více než 191 znaků',
			'street.required'                  => 'Ulice je povinná',
			'street.max'                       => 'Ulice nesmí mít více než 191 znaků',
			'house_number.required'            => 'Číslo popisné je povinné',
			'house_number.integer'             => 'Číslo popisné musí být celé číslo',
			'city.required'                    => 'Město je povinné',
			'city.max'                         => 'Město nesmí mít více než 191 znaků',
			'postcode.required'                => 'PSC je povinné',
			'postcode.digits'                  => 'PSČ musí mít 5 číslic',
			'email.required'                   => 'Email je povinný',
			'email.email'                      => 'Email musí mít formát emailu',
			'email.max'                        => 'Email nesmí mít více než 191 znaků',
			'phone.required'                   => 'Telefon je povinný',
			'phone.max'                        => 'Telefon nesmí mít více než 191 znaků',
			// Event info
			'invoice_event.required'           => 'Název akce je povinný',
			'invoice_event.max'                => 'Název akcenesmí mít více než 191 znaků',
			'background_id.required'           => 'Pozadí musí být vybráno',
			'background_id.integer'            => 'Pozadí musí být vybráno ze seznamu',
			'print.required'                   => 'Možnost tisku musí být vybrána',
			'print.max'                        => 'Možnost tisku nesmí mít více než 191 znaků',
			'rental_from.required'             => 'Datum a čas začátku pronájmu musí být vyplněn',
			'rental_from.date_format'          => 'Datum a čas začátku pronájmu musí mít formát DD.MM.RRRR HH:MM',
			'rental_to.required'               => 'Datum a čas konce pronájmu musí být vyplněn',
			'rental_to.date_format'            => 'Datum a čas konce pronájmu musí mít formát DD.MM.RRRR HH:MM',
			'rental_to.date'                   => 'Datum a čas konce pronájmu musí být datum',
			'rental_to.after'                  => 'Datum a čas konce pronájmu musí být po jeho začátku',
			// Company additional info
			'company.required_if'              => 'V případě firmy je její název povinný',
			'company.max'                      => 'Název firmy nesmí mít více než 191 znaků',
			'ic.required_if'                   => 'V případě firmy je její IČ povinné',
			'ic.digits'                        => 'IČ musí mít 8 číslic. Pokud jich má méně, doplňte před něj nuly',
			'dic.max'                          => 'DIČ nesmí mít více než 191 znaků',
			// Invoice data
			'invoice_first_name.required_if'   => 'Pokud se fakturační údaje liší, fakturační jméno je povinné',
			'invoice_first_name.max'           => 'Fakturační jméno nesmí mít více než 191 znaků',
			'invoice_last_name.required_if'    => 'Pokud se fakturační údaje liší, fakturační příjmení je povinné',
			'invoice_last_name.max'            => 'Fakturační příjmení nesmí mít více než 191 znaků',
			'invoice_street.required_if'       => 'Pokud se fakturační údaje liší, fakturační ulice je povinná',
			'invoice_street.max'               => 'Fakturační ulice nesmí mít více než 191 znaků',
			'invoice_house_number.required_if' => 'Pokud se fakturační údaje liší, fakturační číslo popisné je povinné',
			'invoice_house_number.integer'     => 'Fakturační číslo popisné musí být celé číslo',
			'invoice_city.required_if'         => 'Pokud se fakturační údaje liší, fakturační město je povinné',
			'invoice_city.max'                 => 'Fakturační město nesmí mít více než 191 znaků',
			'invoice_postcode.required_if'     => 'Pokud se fakturační údaje liší, fakturační PSČ je povinné',
			'invoice_postcode.digits'          => 'Fakturační PSČ musí mít 5 číslic',
			'invoice_postcode.max'             => 'Fakturační PSČ nesmí mít více než 191 znaků',
			'invoice_company.required_if'      => 'Pokud se fakturační údaje liší a fakturuje se na firmu, název firmy je povinný',
			'invoice_company.max'              => 'Název firmy nesmí mít více než 191 znaků',
			'invoice_ic.required_if'           => 'Pokud se fakturační údaje liší a fakturuje se na firmu, IČ firmy je povinné',
			'invoice_ic.digits'                => 'IČ fakturační firmy musí mít 8 číslic. Pokud jich má méně, doplňte před něj nuly',
			'invoice_dic.max'                   => 'DIČ fakturační firmy nesmí mít více než 191 znaků',
		]);

		$order = new Order([
			// Contact info
			'first_name'           => $request->first_name,
			'last_name'            => $request->last_name,
			'street'               => $request->street,
			'house_number'         => $request->house_number,
			'city'                 => $request->city,
			'postcode'             => $request->postcode,
			'email'                => $request->email,
			'phone'                => $request->phone,
			// Event info
			'invoice_event'        => $request->invoice_event,
			'background_id'        => $request->background_id,
			'print'                => $request->print,
			'rental_from'          => Carbon::createFromFormat('d.m.Y H:i', $request->rental_from),
			'rental_to'            => Carbon::createFromFormat('d.m.Y H:i', $request->rental_to),
			'note'                 => $request->note,
			// Company additional info
			'company'              => $request->invoice_company ?: $request->company,
			'invoice_ic'           => $request->invoice_ic ?: $request->ic ,
			'invoice_dic'          => $request->invoice_dic ?: $request->dic,
			// Invoice data
			'confirmed'            => 0,
			'invoice_first_name'   => $request->invoice_first_name ?: $request->first_name,
			'invoice_last_name'    => $request->invoice_last_name ?: $request->last_name,
			'invoice_street'       => $request->invoice_street ?: $request->street,
			'invoice_house_number' => $request->invoice_house_number ?: $request->house_number,
			'invoice_city'         => $request->invoice_city ?: $request->city,
			'invoice_postcode'     => $request->invoice_postcode ?: $request->postcode,
		]);
		$order->save();

		$order->sendEmailOrderCreated();

		$request->session()->flash('message', 'Vaše objednávka byla úspěšně odeslána. Potvrzení máte na emailu. Děkujeme!');
		return redirect('/objednat-cvakmat');
	}


	/*
	 * Blog part
	 */
	public function blog()
	{
		return view('FE.blog.index', [
			'articles' => Article::orderBy('created_at', 'DESC')->paginate(10),
		]);
	}

	public function category(string $alias)
	{
		if (!$alias) {
			abort(404);
		}
		$category = Category::where('alias', $alias)->first();
		if (!$category) {
			abort(404);
		}

		return view('FE.blog.category', [
			'category' => $category,
			'articles' => $category->articles()->orderBy('created_at', 'DESC')->paginate(10),
		]);
	}

	public function tag(string $alias)
	{
		if (!$alias) {
			abort(404);
		}
		$tag = Tag::where('alias', $alias)->first();
		if (!$tag) {
			abort(404);
		}
		return view('FE.blog.tag', [
			'tag'      => $tag,
			'articles' => $tag->articles()->orderBy('created_at', 'DESC')->paginate(10),
		]);
	}

	public function article(string $alias)
	{
		if (!$alias) {
			abort(404);
		}

		$article = Article::where('alias', $alias)->first();
		if (!$article) {
			abort(404);
		}

		return view('FE.blog.article', [
			'article'        => $article,
			'latestArticles' => Article::orderBy('created_at', 'DESC')->limit(5)->get(),
			'tags'           => Tag::whereHas('articles')->get(),
		]);
	}
}
