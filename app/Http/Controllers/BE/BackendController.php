<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BackendController extends Controller
{
	public function __construct(Request $request)
	{
		$this->middleware('auth');
	}
}
