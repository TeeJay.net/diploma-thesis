<?php

namespace App\Http\Controllers\BE;

use App\DataTables\Columns\Column;
use App\DataTables\DataTable;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UsersController extends BackendController
{
	public function index(Request $request)
	{
		$this->checkAccess($request->user(), 'users');

		$dataTable = new DataTable(User::query(), $request);
		$dataTable->setColumns(
			(new Column('id'))
				->orderBy('id'),
			(new Column('first_name'))
				->orderBy('first_name'),
			(new Column('last_name'))
				->orderBy('last_name'),
			(new Column('avatar'))
				->formatView(view('BE.components.columns.avatar')),
			(new Column('public_name'))
				->orderBy('public_name'),
			(new Column('email'))
				->orderBy('email'),
			(new Column('permissions'))
				->formatView(view('BE.components.columns.permissions')),
			(new Column('actions'))
				->class('text-right')
				->formatView(view('BE.components.columns.actions-user'))
		);
		$dataTable->setColumnWidths([
			'id'      => 3,
			'actions' => 20,
		]);
		$dataTable->setDefaultOrderBy('id', 'desc');

		return view('BE.users.index', [
			'dataTable' => $dataTable,
		]);
	}

	public function create(Request $request)
	{
		$this->checkAccess($request->user(), 'users');

		return view('BE.users.add', [
			'user'        => null,
			'permissions' => getPermissions(),
		]);
	}

	public function store(Request $request)
	{
		$this->checkAccess($request->user(), 'users');

		$this->validateForm($request);

		$user = new User($request->all(['first_name', 'last_name', 'email']));
		$user->fill([
			'public_name' => $request->public_name ?: $user->first_name . ' ' . $user->last_name,
			'password'    => bcrypt($request->input('password')),
			'permissions' => json_encode($request->input('permissions')),
		]);
		$user->save();

		$request->session()->flash('message', trans('models.users.messages.created'));
		return redirect('admin/users');
	}

	public function edit(Request $request, User $user)
	{
		$this->checkAccess($request->user(), 'users');

		return view('BE.users.edit', [
			'user'        => $user,
			'permissions' => getPermissions(),
		]);
	}

	public function update(Request $request, User $user)
	{
		$this->checkAccess($request->user(), 'users');

		$this->validateForm($request);

		$user->fill([
			'first_name'  => $request->first_name,
			'last_name'   => $request->last_name,
			'public_name' => $request->public_name ?: $user->first_name . ' ' . $user->last_name,
			'email'       => $request->email,
		]);
		if ($user->id != $request->user()->id) { // cannot change access rights for himself
			$user->permissions = json_encode($request->input('permissions'));
		}
		$user->save();

		$request->session()->flash('message', trans('models.users.messages.edited'));
		return redirect('admin/users');
	}

	public function destroy(Request $request, User $user)
	{
		$this->checkAccess($request->user(), 'users');

		$user->delete();

		$request->session()->flash('message', trans('models.users.messages.deleted'));
		return $user;
	}

	private function validateForm(Request $request)
	{
		$this->validate($request, [
			'first_name'  => 'required|max:191',
			'last_name'   => 'required|max:191',
			'public_name' => 'max:191',
			'email'       => 'required|email|max:191',
		]);
	}

	public function showChangePassword(Request $request, User $user)
	{
		$this->checkAccess($request->user(), 'users');

		return view('BE.users.change-password', [
			'user' => $user,
		]);
	}

	public function storeChangePassword(Request $request, User $user)
	{
		$this->checkAccess($request->user(), 'users');

		$this->validate($request, [
			'old_password'       => 'required|max:191',
			'new_password'       => 'required|max:191',
			'new_password_again' => 'required|max:191|same:new_password',
		]);

		if (!Hash::check($request->old_password, $user->password)) {
			$request->session()->flash('message', trans('models.users.messages.incorrect_password'));
			$request->session()->flash('message_type', 'error');
			return redirect('admin/users/' . $user->id . '/change-password');
		}

		$user->fill([
			'password' => bcrypt($request->new_password),
		]);
		$user->save();

		$request->session()->flash('message', trans('models.users.messages.password_changed'));
		return redirect('admin/users');
	}

	public function showChangeAvatar(Request $request, User $user)
	{
		$this->checkAccess($request->user(), 'users');

		return view('BE.users.change-avatar', [
			'user' => $user,
		]);
	}

	public function storeChangeAvatar(Request $request, User $user)
	{
		$this->checkAccess($request->user(), 'users');

		$this->validate($request, [
			'avatar-upload' => 'required|image',
		]);

		$storagePath = 'images/avatars/';
		$cachePath = 'images/.cache/avatars/';
		$savePath = storage_path('app/' . $storagePath);
		$fileName = $user->id . '.png';
		if (!Storage::has($storagePath)) {
			Storage::makeDirectory($storagePath);
		} else {
			if (Storage::has($storagePath . $fileName)) {
				Storage::delete($storagePath . $fileName);
			}
			if (Storage::has($cachePath . $fileName . '/')) {
				Storage::deleteDirectory($cachePath . $fileName . '/');
			}
		}

		$image = Image::make($request->file('avatar-upload'));
		$fitTo = $image->getHeight();
		if ($image->getWidth() < $image->getHeight()) {
			$fitTo = $image->getWidth();
		}
		$image->fit($fitTo, $fitTo);
		$image->save($savePath . $fileName); // Converts to format defined by the file name automatically

		// Make sure the user's udpated_at timestamp is updated
		$user->avatar = null;
		$user->save();
		$user->avatar = $storagePath . $fileName;
		$user->save();

		$request->session()->flash('message', trans('generic.messages.saved'));
		return redirect('admin/users');
	}
}
