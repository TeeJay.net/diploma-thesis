<?php

namespace App\Http\Controllers\BE;

use Illuminate\Http\Request;

class DashboardController extends BackendController
{
	public function index(Request $request)
	{
		return view('BE.dashboard.index');
	}
}
