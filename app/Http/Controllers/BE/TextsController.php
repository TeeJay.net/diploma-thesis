<?php

namespace App\Http\Controllers\BE;

use App\DataTables\Columns\Column;
use App\DataTables\DataTable;
use App\Text;
use Illuminate\Http\Request;

class TextsController extends BackendController
{
	public function index(Request $request)
	{
		$this->checkAccess($request->user(), 'texts');

		$dataTable = new DataTable(Text::query(), $request);
		$dataTable->setColumns(
			(new Column('id'))
				->orderBy('id'),
			(new Column('name'))
				->orderBy('name'),
			(new Column('text'))
				->orderBy('text')
				->format(function($value) {
					return str_limit(strip_tags($value), 220);
				}),
			(new Column('actions'))
				->class('text-right')
				->formatView(view('BE.components.columns.actions-edit-only'))
		);
		$dataTable->setColumnWidths([
			'id'    => 3,
			'intro' => 30,
		]);

		return view('BE.texts.index', [
			'dataTable' => $dataTable,
		]);
	}

	public function edit(Request $request, Text $text)
	{
		$this->checkAccess($request->user(), 'texts');

		return view('BE.texts.edit', [
			'text' => $text,
		]);
	}

	public function update(Request $request, Text $text)
	{
		$this->checkAccess($request->user(), 'texts');

		$this->validate($request, [
			'name' => 'required|max:191',
			'text' => 'required',
		]);
		$text->fill([
			'name' => $request->name,
			'text' => $request->text,
		]);
		$text->save();

		$request->session()->flash('message', trans('models.texts.messages.edited'));
		return redirect('admin/texts');
	}
}
