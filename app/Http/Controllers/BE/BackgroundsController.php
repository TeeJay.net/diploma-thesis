<?php

namespace App\Http\Controllers\BE;

use App\Background;
use App\DataTables\Columns\Column;
use App\DataTables\DataTable;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class BackgroundsController extends BackendController
{
	public function index(Request $request)
	{
		$this->checkAccess($request->user(), 'backgrounds');

		$dataTable = new DataTable(Background::query(), $request);
		$dataTable->setColumns(
			(new Column('id'))
				->orderBy('id'),
			(new Column('name'))
				->orderBy('name'),
			(new Column('image'))
				->formatView(view('BE.components.columns.image')),
			(new Column('description'))
				->orderBy('description')
				->format(function($value) {
					return str_limit(strip_tags($value), 220);
				}),
			(new Column('actions'))
				->class('text-right')
				->formatView(view('BE.components.columns.actions'))
		);
		$dataTable->setColumnWidths([
			'id'      => 3,
			'actions' => 20,
		]);
		$dataTable->setDefaultOrderBy('id', 'desc');

		return view('BE.backgrounds.index', [
			'dataTable' => $dataTable,
		]);
	}

	public function create(Request $request)
	{
		$this->checkAccess($request->user(), 'backgrounds');

		return view('BE.backgrounds.add', [
			'background' => null,
		]);
	}

	public function store(Request $request)
	{
		$this->checkAccess($request->user(), 'backgrounds');

		$this->validateForm($request);
		$this->saveInstance(new Background(), $request);

		$request->session()->flash('message', trans('models.backgrounds.messages.created'));
		return redirect('admin/backgrounds');
	}

	public function edit(Request $request, Background $background)
	{
		$this->checkAccess($request->user(), 'backgrounds');

		return view('BE.backgrounds.edit', [
			'background' => $background,
		]);
	}

	public function update(Request $request, Background $background)
	{
		$this->checkAccess($request->user(), 'backgrounds');

		$this->validateForm($request);
		$this->saveInstance($background, $request);

		$request->session()->flash('message', trans('models.backgrounds.messages.edited'));
		return redirect('admin/backgrounds');
	}

	public function destroy(Request $request, Background $background)
	{
		$this->checkAccess($request->user(), 'backgrounds');

		$background->delete();

		$request->session()->flash('message', trans('models.backgrounds.messages.deleted'));
		return $background;
	}

	private function validateForm(Request $request)
	{
		$this->validate($request, [
			'name'  => 'required|max:191',
			'image' => 'image',
		]);
	}

	private function saveInstance(Background $background, Request $request)
	{
		$background->fill([
			'name'        => $request->name,
			'description' => $request->description ?: null,
		]);
		$background->save();
		if ($request->image) {
			$this->addImageToBackground($background, $request->image, 'image');
		}

		return $background;
	}

	private function addImageToBackground(Background $background, UploadedFile $file, string $columnName)
	{
		$fileNameParts = explode('.', $file->getClientOriginalName());
		$path = $file->storeAs('images/backgrounds/' . $background->id, str_slug($fileNameParts[0]) . '.' . $fileNameParts[1]);

		$background->$columnName = $path;
		$background->save();
	}
}
