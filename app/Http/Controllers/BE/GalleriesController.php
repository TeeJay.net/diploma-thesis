<?php

namespace App\Http\Controllers\BE;

use App\BaseModel;
use App\DataTables\Columns\Column;
use App\DataTables\DataTable;
use App\Gallery;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GalleriesController extends BackendController
{
	public function index(Request $request)
	{
		$this->checkAccess($request->user(), 'galleries');

		$dataTable = new DataTable(Gallery::query(), $request);
		$dataTable->setColumns(
			(new Column('id'))
				->orderBy('id'),
			(new Column('name'))
				->orderBy('name'),
			(new Column('description'))
				->orderBy('description')
				->format(function($value) {
					return str_limit(strip_tags($value), 220);
				}),
			(new Column('password'))
				->orderBy('password')
				->formatView(view('BE.components.columns.checked')),
			(new Column('actions'))
				->class('text-right')
				->formatView(view('BE.components.columns.actions-gallery'))
		);
		$dataTable->setColumnWidths([
			'id'      => 3,
			'actions' => 20,
		]);
		$dataTable->setDefaultOrderBy('id', 'desc');

		return view('BE.galleries.index', [
			'dataTable' => $dataTable,
		]);
	}

	public function create(Request $request)
	{
		$this->checkAccess($request->user(), 'galleries');

		return view('BE.galleries.add', [
			'gallery' => null,
		]);
	}

	public function store(Request $request)
	{
		$this->checkAccess($request->user(), 'galleries');

		$this->validateForm($request);
		$this->saveInstance(new Gallery(), $request);

		$request->session()->flash('message', trans('models.galleries.messages.created'));
		return redirect('admin/galleries');
	}

	public function edit(Request $request, Gallery $gallery)
	{
		$this->checkAccess($request->user(), 'galleries');

		return view('BE.galleries.edit', [
			'gallery' => $gallery,
		]);
	}

	public function update(Request $request, Gallery $gallery)
	{
		$this->checkAccess($request->user(), 'galleries');

		$this->validateForm($request);
		$this->saveInstance($gallery, $request);

		$request->session()->flash('message', trans('models.galleries.messages.edited'));
		return redirect('admin/galleries');
	}

	public function destroy(Request $request, Gallery $gallery)
	{
		$this->checkAccess($request->user(), 'galleries');

		$gallery->delete();

		$request->session()->flash('message', trans('models.galleries.messages.deleted'));
		return $gallery;
	}

	private function validateForm(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:191',
		]);
	}

	private function saveInstance(Gallery $gallery, Request $request)
	{
		$gallery->fill([
			'name'        => $request->name,
			'alias'       => $request->alias ?: str_slug($request->name),
			'password'    => $request->password ? bcrypt($request->password) : $gallery->password,
			'order_id'    => $request->order_id,
			'description' => $request->description ?: null,
		]);
		$gallery->save();

		return $gallery;
	}

	public function editPhotos(Request $request, Gallery $gallery)
	{
		$this->checkAccess($request->user(), 'galleries');

		return view('BE.galleries.edit-photos', [
			'gallery' => $gallery,
		]);
	}

	public function uploadPhoto(Request $request, Gallery $gallery)
	{
		$galleryFolder = BaseModel::getPathTo($gallery->path_to_photos); // creates the folder if doesn't exists

		// File in stream format
		$stream = fopen('php://input', 'r');

		// Validation
		$filePath = $galleryFolder . '/' . strtolower($request->name);
		$position = strrpos($filePath, '.');

		if ($position === false) {
			return JsonResponse::HTTP_UNPROCESSABLE_ENTITY; // No extension, not an image then
		} else {
			$fileName = substr($filePath, 0, $position);
			$fileExtension = substr($filePath, $position + 1);
			if (!in_array($fileExtension, ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'webp'])) {
				return JsonResponse::HTTP_UNPROCESSABLE_ENTITY; // Not allowed image format
			}
		}

		// Append a random string if the file already exists
		if (file_exists($filePath)) {
			$randomString = '-' . str_random(str_random(5));
			$filePath = $fileName . $randomString . '.' . $fileExtension;
		}

		// Copy the image from the stream to the server
		$fp = fopen($filePath, 'w');
		stream_copy_to_stream($stream, $fp);
		fclose($fp);

		return JsonResponse::HTTP_CREATED;
	}

	public function deletePhoto(Request $request, Gallery $gallery)
	{
		Storage::delete($gallery->path_to_photos . '/' . $request->name);
		return ['path' => $gallery->path_to_photos . '/' . $request->name];
	}
}
