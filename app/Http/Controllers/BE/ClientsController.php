<?php

namespace App\Http\Controllers\BE;

use App\Client;
use App\DataTables\Columns\Column;
use App\DataTables\DataTable;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ClientsController extends BackendController
{
	public function index(Request $request)
	{
		$this->checkAccess($request->user(), 'clients');

		$dataTable = new DataTable(Client::query(), $request);
		$dataTable->setColumns(
			(new Column('id'))
				->orderBy('id'),
			(new Column('name'))
				->orderBy('name'),
			(new Column('image'))
				->formatView(view('BE.components.columns.image')),
			(new Column('actions'))
				->class('text-right')
				->formatView(view('BE.components.columns.actions'))
		);
		$dataTable->setColumnWidths([
			'id'      => 3,
			'actions' => 20,
		]);
		$dataTable->setDefaultOrderBy('id', 'desc');

		return view('BE.clients.index', [
			'dataTable' => $dataTable,
		]);
	}

	public function create(Request $request)
	{
		$this->checkAccess($request->user(), 'clients');

		return view('BE.clients.add', [
			'client' => null,
		]);
	}

	public function store(Request $request)
	{
		$this->checkAccess($request->user(), 'clients');

		$this->validateForm($request);
		$this->saveInstance(new Client(), $request);

		$request->session()->flash('message', trans('models.clients.messages.created'));
		return redirect('admin/clients');
	}

	public function edit(Request $request, Client $client)
	{
		$this->checkAccess($request->user(), 'clients');

		return view('BE.clients.edit', [
			'client' => $client,
		]);
	}

	public function update(Request $request, Client $client)
	{
		$this->checkAccess($request->user(), 'clients');

		$this->validateForm($request);
		$this->saveInstance($client, $request);

		$request->session()->flash('message', trans('models.clients.messages.edited'));
		return redirect('admin/clients');
	}

	public function destroy(Request $request, Client $client)
	{
		$this->checkAccess($request->user(), 'clients');

		$client->delete();

		$request->session()->flash('message', trans('models.clients.messages.deleted'));
		return $client;
	}

	private function validateForm(Request $request)
	{
		$this->validate($request, [
			'name'  => 'required|max:191',
			'image' => 'image',
		]);
	}

	private function saveInstance(Client $client, Request $request)
	{
		$client->fill([
			'name'        => $request->name,
		]);
		$client->save();
		if ($request->image) {
			$this->addImageToClient($client, $request->image, 'image');
		}

		return $client;
	}

	private function addImageToClient(Client $client, UploadedFile $file, string $columnName)
	{
		$fileNameParts = explode('.', $file->getClientOriginalName());
		$path = $file->storeAs('images/clients/' . $client->id, str_slug($fileNameParts[0]) . '.' . $fileNameParts[1]);

		$client->$columnName = $path;
		$client->save();
	}
}
