<?php

namespace App\Http\Controllers\BE;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class SettingsController extends BackendController
{
	public function edit(Request $request)
	{
		$this->checkAccess($request->user(), 'settings');

		return view('BE.settings.edit', [
			'settings' => DB::table('settings')->orderBy('order')->get(),
		]);
	}

	public function update(Request $request)
	{
		$this->checkAccess($request->user(), 'settings');

		$oldSettings = DB::table('settings')->get();

		// Validate
		$validationRules = [];
		foreach ($oldSettings as $setting) {
			$validationRules[$setting->key] = $setting->validation_rule;
		}
		$this->validate($request, $validationRules);

		foreach ($request->all() as $key => $newSetting) {
			DB::table('settings')
				->where('key', $key)
				->update(['value' => $newSetting, 'updated_at' => DB::raw('NOW()')]);
		}

		// Clear print option cache in case they or the prices would change
		Cache::forget('printOptions');

		$request->session()->flash('message', trans('models.settings.messages.edited'));
		return redirect('admin/settings');
	}
}
