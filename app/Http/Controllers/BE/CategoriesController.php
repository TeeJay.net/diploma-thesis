<?php

namespace App\Http\Controllers\BE;

use App\Category;
use App\DataTables\Columns\Column;
use App\DataTables\DataTable;
use Illuminate\Http\Request;

class CategoriesController extends BackendController
{
	public function index(Request $request)
	{
		$this->checkAccess($request->user(), 'categories');

		$dataTable = new DataTable(Category::query(), $request);
		$dataTable->setColumns(
			(new Column('id'))
				->orderBy('id'),
			(new Column('name'))
				->orderBy('name'),
			(new Column('description'))
				->format(function($value) {
					return str_limit(strip_tags($value), 220);
				}),
			(new Column('actions'))
				->class('text-right')
				->formatView(view('BE.components.columns.actions'))
		);
		$dataTable->setColumnWidths([
			'id'          => 3,
			'description' => 40,
		]);
		$dataTable->setDefaultOrderBy('id', 'desc');

		return view('BE.categories.index', [
			'dataTable' => $dataTable,
		]);
	}

	public function create(Request $request)
	{
		$this->checkAccess($request->user(), 'categories');

		return view('BE.categories.add', [
			'category' => null,
		]);
	}

	public function store(Request $request)
	{
		$this->checkAccess($request->user(), 'categories');

		$this->validateForm($request);
		$this->saveInstance(new Category(), $request);

		$request->session()->flash('message', trans('models.categories.messages.created'));
		return redirect('admin/categories');
	}

	public function edit(Request $request, Category $category)
	{
		$this->checkAccess($request->user(), 'categories');

		return view('BE.categories.edit', [
			'category' => $category,
		]);
	}

	public function update(Request $request, Category $category)
	{
		$this->checkAccess($request->user(), 'categories');

		$this->validateForm($request);
		$this->saveInstance($category, $request);

		$request->session()->flash('message', trans('models.categories.messages.edited'));
		return redirect('admin/categories');
	}

	public function destroy(Request $request, Category $category)
	{
		$this->checkAccess($request->user(), 'categories');

		$category->delete();

		$request->session()->flash('message', trans('models.categories.messages.deleted'));
		return $category;
	}

	private function validateForm(Request $request)
	{
		$this->validate($request, [
			'name'  => 'required|max:191',
			'alias' => 'max:191',
		]);
	}

	private function saveInstance(Category $category, Request $request)
	{
		$category->fill([
			'name'        => $request->name,
			'alias'       => $request->alias ?: str_slug($request->name),
			'description' => $request->description,
		]);
		$category->save();

		return $category;
	}
}
