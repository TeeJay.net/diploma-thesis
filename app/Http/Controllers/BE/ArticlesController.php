<?php

namespace App\Http\Controllers\BE;

use App\Article;
use App\DataTables\Columns\BelongsToColumn;
use App\DataTables\Columns\Column;
use App\DataTables\DataTable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ArticlesController extends BackendController
{
	public function index(Request $request)
	{
		$this->checkAccess($request->user(), 'articles');

		$dataTable = new DataTable(Article::query()->with(['publishedByUser', 'category', 'tags']), $request);
		$dataTable->setColumns(
			(new Column('id'))
				->orderBy('id'),
			(new Column('heading'))
				->orderBy('heading'),
			(new Column('published_at'))
				->orderBy('published_at'),
			(new Column('publishedByUser'))
				->formatView(view('BE.components.columns.profile-picture-name')),
			(new Column('intro'))
				->orderBy('intro')
				->format(function($value) {
					return str_limit(strip_tags($value), 220);
				}),
			(new BelongsToColumn('category'))
				->orderBy('articles.category.name'),
			(new Column('tags')),
			(new Column('actions'))
				->class('text-right')
				->formatView(view('BE.components.columns.actions'))
		);
		$dataTable->setColumnWidths([
			'id'    => 3,
			'intro' => 30,
		]);
		$dataTable->setDefaultOrderBy('id', 'desc');

		return view('BE.articles.index', [
			'dataTable' => $dataTable,
		]);
	}

	public function create(Request $request)
	{
		$this->checkAccess($request->user(), 'articles');

		return view('BE.articles.add', [
			'article' => null,
		]);
	}

	public function store(Request $request)
	{
		$this->checkAccess($request->user(), 'articles');

		$this->validateForm($request);
		$this->saveInstance(new Article(), $request);

		$request->session()->flash('message', trans('models.articles.messages.created'));
		return redirect('admin/articles');
	}

	public function edit(Request $request, Article $article)
	{
		$this->checkAccess($request->user(), 'articles');

		return view('BE.articles.edit', [
			'article' => $article,
		]);
	}

	public function update(Request $request, Article $article)
	{
		$this->checkAccess($request->user(), 'articles');

		$this->validateForm($request);
		$this->saveInstance($article, $request);

		$request->session()->flash('message', trans('models.articles.messages.edited'));
		return redirect('admin/articles');
	}

	public function destroy(Request $request, Article $article)
	{
		$this->checkAccess($request->user(), 'articles');

		$article->delete();

		$request->session()->flash('message', trans('models.articles.messages.deleted'));
		return $article;
	}

	private function validateForm(Request $request)
	{
		$this->validate($request, [
			'heading'              => 'required|max:191',
			'alias'                => 'max:191',
			'category_id'          => 'required',
			'published_by_user_id' => 'required',
			'published_at'         => 'required|date_format:d.m.Y',
			'intro'                => 'required',
			'image'                => 'image',
			'fb_image'             => 'image',
		]);
	}

	private function saveInstance(Article $article, Request $request)
	{
		$article->fill([
			'heading'              => $request->heading,
			'alias'                => $request->alias ?: str_slug($request->heading),
			'intro'                => $request->intro,
			'text'                 => $request->text,
			'category_id'          => $request->category_id,
			'published_by_user_id' => $request->published_by_user_id,
			'published_at'         => Carbon::createFromFormat('d.m.Y', $request->published_at),
		]);
		$article->save();

		$tagIds = $request->tag_ids;
		$article->tags()->sync(array_filter($tagIds ?? []));

		if ($request->image) {
			$this->addImageToArticle($article, $request->image, 'image');
		}
		if ($request->fb_image) {
			$this->addImageToArticle($article, $request->fb_image, 'fb_image');
		}

		return $article;
	}

	private function addImageToArticle(Article $article, UploadedFile $file, string $columnName)
	{
		$fileNameParts = explode('.', $file->getClientOriginalName());
		$path = $file->storeAs('images/articles/' . $article->id, str_slug($fileNameParts[0]) . '.' . $fileNameParts[1]);

		$article->$columnName = $path;
		$article->save();
	}
}
