<?php

namespace App\Http\Controllers\BE;

use App\DataTables\Columns\BelongsToColumn;
use App\DataTables\Columns\Column;
use App\DataTables\DataTable;
use App\DataTables\Filters\DropdownFilter;
use App\DataTables\Filters\QuickFilter;
use App\Gallery;
use App\Order;
use App\Support\Calendar;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrdersController extends BackendController
{
	public function index(Request $request)
	{
		$this->checkAccess($request->user(), 'orders');

		$galleries = Gallery::all()->pluck('order_id', 'id')->toArray();

		$dataTable = new DataTable(Order::query(), $request);
		$dataTable->setColumns(
			(new Column('id'))
				->orderBy('id'),
			(new Column('created_at'))
				->orderBy('created_at')
				->format(function($value) {
					return $value->format('d.m.Y H:i');
				}),
			(new Column('first_name'))
				->orderBy('first_name'),
			(new Column('last_name'))
				->orderBy('last_name'),
			(new Column('email'))
				->orderBy('email')
				->formatView(view('BE.components.columns.email')),
			(new Column('phone'))
				->orderBy('phone'),
			(new Column('address'))
				->orderBy('street')
				->formatView(view('BE.components.columns.order-address')),
			(new Column('rental_from'))
				->orderBy('rental_from')
				->format(function($value) {
					return $value->format('d.m.Y H:i');
				}),
			(new Column('rental_to'))
				->orderBy('rental_to')
				->format(function($value) {
					return $value->format('d.m.Y H:i');
				}),
			(new BelongsToColumn('background'))
				->orderBy('orders.background.name'),
			(new Column('confirmed'))
				->orderBy('confirmed')
				->formatView(view('BE.components.columns.checked')),
			(new Column('note'))
				->orderBy('note')
				->formatView(view('BE.components.columns.order-note')),
			(new Column('final_price'))
				->orderBy('final_price')
				->format(function($value) {
					return $value . ' Kč';
				}),
			(new Column('side_data'))
				->formatView(view('BE.components.columns.order-side-data', ['galleries' => $galleries])),
			(new Column('actions'))
				->class('text-right')
				->formatView(view('BE.components.columns.actions-order', ['galleries' => $galleries]))
		);
		$dataTable->setDefaultOrderBy('id', 'desc');
		$dataTable->setColumnWidths([
			'id'        => 3,
			'address' => 10,
			'side_data' => 12,
			'actions'   => 15,
		]);

		$dataTable->setFilters(
			new QuickFilter(),
			(new DropdownFilter([
				'1' => trans('models.orders.filters.confirmed_orders'),
				'0' => trans('models.orders.filters.not_confirmed_orders'),
			]))->setName('confirmed')
				->setEmptyOptionValue(trans('models.orders.filters.both_confirmed_and_not_confirmed'))
		);

		// Build calendar with events
		$month = null;
		if ($request->has('month')) {
			$month = Carbon::createFromFormat('d-m-Y', $request->month);
		}
		$calendar = new Calendar($month);
		foreach (Order::all() as $order) {
			$calendar->addEvent($order->rental_from, $order->name, $order->confirmed ? 'label-primary' : 'label-warning');
		}

		return view('BE.orders.index', [
			'dataTable' => $dataTable,
			'calendar'  => $calendar,
		]);
	}

	public function create(Request $request)
	{
		$this->checkAccess($request->user(), 'orders');

		return view('BE.orders.add', [
			'order' => null,
		]);
	}

	public function store(Request $request)
	{
		$this->checkAccess($request->user(), 'orders');

		$this->validateForm($request);
		$order = $this->saveInstance(new Order(), $request);

		$order->setDistance();
		$order->setDeliveryPrice();
		$order->sendEmailOrderCreated();
		if ($order->confirmed) {
			$order->setInvoiceNumber();
			$order->createInvoice();
			$order->createContract();
		}

		$request->session()->flash('message', trans('models.orders.messages.created'));
		return redirect('admin/orders');
	}

	public function edit(Request $request, Order $order)
	{
		$this->checkAccess($request->user(), 'orders');

		return view('BE.orders.edit', [
			'order' => $order,
		]);
	}

	public function update(Request $request, Order $order)
	{
		$this->checkAccess($request->user(), 'orders');

		$orderBeforeEdit = clone $order;

		$this->validateForm($request);
		$this->saveInstance($order, $request);

		$order->setDistance();
		$order->setDeliveryPrice();
		if ($order->confirmed) {
			$order->setInvoiceNumber();
		}
		if ($orderBeforeEdit->confirmed == 0 && $order->confirmed == 1) {
			$order->createInvoice();
			$order->createContract();
		}

		$request->session()->flash('message', trans('models.orders.messages.edited'));
		return redirect('admin/orders');
	}

	public function destroy(Request $request, Order $order)
	{
		$this->checkAccess($request->user(), 'orders');

		$order->delete();

		$request->session()->flash('message', trans('models.orders.messages.deleted'));
		return $order;
	}

	private function validateForm(Request $request)
	{
		$this->validate($request, [
			'company'              => 'max:191',
			'first_name'           => 'required|max:191',
			'last_name'            => 'required|max:191',
			'street'               => 'required|max:191',
			'house_number'         => 'required|max:191',
			'city'                 => 'required|max:191',
			'postcode'             => 'required|max:191',
			'distance'             => 'integer|max:191',
			'email'                => 'required|email|max:191',
			'phone'                => 'required|max:191',
			'print'                => 'required|max:191',
			'rental_from'          => 'required|date_format:d.m.Y H:i',
			'rental_to'            => 'required|date_format:d.m.Y H:i|date|after:rental_from',
			'background_id'        => 'required|int',
			'final_price'          => 'required|integer',
			'final_price_words'    => 'max:191',
			'invoice_number'       => 'max:191',
			'invoice_first_name'   => 'max:191',
			'invoice_last_name'    => 'max:191',
			'invoice_street'       => 'max:191',
			'invoice_house_number' => 'max:191',
			'invoice_city'         => 'max:191',
			'invoice_postcode'     => 'max:191',
			'invoice_ic'           => 'max:191',
			'invoice_dic'          => 'max:191',
			'invoice_event'        => 'max:191',
			'invoice_date_created' => 'date_format:d.m.Y',
			'invoice_date_due'     => 'date_format:d.m.Y',
			'invoice_vs'           => 'max:191',
		]);
	}

	private function saveInstance(Order $order, Request $request)
	{
		$order->fill([
			'company'              => $request->company,
			'first_name'           => $request->first_name,
			'last_name'            => $request->last_name,
			'street'               => $request->street,
			'house_number'         => $request->house_number,
			'city'                 => $request->city,
			'postcode'             => $request->postcode,
			'distance'             => $request->distance,
			'email'                => $request->email,
			'phone'                => $request->phone,
			'print'                => $request->print,
			'rental_from'          => Carbon::createFromFormat('d.m.Y H:i', $request->rental_from),
			'rental_to'            => Carbon::createFromFormat('d.m.Y H:i', $request->rental_to),
			'note'                 => $request->note,
			'background_id'        => $request->background_id,
			'final_price'          => $request->final_price,
			'final_price_words'    => $request->final_price_words,
			'confirmed'            => $request->confirmed ?: 0,
			'invoice_number'       => $request->invoice_number,
			'invoice_first_name'   => $request->invoice_first_name,
			'invoice_last_name'    => $request->invoice_last_name,
			'invoice_street'       => $request->invoice_street,
			'invoice_house_number' => $request->invoice_house_number,
			'invoice_city'         => $request->invoice_city,
			'invoice_postcode'     => $request->invoice_postcode,
			'invoice_ic'           => $request->invoice_ic,
			'invoice_dic'          => $request->invoice_dic,
			'invoice_date_created' => Carbon::createFromFormat('d.m.Y', $request->invoice_date_created),
			'invoice_date_due'     => Carbon::createFromFormat('d.m.Y', $request->invoice_date_due),
			'invoice_vs'           => $request->invoice_vs,
			'invoice_event'        => $request->invoice_event,
		]);
		$order->save();

		return $order;
	}

	public function confirmOrder(Request $request, Order $order)
	{
		$this->checkAccess($request->user(), 'orders');

		$order->fill([
			'confirmed' => 1,
		]);
		$order->save();

		$order->setInvoiceNumber();
		$order->createInvoice();
		$order->createContract();

		$request->session()->flash('message', trans('models.orders.messages.confirmed'));
		return $order;
	}

	public function sendEmailOrderConfirmed(Request $request, Order $order)
	{
		$order->sendEmailOrderConfirmed();
		return redirect('admin/orders');
	}

	public function getInvoicePdf(Request $request, Order $order)
	{
		$mpdf = new \mPDF('cs_CZ', 'A4', 0, 'opensanslight');
		$mpdf->WriteHTML($order->invoice_html ?: '');
		$mpdf->Output();
	}

	public function refreshInvoicePdf(Request $request, Order $order)
	{
		$order->createInvoice();

		$request->session()->flash('message', trans('models.orders.messages.invoice_updated'));
		if ($request->ajax()) {
			return 1;
		}
		return redirect('admin/orders/' . $order->id . '/edit-invoice-pdf');
	}

	public function editInvoicePdf(Request $request, Order $order)
	{
		$this->checkAccess($request->user(), 'orders');

		return view('BE.orders.edit-pdf', [
			'order'    => $order,
			'itemName' => 'invoice',
		]);
	}

	public function updateInvoicePdf(Request $request, Order $order)
	{
		$this->checkAccess($request->user(), 'orders');

		$this->validate($request, [
			'invoice_html' => 'required',
		]);
		$order->createInvoice($request->invoice_html);

		$request->session()->flash('message', trans('models.orders.messages.invoice_updated'));
		return redirect('admin/orders/' . $order->id . '/edit-invoice-pdf');
	}

	public function getContractPdf(Request $request, Order $order)
	{
		$mpdf = new \mPDF('cs_CZ', 'A4', 0, 'opensanslight', 15, 15, 35, 35);

		$settings = getSettings(['contract_header', 'contract_footer']);
		$header = str_replace('[assets_path]', asset('/'), $settings->get('contract_header')->value);
		$footer = str_replace('[assets_path]', asset('/'), $settings->get('contract_footer')->value);

		$mpdf->SetHTMLHeader($header);
		$mpdf->SetHTMLFooter($footer);
		$mpdf->WriteHTML($order->contract_html ?: '');

		$mpdf->Output();
	}

	public function refreshContractPdf(Request $request, Order $order)
	{
		$order->createContract();

		$request->session()->flash('message', trans('models.orders.messages.contract_updated'));
		if ($request->ajax()) {
			return 1;
		}
		return redirect('admin/orders/' . $order->id . '/edit-contract-pdf');
	}

	public function editContractPdf(Request $request, Order $order)
	{
		$this->checkAccess($request->user(), 'orders');

		return view('BE.orders.edit-pdf', [
			'order'    => $order,
			'itemName' => 'contract',
		]);
	}

	public function updateContractPdf(Request $request, Order $order)
	{
		$this->checkAccess($request->user(), 'orders');

		$this->validate($request, [
			'contract_html' => 'required',
		]);
		$order->createContract($request->contract_html);

		$request->session()->flash('message', trans('models.orders.messages.contract_updated'));
		return redirect('admin/orders/' . $order->id . '/edit-contract-pdf');
	}
}
