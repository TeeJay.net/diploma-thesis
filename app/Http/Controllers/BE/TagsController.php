<?php

namespace App\Http\Controllers\BE;

use App\DataTables\Columns\Column;
use App\DataTables\DataTable;
use App\Tag;
use Illuminate\Http\Request;

class TagsController extends BackendController
{
	public function index(Request $request)
	{
		$this->checkAccess($request->user(), 'tags');

		$dataTable = new DataTable(Tag::query(), $request);
		$dataTable->setColumns(
			(new Column('id'))
				->orderBy('id'),
			(new Column('name'))
				->orderBy('name'),
			(new Column('description'))
				->format(function($value) {
					return str_limit(strip_tags($value), 220);
				}),
			(new Column('actions'))
				->class('text-right')
				->formatView(view('BE.components.columns.actions'))
		);
		$dataTable->setColumnWidths([
			'id'          => 3,
			'description' => 40,
		]);
		$dataTable->setDefaultOrderBy('id', 'desc');

		return view('BE.tags.index', [
			'dataTable' => $dataTable,
		]);
	}

	public function create(Request $request)
	{
		$this->checkAccess($request->user(), 'tags');

		return view('BE.tags.add', [
			'tag' => null,
		]);
	}

	public function store(Request $request)
	{
		$this->checkAccess($request->user(), 'tags');

		$this->validateForm($request);
		$this->saveInstance(new Tag(), $request);

		$request->session()->flash('message', trans('models.tags.messages.created'));
		return redirect('admin/tags');
	}

	public function edit(Request $request, Tag $tag)
	{
		$this->checkAccess($request->user(), 'tags');

		return view('BE.tags.edit', [
			'tag' => $tag,
		]);
	}

	public function update(Request $request, Tag $tag)
	{
		$this->checkAccess($request->user(), 'tags');

		$this->validateForm($request);
		$this->saveInstance($tag, $request);

		$request->session()->flash('message', trans('models.tags.messages.edited'));
		return redirect('admin/tags');
	}

	public function destroy(Request $request, Tag $tag)
	{
		$this->checkAccess($request->user(), 'tags');

		$tag->delete();

		$request->session()->flash('message', trans('models.tags.messages.deleted'));
		return $tag;
	}

	private function validateForm(Request $request)
	{
		$this->validate($request, [
			'name'  => 'required|max:191',
			'alias' => 'max:191',
		]);
	}

	private function saveInstance(Tag $tag, Request $request)
	{
		$tag->fill([
			'name'        => $request->name,
			'alias'       => $request->alias ?: str_slug($request->name),
			'description' => $request->description,
		]);
		$tag->save();

		return $tag;
	}
}
