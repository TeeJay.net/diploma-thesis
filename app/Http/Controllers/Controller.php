<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function checkAccess(User $user, string $section)
	{
		if (!$user->hasPermission($section)) {
			abort(403);
		}
	}

	public function clearCache(Request $request)
	{
		if (!$request->has('ok') || !$request->get('ok')) {
			return 'You shall not pass!';
		}
		Artisan::call('cache:clear');
		Artisan::call('config:clear');
		Artisan::call('route:clear');
		Artisan::call('view:clear');
		Artisan::call('clear-compiled');
		//		Artisan::call('debugbar:clear'); // works on local only

		return 'ok';
	}

	public function cache(Request $request)
	{
		if (!$request->has('ok') || !$request->get('ok')) {
			return 'You shall not pass!';
		}
		Artisan::call('config:cache');
		Artisan::call('route:cache');
		Artisan::call('optimize');

		return 'ok';
	}
}
