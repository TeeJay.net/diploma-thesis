<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Glide\Server;

class ImageController extends Controller
{
	protected $glideServer;

	public function __construct(Server $glideServer)
	{
		$this->glideServer = $glideServer;
	}

	/**
	 * Shows image at $path of the given dimensions and caches it
	 * Add nocache GET parameter to load the image without server cache
	 * Add version GET parameter with a timestamp to load the image without browser cache
	 *
	 * @param Request $request
	 * @param string  $path
	 */
	public function show(Request $request, string $path)
	{
		if ($request->has('nocache')) {
			$this->glideServer->deleteCache($path);
		}
		$this->glideServer->getImageResponse($path, $request->except(['nocache', 'version']))->send();
	}
}
