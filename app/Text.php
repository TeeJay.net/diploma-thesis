<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Text
 *
 * @property int                 $id
 * @property string              $name
 * @property string              $alias
 * @property string              $text
 * @property int|null            $created_by_user_id
 * @property int|null            $updated_by_user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\User|null $createdByUser
 * @property-read \App\User|null $updatedByUser
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Text onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereUpdatedByUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Text withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Text withoutTrashed()
 * @mixin \Eloquent
 */
class Text extends BaseModel
{
	use SoftDeletes;

	protected $table = 'texts';
	protected $guarded = [];
	protected $dates = [
		'deleted_at',
	];

	/*
	 * Relationships
	 */
	public function createdByUser()
	{
		return $this->belongsTo(User::class);
	}

	public function updatedByUser()
	{
		return $this->belongsTo(User::class);
	}

	/*
	 * Inverse relationships
	 */

}
