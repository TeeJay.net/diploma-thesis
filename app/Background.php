<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Background
 *
 * @property int                 $id
 * @property string              $name
 * @property string              $description
 * @property int                 $created_by_user_id
 * @property int                 $updated_by_user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\User      $createdByUser
 * @property-read \App\User      $updatedByUser
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Background onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Background whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Background whereCreatedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Background whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Background whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Background whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Background whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Background whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Background whereUpdatedByUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Background withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Background withoutTrashed()
 * @mixin \Eloquent
 * @property string $image
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Background whereImage($value)
 */
class Background extends BaseModel
{
	use SoftDeletes;

	protected $table = 'backgrounds';
	protected $guarded = [];
	protected $dates = [
		'deleted_at',
	];

	/*
	* Relationships
	*/
	public function createdByUser()
	{
		return $this->belongsTo(User::class);
	}

	public function updatedByUser()
	{
		return $this->belongsTo(User::class);
	}
}

