<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends BaseModel
{
	use SoftDeletes;

	protected $table = 'clients';
	protected $guarded = [];
	protected $dates = [
		'deleted_at',
	];

	/*
	* Relationships
	*/
	public function createdByUser()
	{
		return $this->belongsTo(User::class);
	}

	public function updatedByUser()
	{
		return $this->belongsTo(User::class);
	}
}

